package com.je.core.service;

import com.je.core.exception.PlatformException;
import com.je.core.exception.PlatformExceptionEnum;
import com.je.core.mapper.MetaMapper;
import com.je.core.util.ArrayUtils;
import com.je.core.util.StringUtil;
import com.je.core.util.bean.DynaBean;
import com.je.ibatis.extension.builder.MetaStatementBuilder;
import com.je.ibatis.extension.cache.MetaDataCacheManager;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.ibatis.extension.enums.DbType;
import com.je.ibatis.extension.metadata.model.Column;
import com.je.ibatis.extension.metadata.model.Table;
import com.je.ibatis.extension.plugins.pagination.Page;
import com.je.ibatis.extension.toolkit.Constants;
import com.je.ibatis.session.CustomConfiguration;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * MetaServiceImpl
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/12/4
 */
@Service
public class MetaServiceImpl implements MetaService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final DataSource dataSource;

    private final MetaMapper baseDataMapper;

    private final SqlSessionFactory sessionFactory;

    @Autowired
    public MetaServiceImpl(DataSource dataSource, MetaMapper baseDataMapper, SqlSessionFactory sessionFactory) {
        this.dataSource = dataSource;
        this.baseDataMapper = baseDataMapper;
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Connection getConnection() {

        try {
            Connection connection;
            //循环获取，避免拿到已经失效的连接
            do {
                connection = dataSource.getConnection();
            } while (connection.isClosed());
            return connection;
        } catch (SQLException e) {
            throw new PlatformException("获取数据库连接失败！", PlatformExceptionEnum.CONNECTION_ERROR, e);
        }
    }

    @Override
    public DbType getDbType() {
        try {
            return DbType.getDbTypeByUrl(getConnection().getMetaData().getURL());
        } catch (SQLException e) {
            throw new PlatformException("获取数据库连接失败！", PlatformExceptionEnum.CONNECTION_ERROR, e);
        }
    }

    @Override
    public void clearMyBatisCache() {
        MetaStatementBuilder metaBuilder = getMetaBuilder();
        if (metaBuilder != null) {
            MetaDataCacheManager cacheManager = metaBuilder.getCacheManager();
            cacheManager.clear();
        }
    }

    @Override
    public void clearMyBatisTableCache(String tableCode) {
        MetaStatementBuilder metaBuilder = getMetaBuilder();
        if (metaBuilder != null) {
            metaBuilder.builderTable(tableCode);
        }
    }

    @Override
    public void clearMyBatisFuncCache() {
        MetaStatementBuilder metaBuilder = getMetaBuilder();
        if (metaBuilder != null) {
            MetaDataCacheManager cacheManager = metaBuilder.getCacheManager();
            cacheManager.clearFunction();
        }
    }

    @Override
    public void clearMyBatisFuncCache(String funcCode) {
        MetaStatementBuilder metaBuilder = getMetaBuilder();
        if (metaBuilder != null) {
            metaBuilder.builderFunction(funcCode);
        }
    }

    /**
     * 获取元数据缓存
     */
    private MetaStatementBuilder getMetaBuilder() {
        Configuration configuration = sessionFactory.getConfiguration();
        if (configuration instanceof CustomConfiguration) {
            CustomConfiguration customConfiguration = (CustomConfiguration) configuration;
            return customConfiguration.getMetaStatementBuilder();
        }
        return null;
    }

    @Override
    public int insert(String tableCode, DynaBean beanMap) {
        if (StringUtils.isNotBlank(tableCode)) {
            beanMap.put(Constants.KEY_TABLE_CODE, tableCode);
        }
        return baseDataMapper.insertMap(beanMap.fetchAllValues());
    }

    @Override
    public int insertBatch(String tableCode, List<DynaBean> list) {

        // 校验插入数据
        if (list == null || list.isEmpty()) {
            logger.info("insertBatch list is null or empty!");
            return 0;
        }

        //如果未指定表名从list中获取
        if (StringUtils.isBlank(tableCode)) {
            tableCode = list.get(0).getStr(Constants.KEY_TABLE_CODE);
        }

        //list转换并插入
        List<Map<String, Object>> collect = list.stream().map(p -> (Map<String, Object>) p.fetchAllValues()).collect(Collectors.toList());
        return baseDataMapper.insertBatch(tableCode, collect);
    }

    @Override
    public int update(String tableCode, String pkValue, DynaBean beanMap, ConditionsWrapper wrapper) {

        if (StringUtils.isNotBlank(tableCode)) {
            beanMap.put(Constants.KEY_TABLE_CODE, tableCode);
        }
        if (StringUtils.isNotBlank(pkValue)) {
            beanMap.put(Constants.KEY_PK_VALUE, pkValue);
        }
        return baseDataMapper.updateMap(beanMap.fetchAllValues(), wrapper);
    }

    @Override
    public int delete(String tableCode, ConditionsWrapper wrapper) {
        if (StringUtils.isNotBlank(tableCode)) {
            wrapper.table(tableCode);
        }
        return baseDataMapper.delete(wrapper);
    }

    @Override
    public List<Map<String, Object>> selectSql(Page page, ConditionsWrapper wrapper) {
        List<Map<String, Object>> list = baseDataMapper.selectSql(page, wrapper);
        return list == null ? new ArrayList<>() : list.stream().filter(Objects::nonNull).collect(Collectors.toList());
    }

    @Override
    public List<DynaBean> select(String tableCode, Page page, ConditionsWrapper wrapper, String columns) {
        if (wrapper == null) {
            wrapper = ConditionsWrapper.builder();
        }
        if (StringUtils.isNotBlank(tableCode)) {
            wrapper.table(tableCode);
        } else {
            tableCode = wrapper.getTable();
        }
        List<DynaBean> list = transformDynaBean(baseDataMapper.select(page, wrapper), tableCode);
        page.setRecords(list);
        return list;
    }

    @Override
    public DynaBean selectOne(String tableCode, ConditionsWrapper wrapper, String columns) {
        List<DynaBean> list = select(tableCode, 0, -1, wrapper);
        if (list.size() == 1) {
            return list.get(0);
        } else if (list.size() == 0) {
            return null;
        } else {
            logger.error("selectOne 查询出({})条数据。", list.size());
            throw new PlatformException("selectOne 查询出多条数据。", PlatformExceptionEnum.UNKOWN_ERROR);
        }
    }

    @Override
    public List<Map<String, Object>> load(String funcCode, Page page, ConditionsWrapper wrapper) {
        if (wrapper == null) {
            wrapper = ConditionsWrapper.builder();
        }
        if (StringUtils.isNotBlank(funcCode)) {
            wrapper.function(funcCode);
        }
        List<Map<String, Object>> load = baseDataMapper.load(page, wrapper);
        page.setRecords(load == null ? new ArrayList<>() : load.stream().filter(Objects::nonNull).collect(Collectors.toList()));
        return load;
    }

    @Override
    public DynaBean selectOneByPk(String tableCode, String pkValue, String columns) {
        Map<String, Object> map = baseDataMapper.selectOneByPk(tableCode, pkValue);
        if (map == null) {
            return null;
        }
        return transformDynaBean(map, tableCode);
    }

    @Override
    public int executeSql(ConditionsWrapper wrapper) {
        String formatSql = wrapper.getSql().trim().toLowerCase();
        if (formatSql.startsWith("update") || formatSql.startsWith("create") || formatSql.startsWith("comment")
                || formatSql.startsWith("alter") || formatSql.startsWith("drop") || formatSql.startsWith("if")
                || formatSql.startsWith("exec") || formatSql.startsWith("execute")
                || formatSql.startsWith("truncate")) {
            return baseDataMapper.updateSql(wrapper);
        } else if (formatSql.startsWith("insert")) {
            return baseDataMapper.insertSql(wrapper);
        } else if (formatSql.startsWith("delete")) {
            return baseDataMapper.deleteSql(wrapper);
        } else {
            logger.error("语句不合法 ({})", wrapper.getSql());
            throw new PlatformException("语句不合法 :" + wrapper.getSql(), PlatformExceptionEnum.UNKOWN_ERROR);
        }
    }

    @Override
    public long countBySql(ConditionsWrapper wrapper) {

        //支持不完整语句 ConditionsWrapper.builder().table("JE_CORE_TABLECOLUMN").apply("TABLECOLUMN_RESOURCETABLE_ID={0} AND TABLECOLUMN_CODE='SY__POSTIL'", table.getStr("JE_CORE_RESOURCETABLE_ID"))
        String lowerCaseSql = wrapper.getSql().trim().toLowerCase();
        if (!lowerCaseSql.startsWith("select") && StringUtils.isNotBlank(wrapper.getTable())) {
            wrapper = ConditionsWrapper.builder().putAll(wrapper.getParameter()).apply("select count(*) from ").apply(wrapper.getTable())
                    .apply(StringUtils.isNotBlank(wrapper.getSql().trim()), " where " + wrapper.getSql());
        }

        List<Map<String, Object>> list = selectSql(wrapper);
        if (list != null && !list.isEmpty() && !list.get(0).isEmpty()) {
            //获取第一个值
            Map<String, Object> map = list.get(0);
            String countStr = map.get(map.keySet().toArray()[0]).toString();
            return Long.parseLong(countStr);
        }
        logger.error("语句无执行结果 {}，{}", wrapper.getSql(), wrapper.getParameter());
        throw new PlatformException("count语句无执行结果。", PlatformExceptionEnum.UNKOWN_ERROR);
    }

    /**
     * 获取所有列
     *
     * @param tableCode 表名
     * @return 列信息
     */
    private List<Column> getColumns(String tableCode) {
        //从 je-ibatis 中读取表缓存
        MetaStatementBuilder metaBuilder = getMetaBuilder();
        if (metaBuilder == null) {
            return null;
        }
        Table table = getMetaBuilder().getCacheManager().getTable(tableCode);
        if (table == null) {
            return null;
        }
        return table.getColumnList();
    }

    private List<DynaBean> transformDynaBean(List<Map<String, Object>> model, String tableCode) {
        ArrayList<DynaBean> list = new ArrayList<>();
        if (model != null) {
            //去除空元素
            model = model.stream().filter(Objects::nonNull).collect(Collectors.toList());
            //初始化大小
            list.ensureCapacity(model.size());
            //获取全部列信息
            List<Column> columns = getColumns(tableCode);
            model.forEach(p -> list.add(transformDynaBean(p, tableCode, columns)));
        }
        return list;
    }

    private DynaBean transformDynaBean(Map<String, Object> model, String tableCode) {
        return transformDynaBean(model, tableCode, getColumns(tableCode));
    }

    /**
     * @param model     mybatis查询出的数据
     * @param tableCode 资源表名
     * @param columns   资源表所有列
     * @return DynaBean
     */
    private DynaBean transformDynaBean(Map<String, Object> model, String tableCode, List<Column> columns) {
        DynaBean dynaBean = new DynaBean();
        dynaBean.setValues(model);
        dynaBean.table(tableCode);
        if (columns != null) {
            columns.forEach(column -> {
                String columnCode = column.getCode();
                Object value = model.get(columnCode);
                if (ArrayUtils.contains(new String[]{"CLOB", "BIGCLOB"}, column.getType()) && value == null) {
                    if (model.get(columnCode) == null) {
                        dynaBean.set(columnCode, "");
                    }
                } else if (ArrayUtils.contains(new String[]{"NUMBER", "FLOAT"}, column.getType()) && value instanceof BigDecimal) {
                    if ("NUMBER".equals(column.getType())) {
                        dynaBean.set(columnCode, Integer.parseInt(StringUtil.getDefaultValue(value, "0")));
                    } else {
                        dynaBean.set(columnCode, Double.parseDouble(StringUtil.getDefaultValue(value, "0")));
                    }
                }
            });
        }
        return dynaBean;
    }
}