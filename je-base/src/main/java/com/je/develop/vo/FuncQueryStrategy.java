package com.je.develop.vo;

import java.io.Serializable;

/**
 * 功能查询策略
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/12/27
 */
public class FuncQueryStrategy implements Serializable {

    private static final long serialVersionUID = -6059221221040988668L;
    /**
     * 主键
     */
    private String id;
    /**
     * 策略sql
     */
    private String sql;
    /**
     * 是否覆盖功能whereSql
     */
    private String isOverride;

    public FuncQueryStrategy(String id, String sql, String isOverride) {
        this.id = id;
        this.sql = sql;
        this.isOverride = isOverride;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public String getIsOverride() {
        return isOverride;
    }

    public void setIsOverride(String isOverride) {
        this.isOverride = isOverride;
    }
}