package com.je.rbac.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户领导以及下属的ihe
 */
public class UserLeader implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 8657827743312520963L;
    /**
     * 直属领导关系
     */
    private List<String> zsUserIds=new ArrayList<>();
    /**
     * 主管领导关系
     */
    private List<String> zgUserIds=new ArrayList<>();
    /**
     * 监管领导关系
     */
    private List<String> jgUserIds=new ArrayList<>();
    /**
     * 层级监管领导关系
     */
    private Map<String,List<String>> cjJgUserIds=new HashMap<>();
    /**
     * 主管领导
     */
    private Map<String,List<String>> cjZglds=new HashMap<>();
    /**
     * 部门负责人关系
     */
    private List<String> fzrUserIds=new ArrayList<>();

    public List<String> getZsUserIds() {
        return zsUserIds;
    }

    public void setZsUserIds(List<String> zsUserIds) {
        this.zsUserIds = zsUserIds;
    }

    public List<String> getZgUserIds() {
        return zgUserIds;
    }

    public void setZgUserIds(List<String> zgUserIds) {
        this.zgUserIds = zgUserIds;
    }

    public List<String> getJgUserIds() {
        return jgUserIds;
    }

    public void setJgUserIds(List<String> jgUserIds) {
        this.jgUserIds = jgUserIds;
    }

    public List<String> getFzrUserIds() {
        return fzrUserIds;
    }

    public void setFzrUserIds(List<String> fzrUserIds) {
        this.fzrUserIds = fzrUserIds;
    }

    public Map<String, List<String>> getCjJgUserIds() {
        return cjJgUserIds;
    }

    public void setCjJgUserIds(Map<String, List<String>> cjJgUserIds) {
        this.cjJgUserIds = cjJgUserIds;
    }

    public Map<String, List<String>> getCjZglds() {
        return cjZglds;
    }

    public void setCjZglds(Map<String, List<String>> cjZglds) {
        this.cjZglds = cjZglds;
    }
}
