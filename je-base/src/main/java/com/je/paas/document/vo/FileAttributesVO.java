package com.je.paas.document.vo;

import com.je.paas.document.model.bean.BasePojo;

import java.util.Date;

/**
 * 文件属性
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/8/27
 */
public class FileAttributesVO extends BasePojo {

    /**
     * 文件唯一标识
     */
    private String fileKey;

    /**
     * 文件真实名称,je_document_rel.name
     */
    private String relName;

    /**
     * 文件后缀
     */
    private String suffix;

    /**
     * 文件访问地址(私有文件为null)
     */
    private String fullUrl;

    /**
     * 文件大小
     */
    private Long size;

    /**
     * 上传人
     */
    private String createUserName;

    /**
     * 上传人部门
     */
    private String createUserDeptName;

    /**
     * 上传时间
     */
    private Date createTime;

    public String getFileKey() {
        return fileKey;
    }

    public void setFileKey(String fileKey) {
        this.fileKey = fileKey;
    }

    public String getRelName() {
        return relName;
    }

    public void setRelName(String relName) {
        this.relName = relName;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getFullUrl() {
        return fullUrl;
    }

    public void setFullUrl(String fullUrl) {
        this.fullUrl = fullUrl;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getCreateUserDeptName() {
        return createUserDeptName;
    }

    public void setCreateUserDeptName(String createUserDeptName) {
        this.createUserDeptName = createUserDeptName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}