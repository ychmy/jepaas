package com.je.cache.service.rbac;

import com.je.cache.service.EhcacheManager;
import com.je.core.util.bean.DynaBean;

import java.util.List;

public class UserInfoCacheManager {
    /**
     * 获取缓存值
     * @param zhId
     * @return
     */
    public static List<DynaBean> getCacheValue(String zhId){
        String cacheName="userInfoCache";
        return (List<DynaBean>) EhcacheManager.getCacheValue(cacheName, zhId);
    }
    /**
     * 添加缓存
     * @param zhId 用户id
     * @param value 值
     */
    public static void putCache(String zhId, List<DynaBean> value){
        String cacheName="userInfoCache";
        EhcacheManager.putCache(cacheName, zhId, value);
    }
    /**
     * 清空所有缓存
     */
    public static void clearAllCache(){
        String cacheName="userInfoCache";
        EhcacheManager.clearAllCache(cacheName);
    }
    /**
     * 清空指定的缓存
     * @param zhId 用户id
     */
    public static void removeCache(String zhId){
        String cacheName="userInfoCache";
        EhcacheManager.removeCache(cacheName, zhId);
    }
}
