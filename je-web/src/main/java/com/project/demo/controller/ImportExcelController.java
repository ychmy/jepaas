package com.project.demo.controller;

import com.google.common.base.Strings;
import com.je.core.base.MethodArgument;
import com.je.core.controller.PlatformController;
import com.je.core.result.BaseRespResult;
import com.je.thrid.importsource.ExcelImportSource;
import com.project.demo.parser.DepartmentImportParser;
import com.project.demo.parser.EndUserImportParser;
import com.project.demo.service.DpetExcelImportService;
import com.project.demo.service.UserImportService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.SQLException;

/**
 * @program: je-platform
 * @author: LIULJ
 * @create: 2020/7/26
 * @description: Excel表导入部门和人员
 */
@Controller
@RequestMapping(value = "/je/demo/importExcel")
public class ImportExcelController extends PlatformController {
    @Autowired
    private DpetExcelImportService dpetExcelImportService;
    @Autowired
    private UserImportService userImportService;


    /**
     * 导入部门
     * 
     * @param param
     * @return
     * @throws SQLException
     */
    @RequestMapping(value = {"/doImportDepartment"}, method = {RequestMethod.POST},
        produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public BaseRespResult doImportDepartment(MethodArgument param) {
        String fileKey = param.getRequest().getParameter("fileKey");
        Integer columnNameStartLine = Integer.valueOf(param.getRequest().getParameter("columnNameStartLine"));
        Integer columnValueStartLine = Integer.valueOf(param.getRequest().getParameter("columnValueStartLine"));
        String sheetIndex = param.getRequest().getParameter("sheetIndex");
        if (Strings.isNullOrEmpty(fileKey)) {
            return BaseRespResult.errorResult("文件获取失败！");
        }
        ExcelImportSource excelImportSource = new ExcelImportSource(fileKey,columnNameStartLine,columnValueStartLine,sheetIndex);
        DepartmentImportParser departmentImportParser = new DepartmentImportParser();
        JSONObject customParameters = new JSONObject();
        dpetExcelImportService.doImport(excelImportSource, departmentImportParser, customParameters);
        return BaseRespResult.successResult("正在导入，请稍等！");
    }

    /**
     * 导入用户
     * 
     * @param param
     * @return
     * @throws SQLException
     */
    @RequestMapping(value = {"/doImportUser"}, method = {RequestMethod.POST},
        produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public BaseRespResult doImportUser(MethodArgument param) {
        String fileKey = param.getRequest().getParameter("fileKey");
        String id = param.getRequest().getParameter("id");
        if (Strings.isNullOrEmpty(fileKey)) {
            return BaseRespResult.errorResult("文件获取失败！");
        }
        ExcelImportSource excelImportSource = new ExcelImportSource(fileKey, 2, 3, "Sheet1");
        EndUserImportParser endUserImportParser = new EndUserImportParser();
        JSONObject customParameters = new JSONObject();
        customParameters.put("id", id);
        userImportService.doImport(excelImportSource, endUserImportParser, customParameters);
        return BaseRespResult.successResult("正在导入，请稍等！");
    }

}
