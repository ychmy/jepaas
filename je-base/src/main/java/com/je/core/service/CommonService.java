package com.je.core.service;

import com.je.core.base.MethodArgument;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.core.mapper.query.Query;
import com.je.core.util.SecurityUserHolder;
import com.je.core.util.bean.DynaBean;
import com.je.develop.vo.FuncInfo;
import com.je.develop.vo.FuncPermVo;
import com.je.rbac.model.EndUser;

import java.util.List;
import java.util.Map;

/**
 * 通用业务方法
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/12/6
 */
public interface CommonService {

    /**
     * 构建mark信息
     *
     * @param lists       查询结果数据信息
     * @param tableCode   表名
     * @param currentUser 人员信息
     * @param funcId      功能id
     */
    void buildMarkInfo(List<Map<String, Object>> lists, String tableCode, EndUser currentUser, String funcId);

    void buildPostilInfo(List<Map<String, Object>> lists, String tableCode, String funcId);

    void buildFuncEditInfo(List<Map<String, Object>> lists, String tableCode, EndUser currentUser, String funcCode);

    /**
     * 构建创建信息
     *
     * @param model 业务bean
     */
    default void buildModelCreateInfo(DynaBean model) {
        buildModelCreateInfo(model, SecurityUserHolder.getCurrentUser());
    }

    /**
     * 构建创建信息
     *
     * @param model       业务bean
     * @param currentUser 用户
     */
    void buildModelCreateInfo(DynaBean model, EndUser currentUser);

    /**
     * 构建修改信息
     *
     * @param model 业务bean
     */
    default void buildModelModifyInfo(DynaBean model) {
        buildModelModifyInfo(model, SecurityUserHolder.getCurrentUser());
    }

    /**
     * 构建修改信息
     *
     * @param model       业务bean
     * @param currentUser 用户
     */
    void buildModelModifyInfo(DynaBean model, EndUser currentUser);

    /**
     * 自动生成字段赋值
     *
     * @param fieldCode 系统自动生成字段的code
     * @param funcCode  功能code
     * @param dynaBean  实体
     * @return 生成的值
     */
    default String buildCode(String fieldCode, String funcCode, DynaBean dynaBean) {
        return buildCode(fieldCode, funcCode, dynaBean, "");
    }

    /**
     * 自动生成字段赋值
     *
     * @param fieldCode 系统自动生成字段的code
     * @param funcCode  功能code
     * @param dynaBean  实体
     * @param zhId      租户ID
     * @return 生成的值
     */
    String buildCode(String fieldCode, String funcCode, DynaBean dynaBean, String zhId);

    /**
     * 自动生成字段赋值
     *
     * @param codeGenFieldInfo 自动生成编号配置
     * @param dynaBean         实体
     */
    default void buildCode(String codeGenFieldInfo, DynaBean dynaBean) {
        buildCode(codeGenFieldInfo, dynaBean, "");
    }

    /**
     * 自动生成字段赋值
     *
     * @param codeGenFieldInfo 自动生成编号配置
     * @param dynaBean         实体
     * @param zhId             租户ID
     */
    void buildCode(String codeGenFieldInfo, DynaBean dynaBean, String zhId);

    /**
     * 保存业务bean
     *
     * @param dynaBean 业务bean
     * @return com.je.core.util.bean.DynaBean
     */
    DynaBean doSave(DynaBean dynaBean);

    /**
     * 构建树形的排序序号
     *
     * @param dynaBean 业务bean
     */
    void generateTreeOrderIndex(DynaBean dynaBean);

    /**
     * 文件保存业务元数据信息
     *
     * @param dynaBean         业务bean
     * @param batchFilesFields 多附件字段名
     * @param uploadableFields 单附件字段名
     * @param funcCode         功能编码
     */
    void doSaveFileMetadata(DynaBean dynaBean, String batchFilesFields, String uploadableFields, String funcCode);

    /**
     * 视图类型功能处理
     *
     * @param funcCode 功能编码
     * @param dynaBean 业务bean
     */
    void doViewData(String funcCode, DynaBean dynaBean);

    /**
     * 视图类型功能处理
     *
     * @param funcCode 功能编码
     * @param dynaBean 业务bean
     */
    void doViewData(String funcCode, DynaBean dynaBean,String viewConfigInfo);



    /**
     * 子功能默认添加树形ROOT节点操作
     *
     * @param dynaBean 业务bean
     * @param funcCode 功能编码
     */
    void doChildrenTree(DynaBean dynaBean, String funcCode);

    /**
     * 递归复制子功能数据
     *
     * @param dynaBean         被复制数据
     * @param funcCode         功能code
     * @param codeGenFieldInfo 构建编号字段
     * @param uploadableFields 附件字段
     * @return com.je.core.util.bean.DynaBean
     */
    DynaBean doCopy(DynaBean dynaBean, String funcCode, String codeGenFieldInfo, String uploadableFields);

    /**
     * 删除子功能数据
     *
     * @param funcCode  功能code
     * @param tableCode 表名
     * @param pkCode    主键字段名
     * @param ids       被删除数据ID
     * @param doTree    是否是树形
     */
    void removeChild(String funcCode, String tableCode, String pkCode, String ids, boolean doTree);

    /**
     * 删除批量附件处理
     *
     * @param tableCode 表code
     * @param ids       被删除数据ID
     */
    void doRemoveBatchFiles(String tableCode, String ids);

    /**
     * 删除树形批量附件处理
     *
     * @param tableCode 表code
     * @param ids       被删除数据ID
     */
    void doRemoveTreeBatchFiles(String tableCode, String ids);

    /**
     * 根据视图级联配置删除数据
     *
     * @param viewConfigInfo 视图级联配置
     * @param viewTableCode  视图名称
     * @param mainPkCode     主键字段名
     * @param ids            被删除数据ID
     */
    int doViewDelData(String viewConfigInfo, String viewTableCode, String mainPkCode, String ids);

    /**
     * 删除平台功能数据
     *
     * @param tableCode 表名
     * @param pkCode    主键字段名
     * @param ids       被删除数据ID
     * @param mark      是否删除标记
     * @param funcEdit  是否删除已读未读
     * @param postil    是否删除批注
     * @param doTree    是否删除是树形数据
     */
    void doRemoveData(String tableCode, String pkCode, String ids, boolean mark, boolean funcEdit, boolean postil, boolean doTree);

    /**
     * doInsertUpdateList
     *
     * @param tableCode        表名
     * @param strData          数据
     * @param funcType         功能类型
     * @param codeGenFieldInfo 自动生成编号字段
     * @param viewConfigInfo   视图级联配置
     * @return java.util.List<java.util.Map>
     */
    List<Map> doInsertUpdateList(String tableCode, String strData, String funcType, String codeGenFieldInfo, String viewConfigInfo);

    /**
     * doUpdateList
     *
     * @param tableCode        表名
     * @param updateStr        数据
     * @param funcType         功能类型
     * @param funcCode         功能编码
     * @param codeGenFieldInfo 自动生成编号字段
     * @return java.util.List<com.je.core.util.bean.DynaBean>
     */
    List<DynaBean> doUpdateList(String tableCode, String updateStr, String funcType, String funcCode, String codeGenFieldInfo);

    /**
     * doUpdateListByJquery
     *
     * @param tableCode  表名
     * @param funcType   功能类型
     * @param funcCode   功能编码
     * @param values 是否全部修改  0 是 1 不是
     * @return java.util.List<com.je.core.util.bean.DynaBean>
     */
    int doUpdateAllList(String tableCode, String funcType, String funcCode, Map<String,Object> values, MethodArgument param);

    /**
     * doUpdateList
     *
     * @param tableCode      表名
     * @param updateStr      数据
     * @param funcType       功能类型
     * @param viewConfigInfo 视图级联配置
     * @return java.util.List<com.je.core.util.bean.DynaBean>
     */
    List<DynaBean> doUpdateList(String tableCode, String updateStr, String funcType, String viewConfigInfo);

    /**
     * treeMove
     *
     * @param dynaBean 实体
     * @return com.je.core.util.bean.DynaBean
     */
    DynaBean treeMove(DynaBean dynaBean);

    /**
     * updateTreePanent4NodeType
     *
     * @param tableCode 实体
     * @param parentId  父级id
     * @return void
     */
    void updateTreePanent4NodeType(String tableCode, String parentId);

    /**
     * 验证字段唯一
     *
     * @param dynaBean  实体
     * @param fieldCode 验证字段
     * @return Boolean
     */
    default Boolean checkFieldUnique(DynaBean dynaBean, String fieldCode) {
        return checkFieldUnique(dynaBean, fieldCode, new Query());
    }

    /**
     * 验证字段唯一
     *
     * @param dynaBean  实体
     * @param fieldCode 验证字段
     * @param query     附加条件
     * @return Boolean
     */
    Boolean checkFieldUnique(DynaBean dynaBean, String fieldCode, Query query);

    /**
     * 保存编辑标记信息
     *
     * @param funcCode  功能编码
     * @param tableCode 业务实体
     * @param pkValue   主键
     * @param userId    标记的用户ID
     * @param isNew     1位标记  0为不标记   2未读
     */
    void doDataFuncEdit(String funcCode, String tableCode, String pkValue, String userId, String isNew);

    /**
     * loadTree
     *
     * @param rootId    根节点ID
     * @param tableCode 资源表名
     * @param excludes
     * @param checked
     * @param query     查询条件
     * @return java.util.List
     */
    List<Map<String, Object>> loadGridTree(String rootId, String tableCode, String excludes, Boolean checked, Query query);

    default List<JSONTreeNode> loadTreeNodeList(String rootId, String tableName, JSONTreeNode template, Query where) {
        return loadTreeNodeList(rootId, tableName, template, where, null, null);
    }

    List<JSONTreeNode> loadTreeNodeList(String rootId, String tableName, JSONTreeNode template, Query where, List<String> includeIds, String[] beanFields);

    JSONTreeNode buildJSONNewTree(List<JSONTreeNode> lists, String rootId);

    /**
     * 功能配置
     *
     * @param funcCode 功能code
     * @return com.je.develop.vo.FuncInfo
     */
    FuncInfo functionConfig(String funcCode);

    /**
     * 功能数据权限
     *
     * @param funcCode 功能code
     * @return com.je.develop.vo.FuncPermVo
     */
    FuncPermVo functionPerm(String funcCode);
}
