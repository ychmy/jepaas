package com.project.demo.service;

import net.sf.json.JSONObject;

import java.util.List;

public interface ImportService {

    public String buildToken(String userCode);

    public String importRole(List<JSONObject> roleInfos);

    public String importDept(List<JSONObject> roleInfos);

    public String importUser(List<JSONObject> roleInfos);
}
