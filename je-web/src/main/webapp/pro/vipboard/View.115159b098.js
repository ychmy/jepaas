/*
 * @Descripttion:
 * @version: V1.0.0
 * @Author: Shuangshuang Song
 * @Date: 2020-06-17 11:20:00
 * @LastEditors: Shuangshuang Song
 * @LastEditTime: 2020-10-29 15:23:30
 */
/**
 * 项目view
 */
/* eslint-disable */
Ext.define("PRO.vipboard.View", {
  extend: 'Ext.panel.Panel',
  alias:'widget.pro.vipboard',
  border: 0,
  layout: 'fit',
  autoScroll: true,
  initComponent: function () {
    var me = this;
    me.bodyStyle = "background-color: #f0f2f5;";
    me.html = '<div class="loading" style="color:#a6a6a6;text-align:center;padding-top:18%;font-size: 20px;font-weight: bold;">' + JE.getLocalLang('common.loading') + '</div>';
    me.callParent(arguments);
    me.on('activate',function(){
      if (me.created){
        me.load();
      }else{
        me.created = true;
      }
    })
  },
  afterRender: function () {
    var me = this;
    me.callParent(arguments);
    var folder = 'pro/vipboard'.split('/').pop();
    var tag = folder.replace(/([A-Z])/g, '-$1').toLowerCase();
    var vueInfo = me.vueInfo || {}
    //加载本页面资源
    JE.loadScript([
      // '/static/ux/moment/moment.min.js',
      '/static/vue/'+folder+'/index.js',
      '/static/vue/'+folder+'/index.css',
    ], function () {
      /* //时间组件
      if (window.moment) {
        if (window._JE_LOCAL_LANG == 'zh_CN') {
          window.moment.locale('zh-CN')
        } else {
          window.moment.locale('en')
        }
      } */
      var loading = me.body.down('.loading');
      loading && loading.remove();
      //创建vue装载dom
      var dom = me.body.insertHtml('beforeEnd', '<' + tag +' :callback="callback" :params="params"/>');
      //载入页面
      me.vm = new Vue({
        el: dom,
        data: function () {
          return {
            params: vueInfo.params
          }
        },
        methods: {
          callback: vueInfo.callback
        }});
    },true);

  },
  load:function(){
    var vm = this.getVM();
    vm && vm.load && vm.load.apply(vm,arguments);
  },
  getVM:function(){
    return this.vm.$children[0];
  }
})
