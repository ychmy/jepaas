package com.je.table.service;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.je.cache.service.table.DynaCacheManager;
import com.je.core.constants.tree.NodeType;
import com.je.core.facade.extjs.JsonBuilder;
import com.je.core.service.CommonService;
import com.je.core.service.MetaService;
import com.je.core.util.ArrayUtils;
import com.je.core.util.DataBaseUtils;
import com.je.core.util.DateUtils;
import com.je.core.util.StringUtil;
import com.je.core.util.bean.BeanUtils;
import com.je.core.util.bean.DynaBean;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 表信息修改  主要负责 键 列 索引的维护实现类
 */
@Service("tableInfoManager")
public class TableInfoManagerImpl implements TableInfoManager {

    @Autowired
    private TableManager tableManager;
    @Autowired
    private MetaService metaService;
    @Autowired
    private CommonService commonService;

    /**
     * 添加列
     *
     * @param dynaBean
     * @return
     */
    @Override
    public DynaBean addField(DynaBean dynaBean) {
        DynaBean table = metaService.selectOne("JE_CORE_RESOURCETABLE", ConditionsWrapper.builder().eq("RESOURCETABLE_TABLECODE", dynaBean.getStr("TABLECOLUMN_TABLECODE")));
        dynaBean.set("TABLECOLUMN_RESOURCETABLE_ID", table.getStr("JE_CORE_RESOURCETABLE_ID"));
        commonService.buildModelCreateInfo(dynaBean);
        int count = 1;
        List<Map<String, Object>> countInfos = metaService.selectSql("SELECT MAX(SY_ORDERINDEX) ORDERINDEX FROM JE_CORE_TABLECOLUMN WHERE TABLECOLUMN_CLASSIFY='PRO' AND TABLECOLUMN_RESOURCETABLE_ID={0}", dynaBean.getStr("TABLECOLUMN_RESOURCETABLE_ID"));
        if (countInfos != null && countInfos.size() > 0 && countInfos.get(0) != null) {
            String countStr = countInfos.get(0).get("ORDERINDEX") + "";
            if (StringUtil.isNotEmpty(countStr)) {
                count = Integer.parseInt(countStr) + 1;
            }
        }
        dynaBean.set("SY_ORDERINDEX", count);
        commonService.doSave(dynaBean);
        tableManager.updateTable(dynaBean.getStr("TABLECOLUMN_RESOURCETABLE_ID"), true);
        return dynaBean;
    }

    /**
     * 删除列
     *
     * @param dynaBean 自定义动态类
     * @param ids      TODO 暂不明确
     * @param isDll    TODO 咋不明确
     * @return
     */
    @Override
    public Integer removeColumn(DynaBean dynaBean, String ids, Boolean isDll) {
        //table信息
        DynaBean table = metaService.selectOne("JE_CORE_RESOURCETABLE", ConditionsWrapper.builder().eq("RESOURCETABLE_TABLECODE", dynaBean.getStr("TABLECOLUMN_TABLECODE")));
        //视图删除特殊处理
        if("VIEW".equalsIgnoreCase(table.getStr("RESOURCETABLE_TYPE"))){
            return metaService.executeSql("DELETE FROM JE_CORE_TABLECOLUMN WHERE JE_CORE_TABLECOLUMN_ID IN ({0})", Arrays.asList(ids.split(",")));
        }

        List<DynaBean> columns = metaService.select(ConditionsWrapper.builder().table("JE_CORE_TABLECOLUMN").in("JE_CORE_TABLECOLUMN_ID", ids.split(",")));
        deleteCascadeViewColumn(dynaBean.getStr("TABLECOLUMN_TABLECODE"),columns);

        List<DynaBean> keys = new ArrayList<DynaBean>();
        List<DynaBean> indexs = new ArrayList<DynaBean>();
        for (DynaBean column : columns) {
            if ("1".equals(column.getStr("TABLECOLUMN_ISCREATE"))) {
                tableManager.saveTableTrace("JE_CORE_TABLECOLUMN", column, null, "DELETE", column.getStr("TABLECOLUMN_RESOURCETABLE_ID"));
            }
            if ("FOREIGNKEY".equals(column.getStr("TABLECOLUMN_TYPE"))) {
                DynaBean key = metaService.selectOne("JE_CORE_TABLEKEY",
                        ConditionsWrapper.builder()
                                .eq("TABLEKEY_RESOURCETABLE_ID", column.getStr("TABLECOLUMN_RESOURCETABLE_ID"))
                                .eq("TABLEKEY_COLUMNCODE", column.getStr("TABLECOLUMN_CODE")));
                if (key != null) {
                    keys.add(key);
                }
            }
            List<DynaBean> columnIndexs = metaService.select(ConditionsWrapper.builder()
                    .table("JE_CORE_TABLEINDEX")
                    .eq("TABLEINDEX_RESOURCETABLE_ID", column.getStr("TABLECOLUMN_RESOURCETABLE_ID"))
                    .eq("TABLEINDEX_FIELDCODE", column.getStr("TABLECOLUMN_CODE")));
            indexs.addAll(columnIndexs);
        }
        //级联删除key键
        for (DynaBean key : keys) {
            if ("1".equals(key.getStr("TABLEKEY_ISCREATE"))) {
                tableManager.saveTableTrace("JE_CORE_TABLEKEY", key, null, "DELETE", key.getStr("TABLEKEY_RESOURCETABLE_ID"));
            }
        }
        for (DynaBean index : indexs) {
            if ("1".equals(index.getStr("TABLEINDEX_ISCREATE"))) {
                tableManager.saveTableTrace("JE_CORE_TABLEINDEX", index, null, "DELETE", index.getStr("TABLEINDEX_RESOURCETABLE_ID"));
            }
        }
        tableManager.deleteKey(dynaBean.getStr("TABLECOLUMN_TABLECODE"), keys, "1");
        tableManager.deleteIndex(dynaBean.getStr("TABLECOLUMN_TABLECODE"), indexs);
        tableManager.deleteColumn(dynaBean.getStr("TABLECOLUMN_TABLECODE"), columns, isDll);
        BeanUtils.getInstance().clearCache(dynaBean.getStr("TABLECOLUMN_TABLECODE"));
        DynaCacheManager.removeCache(dynaBean.getStr("TABLECOLUMN_TABLECODE"));
        return columns.size();
    }

    @Override
    public void deleteCascadeViewColumn(String tableCode, List<DynaBean> columns) {
        List<DynaBean> cascadeBeans = metaService.select("JE_CORE_VIEWCASCADE",ConditionsWrapper.builder()
                .eq("VIEWCASCADE_YBBM",tableCode)
                .or()
                .eq("VIEWCASCADE_MBBBM",tableCode));
        if(cascadeBeans == null || cascadeBeans.isEmpty()){
            return;
        }

        List<DynaBean> columnList = Lists.newArrayList();
        List<String> viewIdList = Lists.newArrayList();
        List<String> viewCodeList = Lists.newArrayList();
        cascadeBeans.forEach(eachCascadeBean -> {
            viewIdList.add(eachCascadeBean.getStr("JE_CORE_RESOURCETABLE_ID"));
        });

        if(viewIdList.isEmpty()){
            return;
        }

        //查找所有视图
        List<DynaBean> viewList = metaService.select("JE_CORE_RESOURCETABLE",ConditionsWrapper.builder().in("JE_CORE_RESOURCETABLE_ID",viewIdList));
        viewList.forEach(eachViewBean -> {
            viewCodeList.add(eachViewBean.getStr("RESOURCETABLE_TABLECODE"));
        });

        List<DynaBean> viewColumns = metaService.select("JE_CORE_TABLECOLUMN",ConditionsWrapper.builder().in("TABLECOLUMN_RESOURCETABLE_ID",viewIdList));
        columnList.addAll(acquireEqualColumns(tableCode,columns,viewColumns));

        if(columnList.isEmpty()){
            return;
        }

        List<String> columnIdList = Lists.newArrayList();
        List<String> columnCodeList = Lists.newArrayList();
        columnList.forEach(eachViewColumnBean -> {
            columnIdList.add(eachViewColumnBean.getStr("JE_CORE_TABLECOLUMN_ID"));
            columnCodeList.add(eachViewColumnBean.getStr("TABLECOLUMN_CODE"));
        });
        //删除视图的字段
        metaService.delete("JE_CORE_TABLECOLUMN",ConditionsWrapper.builder().in("JE_CORE_TABLECOLUMN_ID",columnIdList));

        //查找使用视图得功能
        List<DynaBean> viewFuncList = metaService.select("JE_CORE_FUNCINFO",ConditionsWrapper.builder().in("FUNCINFO_TABLENAME",viewCodeList));
        List<String> viewFuncIdList = Lists.newArrayList();
        viewFuncList.forEach(eachBean -> {
            viewFuncIdList.add(eachBean.getStr("JE_CORE_FUNCINFO_ID"));
        });
        //删除功能列表的字段
        metaService.delete("JE_CORE_RESOURCECOLUMN",ConditionsWrapper.builder().in("RESOURCECOLUMN_FUNCINFO_ID",viewFuncIdList).in("RESOURCECOLUMN_CODE",columnCodeList));
        //删除功能表单的字段
        metaService.delete("JE_CORE_RESOURCEFIELD",ConditionsWrapper.builder().in("RESOURCEFIELD_FUNCINFO_ID",viewFuncIdList).in("RESOURCEFIELD_CODE",columnCodeList));
    }

    /**
     * 获取删除表字段，对应视图中存在此字段的ID列表
     * 此逻辑是判断资源表视图列中字段值TABLECOLUMN_VIEWCONFIG
     * @param tableCode 资源表编码
     * @param columns 需要删除的列
     * @param viewColumns 视图中存在的列
     * @return
     */
    private List<DynaBean> acquireEqualColumns(String tableCode,List<DynaBean> columns,List<DynaBean> viewColumns){
        List<DynaBean> columnIds = Lists.newArrayList();
        for (DynaBean eachDelColumnBean:columns) {
            for (DynaBean eachViewColumnBean:viewColumns) {
                String viewColumnConfigStr = eachViewColumnBean.getStr("TABLECOLUMN_VIEWCONFIG");
                if(Strings.isNullOrEmpty(viewColumnConfigStr)){
                    continue;
                }
                com.alibaba.fastjson.JSONObject viewColumnObj = JSON.parseObject(viewColumnConfigStr);
                if(tableCode.equals(viewColumnObj.getString("table"))
                        && viewColumnObj.getString("code").equals(eachDelColumnBean.getStr("TABLECOLUMN_CODE"))){
                    columnIds.add(eachViewColumnBean);
                }
            }
        }
        return columnIds;
    }

    /**
     * 增量导入
     *
     * @param dynaBean
     */
    @Override
    public void impNewCols(DynaBean dynaBean) {
        List<DynaBean> tableCols = metaService.select("JE_CORE_TABLECOLUMN",
                ConditionsWrapper.builder()
                        .eq("TABLECOLUMN_RESOURCETABLE_ID", dynaBean.getStr("JE_CORE_RESOURCETABLE_ID")));
        DynaBean currentTable = DataBaseUtils.getTableBaseInfo(dynaBean);
        //当前表中真实存在的列
        List<DynaBean> currentCols = (List<DynaBean>) currentTable.get(BeanUtils.KEY_TABLE_COLUMNS);
        //列表中显示的列
        for (int x = 0; x < currentCols.size(); x++) {
            DynaBean entity = currentCols.get(x);
            boolean flag = true;
            for (int i = 0; i < tableCols.size(); i++) {
                if (entity.getStr("TABLECOLUMN_CODE").equals(tableCols.get(i).getStr("TABLECOLUMN_CODE"))) {
                    flag = false;
                    break;
                }

            }
            if (flag) {
                commonService.doSave(dynaBean);
                metaService.insert(entity);
            }
        }
    }

    /**
     * 字典辅助添加列
     *
     * @param request
     */
    @Override
    public void addColumnByDD(HttpServletRequest request) {
        String ids = request.getParameter("ids");
        String pkValue = request.getParameter("pkValue");
        String ddName = request.getParameter("ddName");
        String ddCode = request.getParameter("ddCode");
        String ddType = request.getParameter("ddType");
        int count = 1;
        List<Map<String, Object>> countInfos = metaService.selectSql("SELECT MAX(SY_ORDERINDEX) ORDERINDEX FROM JE_CORE_TABLECOLUMN WHERE TABLECOLUMN_CLASSIFY='PRO' AND TABLECOLUMN_RESOURCETABLE_ID={0}", pkValue);
        if (countInfos != null && countInfos.size() > 0 && countInfos.get(0) != null) {
            String countStr = countInfos.get(0).get("ORDERINDEX") + "";
            if (StringUtil.isNotEmpty(countStr)) {
                count = Integer.parseInt(countStr) + 1;
            }
        }
        String[] codeArray = ids.split(",");
        String queryField = "";
        Boolean idField = false;
        for (String code : codeArray) {
            if ("CODE".equalsIgnoreCase(code.substring(code.lastIndexOf("_") + 1))) {
                queryField = code;
            }
        }
        if (StringUtil.isEmpty(queryField)) {
            for (String code : codeArray) {
                if ("ID".equalsIgnoreCase(code.substring(code.lastIndexOf("_") + 1))) {
                    queryField = code;
                    idField = true;
                }
            }
        }
        String[] configInfoArray = new String[codeArray.length];
        for (int i = 0; i < codeArray.length; i++) {
            String code = codeArray[i];
            String type = code.substring(code.lastIndexOf("_") + 1);
            if ("name".equalsIgnoreCase(type)) {
                configInfoArray[i] = "text";
            } else if ("code".equalsIgnoreCase(type)) {
                configInfoArray[i] = "code";
            } else if ("id".equalsIgnoreCase(type)) {
                configInfoArray[i] = "id";
            }
        }
        for (String code : codeArray) {
            DynaBean column = new DynaBean("JE_CORE_TABLECOLUMN", false);
            column.set(BeanUtils.KEY_PK_CODE, "JE_CORE_TABLECOLUMN_ID");
            column.set("TABLECOLUMN_CODE", code);
            String type = code.substring(code.lastIndexOf("_") + 1);
            if ("name".equalsIgnoreCase(type)) {
                if (ArrayUtils.contains(new String[]{"DYNA_TREE", "SQL_TREE", "TREE", "SQL", "CUSTOM"}, ddType)) {
                    column.set("TABLECOLUMN_NAME", ddName);
                } else {
                    column.set("TABLECOLUMN_NAME", ddName + "_NAME");
                }
            } else if ("code".equalsIgnoreCase(type)) {
                if (ArrayUtils.contains(new String[]{"DYNA_TREE", "SQL_TREE", "TREE", "SQL", "CUSTOM"}, ddType)) {
                    column.set("TABLECOLUMN_NAME", ddName + "_CODE");
                } else {
                    column.set("TABLECOLUMN_NAME", ddName);
                }
            } else if ("id".equalsIgnoreCase(type)) {
                column.set("TABLECOLUMN_NAME", ddName + "_ID");
            }
            column.set("TABLECOLUMN_TYPE", "VARCHAR50");
            column.set("TABLECOLUMN_UNIQUE", "0");
            column.set("TABLECOLUMN_CLASSIFY", "PRO");
            column.set("TABLECOLUMN_ISCREATE", "0");
            column.set("TABLECOLUMN_ISNULL", "1");
            column.set("TABLECOLUMN_LENGTH", "");
            column.set("TABLECOLUMN_TREETYPE", "NORMAL");
            column.set("TABLECOLUMN_RESOURCETABLE_ID", pkValue);
            String configInfo = StringUtil.buildSplitString(configInfoArray, "~");
            //树形字典
            if (ArrayUtils.contains(new String[]{"DYNA_TREE", "SQL_TREE", "TREE", "SQL", "CUSTOM"}, ddType)) {
                if ("NAME".equalsIgnoreCase(code.substring(code.lastIndexOf("_") + 1))) {
                    column.set("TABLECOLUMN_DICCONFIG", ddCode + "," + ids.replace(",", "~") + "," + configInfo + ",S");
                    column.set("TABLECOLUMN_DICQUERYFIELD", queryField);
                }
            } else {
                if ("CODE".equalsIgnoreCase(code.substring(code.lastIndexOf("_") + 1))) {
                    column.set("TABLECOLUMN_DICCONFIG", ddCode + "," + ids.replace(",", "~") + "," + configInfo + ",S");
                }
            }
            commonService.buildModelCreateInfo(column);
            column.set("SY_ORDERINDEX", count);
            count++;
            metaService.insert(column);
        }
    }

    /**
     * 字典辅助添加列
     *
     * @param request
     */
    @Override
    public void addColumnByDDList(HttpServletRequest request) {
        String strData = request.getParameter("strData");
        String pkValue = request.getParameter("pkValue");
        String whereSql = request.getParameter("whereSql");
        int count = 1;
        List<Map<String, Object>> countInfos = metaService.selectSql("SELECT MAX(SY_ORDERINDEX) ORDERINDEX FROM JE_CORE_TABLECOLUMN WHERE TABLECOLUMN_CLASSIFY='PRO' AND TABLECOLUMN_RESOURCETABLE_ID={0}", pkValue);
        if (countInfos != null && countInfos.size() > 0 && countInfos.get(0) != null) {
            String countStr = countInfos.get(0).get("ORDERINDEX") + "";
            if (StringUtil.isNotEmpty(countStr)) {
                count = Integer.parseInt(countStr) + 1;
            }
        }
        JSONArray fields = JSONArray.fromObject(strData);
        for (int i = 0; i < fields.size(); i++) {
            JSONObject field = fields.getJSONObject(i);
            String type = field.getString("type");
            String name = field.getString("text");
            String code = field.getString("name");
            String configInfo = field.getString("configInfo");
            String otherConfig = field.getString("otherConfig");
            JSONObject config = new JSONObject();
            config.put("other", otherConfig);
            config.put("where", whereSql);
            DynaBean column = new DynaBean("JE_CORE_TABLECOLUMN", true);
            column.set("TABLECOLUMN_NAME", name);
            column.set("TABLECOLUMN_CODE", code);
            column.set("TABLECOLUMN_TYPE", "VARCHAR50");
            column.set("TABLECOLUMN_UNIQUE", "0");
            column.set("TABLECOLUMN_CLASSIFY", "PRO");
            column.set("TABLECOLUMN_ISCREATE", "0");
            column.set("TABLECOLUMN_ISNULL", "1");
            column.set("TABLECOLUMN_LENGTH", "");
            column.set("TABLECOLUMN_TREETYPE", "NORMAL");
            column.set("TABLECOLUMN_RESOURCETABLE_ID", pkValue);
            column.set("TABLECOLUMN_DICCONFIG", configInfo);
            column.set("TABLECOLUMN_DICQUERYFIELD", config.toString());
            commonService.buildModelCreateInfo(column);
            column.set("SY_ORDERINDEX", count);
            count++;
            metaService.insert(column);
        }
    }

    /**
     * 表辅助添加列
     *
     * @param request
     * @return
     */
    @Override
    public Integer addColumnByTable(HttpServletRequest request) {
        String strData = request.getParameter("strData");
        String tableCode = request.getParameter("tableCode");
        String pkValue = request.getParameter("pkValue");
        List<Map> sqlMapList = JsonBuilder.getInstance().fromJsonArray(strData);
        String targerTableCode = request.getParameter("TARGERTABLECODE");
        String createChild = request.getParameter("CREATECHILD");
        int count = 1;
        List<Map<String, Object>> countInfos = metaService.selectSql("SELECT MAX(SY_ORDERINDEX) ORDERINDEX FROM JE_CORE_TABLECOLUMN WHERE TABLECOLUMN_CLASSIFY='PRO' AND TABLECOLUMN_RESOURCETABLE_ID={0}", pkValue);
        if (countInfos != null && countInfos.size() > 0 && countInfos.get(0) != null) {
            String countStr = countInfos.get(0).get("ORDERINDEX") + "";
            if (StringUtil.isNotEmpty(countStr)) {
                count = Integer.parseInt(countStr) + 1;
            }
        }
        for (Map map : sqlMapList) {
            //插入表字段
            DynaBean column = new DynaBean("JE_CORE_TABLECOLUMN", false);
            column.set(BeanUtils.KEY_PK_CODE, "JE_CORE_TABLECOLUMN_ID");
            column.set("TABLECOLUMN_NAME", map.get("TABLECOLUMN_NAME"));
            column.set("TABLECOLUMN_CODE", map.get("TABLECOLUMN_CLASSIFY"));
            column.set("TABLECOLUMN_UNIQUE", "0");
            if ("ID".equals(map.get("TABLECOLUMN_TYPE") + "")) {
                column.set("TABLECOLUMN_TYPE", "VARCHAR");
                column.set("TABLECOLUMN_LENGTH", "50");
            } else {
                column.set("TABLECOLUMN_TYPE", map.get("TABLECOLUMN_TYPE"));
                column.set("TABLECOLUMN_LENGTH", map.get("TABLECOLUMN_LENGTH"));
            }
            column.set("TABLECOLUMN_ISCREATE", "0");
            column.set("TABLECOLUMN_ISNULL", "1");
            column.set("TABLECOLUMN_CLASSIFY", "PRO");
            column.set("TABLECOLUMN_TABLECODE", tableCode);
            column.set("TABLECOLUMN_RESOURCETABLE_ID", pkValue);
            column.set("TABLECOLUMN_TREETYPE", "NORMAL");
            if (map.containsKey("TABLECOLUMN_DICCONFIG")) {
                String dicConfig = map.get("TABLECOLUMN_DICCONFIG") + "";
                if (StringUtil.isNotEmpty(dicConfig)) {
                    for (Map vals : sqlMapList) {
                        dicConfig = dicConfig.replace(vals.get("TABLECOLUMN_CODE") + "", vals.get("TABLECOLUMN_CLASSIFY") + "");
                    }
                    column.setStr("TABLECOLUMN_DICCONFIG", dicConfig);
                }
            }
            if (map.containsKey("TABLECOLUMN_DICQUERYFIELD")) {
                String dicQueryConfig = map.get("TABLECOLUMN_DICQUERYFIELD") + "";
                if (StringUtil.isNotEmpty(dicQueryConfig)) {
                    for (Map vals : sqlMapList) {
                        dicQueryConfig = dicQueryConfig.replace(vals.get("TABLECOLUMN_CODE") + "", vals.get("TABLECOLUMN_CLASSIFY") + "");
                    }
                    column.setStr("TABLECOLUMN_DICQUERYFIELD", dicQueryConfig);
                }
            }
//			if(map.containsKey("TABLECOLUMN_QUERYCONFIG")) {
//				String queryConfig=map.get("TABLECOLUMN_QUERYCONFIG")+"";
//				if(StringUtil.isNotEmpty(queryConfig)) {
//					String[] queryArray=queryConfig.split(",");
//					if(queryArray.length>1){
//						String funcFields=queryArray[1];
//						for(Map vals:sqlMapList){
//							funcFields=funcFields.replace(vals.get("TABLECOLUMN_CODE")+"",vals.get("TABLECOLUMN_CLASSIFY")+"");
//						}
//						queryArray[1]=funcFields;
//					}
//					column.setStr("TABLECOLUMN_QUERYCONFIG", StringUtil.buildSplitString(queryArray, ","));
//				}
//			}
            if ("1".equals(createChild)) {
                column.set("TABLECOLUMN_CHILDCONFIG", targerTableCode + "," + map.get("TABLECOLUMN_CODE"));
            }
            commonService.buildModelCreateInfo(column);
            if ("1".equals(map.get("TABLECOLUMN_ISNULL") + "")) {
                //查询选择配置
                if (StringUtil.isNotEmpty(request.getParameter("FUNCCODE"))) {
                    column.set("TABLECOLUMN_QUERYCONFIG", request.getParameter("FUNCCODE") + "," + request.getParameter("QUERYSTR"));
                }
            }
            //使用外键
            if ("1".equals(map.get("TABLECOLUMN_ISCREATE") + "")) {
                column.set("TABLECOLUMN_TYPE", "FOREIGNKEY");
                column.set("TABLECOLUMN_LENGTH", "");
                //生成外键
                DynaBean forenignKey = new DynaBean("JE_CORE_TABLEKEY", false);
                forenignKey.set(BeanUtils.KEY_PK_CODE, "JE_CORE_TABLEKEY_ID");
                String nowDateTime = DateUtils.formatDateTime(new Date());
                nowDateTime = nowDateTime.replaceAll("-", "");
                nowDateTime = nowDateTime.replaceAll(" ", "");
                nowDateTime = nowDateTime.replaceAll(":", "");
                forenignKey.set("TABLEKEY_CODE", "JE_" + nowDateTime);
                forenignKey.set("TABLEKEY_COLUMNCODE", column.get("TABLECOLUMN_CODE"));
                forenignKey.set("TABLEKEY_TYPE", "Foreign");
                forenignKey.set("TABLEKEY_CHECKED", "1");
                forenignKey.set("TABLEKEY_LINKTABLE", targerTableCode);
                forenignKey.set("TABLEKEY_LINECOLUMNCODE", map.get("TABLECOLUMN_CODE"));
                forenignKey.set("TABLEKEY_LINETYLE", "Cascade");
                forenignKey.set("TABLEKEY_ISRESTRAINT", "1");
                forenignKey.set("TABLEKEY_RESOURCETABLE_ID", pkValue);
                forenignKey.set("TABLEKEY_TABLECODE", tableCode);
                forenignKey.set("TABLEKEY_ISCREATE", "0");
                forenignKey.set("TABLEKEY_CLASSIFY", "PRO");
                forenignKey.set("SY_ORDERINDEX", 1);
                commonService.buildModelCreateInfo(forenignKey);
                metaService.insert(forenignKey);
            }
            column.set("SY_ORDERINDEX", count);
            count++;
            metaService.insert(column);
        }
        return sqlMapList.size();
    }

    /**
     * 原子辅助添加列
     *
     * @param strData   TODO 暂不明确
     * @param tableCode 表编码
     * @param pkValue   主键
     * @return
     */
    @Override
    public Integer addColumnByAtom(String strData, String tableCode, String pkValue) {
        List<Map> sqlMapList = JsonBuilder.getInstance().fromJsonArray(strData);
        int count = 1;
        List<Map<String, Object>> countInfos = metaService.selectSql("SELECT MAX(SY_ORDERINDEX) ORDERINDEX FROM JE_CORE_TABLECOLUMN WHERE TABLECOLUMN_CLASSIFY='PRO' AND TABLECOLUMN_RESOURCETABLE_ID={0}", pkValue);
        if (countInfos != null && countInfos.size() > 0 && countInfos.get(0) != null) {
            String countStr = countInfos.get(0).get("ORDERINDEX") + "";
            if (StringUtil.isNotEmpty(countStr)) {
                count = Integer.parseInt(countStr) + 1;
            }
        }
        for (Map map : sqlMapList) {
            //插入表字段
            DynaBean column = new DynaBean("JE_CORE_TABLECOLUMN", false);
            column.set(BeanUtils.KEY_PK_CODE, "JE_CORE_TABLECOLUMN_ID");
            column.set("TABLECOLUMN_NAME", map.get("ATOMCOLUMN_NAME"));
            column.set("TABLECOLUMN_CODE", map.get("ATOMCOLUMN_CONTEXT"));
            column.set("TABLECOLUMN_UNIQUE", "0");
            column.set("TABLECOLUMN_TYPE", map.get("ATOMCOLUMN_TYPE"));
            column.set("TABLECOLUMN_LENGTH", map.get("ATOMCOLUMN_LENGTH"));
            column.set("TABLECOLUMN_ISCREATE", "0");
            column.set("TABLECOLUMN_ISNULL", "1");
            column.set("TABLECOLUMN_CLASSIFY", "PRO");
            column.set("TABLECOLUMN_TABLECODE", tableCode);
            column.set("TABLECOLUMN_RESOURCETABLE_ID", pkValue);
            column.set("TABLECOLUMN_TREETYPE", "NORMAL");
            column.set("SY_ORDERINDEX", count);
            count++;
            commonService.buildModelCreateInfo(column);
            metaService.insert(column);
        }
        return sqlMapList.size();
    }

    /**
     * 存为原子
     *
     * @param strData
     * @param pkValue 主键
     * @return
     */
    @Override
    public Integer addAtomByColumn(String strData, String pkValue) {
        List<Map> sqlMapList = JsonBuilder.getInstance().fromJsonArray(strData);
        for (Map map : sqlMapList) {
            DynaBean atomColumn = new DynaBean("JE_CORE_ATOMCOLUMN", false);
            atomColumn.set(BeanUtils.KEY_TABLE_CODE, "JE_CORE_ATOMCOLUMN");
            atomColumn.set("ATOMCOLUMN_CODE", map.get("ATOMCOLUMN_CONTEXT"));
            atomColumn.set("ATOMCOLUMN_ISNULL", map.get("ATOMCOLUMN_ISNULL"));
            atomColumn.set("ATOMCOLUMN_NAME", map.get("ATOMCOLUMN_NAME"));
            atomColumn.set("ATOMCOLUMN_ATOM_ID", pkValue);
            atomColumn.set("ATOMCOLUMN_LENGTH", map.get("ATOMCOLUMN_LENGTH"));
            atomColumn.set("ATOMCOLUMN_TYPE", map.get("ATOMCOLUMN_TYPE"));
            commonService.buildModelCreateInfo(atomColumn);
            metaService.insert(atomColumn);
        }
        return sqlMapList.size();
    }

    /**
     * 删除索引
     *
     * @param dynaBean 自定义动态类
     * @param ids      索引ID列表
     * @return
     */
    @Override
    public Integer removeIndex(DynaBean dynaBean, String ids) {
        List<DynaBean> indexs = metaService.select(ConditionsWrapper.builder()
                .table("JE_CORE_TABLEINDEX")
                .in("JE_CORE_TABLEINDEX_ID", ids.split(",")));
        for (DynaBean index : indexs) {
            if ("1".equals(index.getStr("TABLEINDEX_ISCREATE"))) {
                tableManager.saveTableTrace("JE_CORE_TABLEINDEX", index, null, "DELETE", index.getStr("TABLEINDEX_RESOURCETABLE_ID"));
            }
        }
        tableManager.deleteIndex(dynaBean.getStr("TABLEINDEX_TABLECODE"), indexs);
        BeanUtils.getInstance().clearCache(dynaBean.getStr("TABLEINDEX_TABLECODE"));
        DynaCacheManager.removeCache(dynaBean.getStr("TABLEINDEX_TABLECODE"));
        return indexs.size();
    }

    /**
     * 删除键
     *
     * @param dynaBean 自定义动态类
     * @param ids      ID列表
     * @param ddl      执行的DDL
     * @return
     */
    @Override
    public Integer removeKey(DynaBean dynaBean, String ids, String ddl) {
        List<DynaBean> keys = metaService.select(ConditionsWrapper.builder()
                .table("JE_CORE_TABLEKEY")
                .in("JE_CORE_TABLEKEY_ID", ids.split(",")));
        for (DynaBean key : keys) {
            if ("1".equals(key.getStr("TABLEKEY_ISCREATE"))) {
                tableManager.saveTableTrace("JE_CORE_TABLEKEY", key, null, "DELETE", key.getStr("TABLEKEY_RESOURCETABLE_ID"));
            }
        }
        tableManager.deleteKey(dynaBean.getStr("TABLEKEY_TABLECODE"), keys, ddl);
        BeanUtils.getInstance().clearCache(dynaBean.getStr("TABLEKEY_TABLECODE"));
        DynaCacheManager.removeCache(dynaBean.getStr("TABLEKEY_TABLECODE"));
        return keys.size();
    }

    /**
     * 更新树形表数据(路径 层次 顺序 排序字段)
     *
     * @param tableCode 表编码
     * @param pkCode    主键code
     * @param preFix    字段前缀
     */
    @Override
    public void syncTreePath(String tableCode, String pkCode, String preFix) {
        syncPath(tableCode, pkCode, preFix, "ROOT");
    }

    private void syncPath(String tableCode, String pkCode, String preFix, String parentId) {
        String parentField = preFix + "PARENT";
        String pathField = preFix + "PATH";
        String parentPathField = preFix + "PARENTPATH";
        String treeOrderField = preFix + "TREEORDERINDEX";
        String layerField = preFix + "LAYER";
        String nodeTypeField = preFix + "NODETYPE";
        String orderField = preFix + "ORDERINDEX";
        String[] fields = new String[]{pkCode, parentField, pathField, parentPathField, treeOrderField, layerField, nodeTypeField, orderField};
        String querySql = " SELECT " + StringUtil.buildSplitString(fields, ",") + " FROM " + tableCode + " WHERE " + parentField + "='" + parentId + "' ORDER BY " + orderField;
        if (NodeType.ROOT.equalsIgnoreCase(parentId)) {
            metaService.executeSql(" UPDATE " + tableCode + " SET "
                    + nodeTypeField + "={0},"
                    + pathField + "={1},"
                    + layerField + "={2},"
                    + treeOrderField + "={3},"
                    + parentPathField + "='' WHERE "
                    + pkCode + "={4}", "ROOT", "/ROOT", "0", "000001", parentId);
        }

        List<Map<String, Object>> parentNodes = metaService.selectSql("SELECT " + StringUtil.buildSplitString(fields, ",") + " FROM " + tableCode + " WHERE " + pkCode + "={0}", parentId);
        if (parentNodes.size() < 0) return;
        Map parent = parentNodes.get(0);
        List<Map<String, Object>> lists = metaService.selectSql(querySql);
        int index = 1;
        for (Map vals : lists) {
            metaService.executeSql("UPDATE " + tableCode + " SET "
                            + nodeTypeField + "={0},"
                            + pathField + "={1},"
                            + layerField + "={2},"
                            + treeOrderField + "={3},"
                            + parentPathField + "={4} WHERE "
                            + pkCode + "={5}",
                    NodeType.LEAF,
                    (parent.get(pathField) + "/" + vals.get(pkCode)),
                    (Integer.parseInt(parent.get(layerField) + "") + 1),
                    (parent.get(treeOrderField) + StringUtil.preFillUp(index + "", 6, '0')),
                    parent.get(pathField),
                    vals.get(pkCode));
            index++;
            String id = vals.get(pkCode) + "";
            syncPath(tableCode, pkCode, preFix, id);
        }
        if (lists.size() > 0 && !NodeType.ROOT.equalsIgnoreCase(parentId)) {
            metaService.executeSql(" UPDATE " + tableCode + " SET "
                    + nodeTypeField + "={0} WHERE " + pkCode + "={1}", NodeType.GENERAL, parentId);
        }
    }

}
