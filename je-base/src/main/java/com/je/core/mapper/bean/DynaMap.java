package com.je.core.mapper.bean;

import com.alibaba.fastjson.JSON;
import com.je.ibatis.extension.toolkit.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * DynaMap
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/12/6
 */
public class DynaMap extends ConcurrentHashMap<String, Object> {

    private Logger logger = LoggerFactory.getLogger(getClass());

    public DynaMap() {
    }

    public DynaMap(Map map) {
        super.clear();
        super.putAll(map);
    }

    public DynaMap table(String tableCode) {
        super.put(Constants.KEY_TABLE_CODE, tableCode);
        return this;
    }

    /**
     * 设置字段值
     *
     * @param column 列名
     * @param value  值
     * @return com.je.core.mapper.bean.DynaMap
     */
    public DynaMap set(String column, Object value) {
        put(column, value);
        return this;
    }

    /**
     * 设置字段值 字符串
     *
     * @param column 列名
     * @param value  值
     * @return com.je.core.mapper.bean.DynaMap
     */
    public DynaMap setStr(String column, String value) {
        set(column, value);
        return this;
    }

    /**
     * 获取字段值
     *
     * @param column 字段名
     * @return 字段值
     */
    public Object get(String column) {
        return get(column, null);
    }

    /**
     * 获取字段值
     *
     * @param column       字段名
     * @param defaultValue 字段默认值
     * @return 字段值
     */
    public Object get(String column, Object defaultValue) {
        return getOrDefault(column, defaultValue);
    }

    /**
     * 获取字段值 字符串
     *
     * @param column 字段名
     * @return 字段值
     */
    public String getStr(String column) {
        return getStr(column, null);
    }

    /**
     * 获取字段值 字符串
     *
     * @param column       字段名
     * @param defaultValue 字段默认值
     * @return 字段值
     */
    public String getStr(String column, String defaultValue) {
        Object value = get(column, defaultValue);
        if (value == null) {
            return defaultValue;
        }
        return String.valueOf(value);
    }

    /**
     * 获取字段值 Integer
     *
     * @param column 字段名
     * @return 字段值
     */
    public Integer getInt(String column) {
        return getInt(column, null);
    }

    /**
     * 获取字段值 Integer
     *
     * @param column       字段名
     * @param defaultValue 字段默认值
     * @return 字段值
     */
    public Integer getInt(String column, Integer defaultValue) {
        String value = getStr(column);
        if (value == null) {
            return defaultValue;
        }
        return Integer.valueOf(value);
    }

    /**
     * 获取字段值 Long
     *
     * @param column 字段名
     * @return 字段值
     */
    public Long getLong(String column) {
        return getLong(column, null);
    }

    /**
     * 获取字段值 Long
     *
     * @param column       字段名
     * @param defaultValue 字段默认值
     * @return 字段值
     */
    public Long getLong(String column, Long defaultValue) {
        String value = getStr(column);
        if (value == null) {
            return defaultValue;
        }
        return Long.valueOf(value);
    }

    /**
     * 获取字段值 Double
     *
     * @param column 字段名
     * @return 字段值
     */
    public Double getDouble(String column) {
        return getDouble(column, null);
    }

    /**
     * 获取字段值 Double
     *
     * @param column       字段名
     * @param defaultValue 字段默认值
     * @return 字段值
     */
    public Double getDouble(String column, Double defaultValue) {
        String value = getStr(column);
        if (value == null) {
            return defaultValue;
        }
        return Double.valueOf(value);
    }

    @Override
    public Object put(String key, Object value) {
        //英文+数字+下划线
        String columnRegex = "^\\w+$";
        //校验是否匹配规则
        if (key != null && !key.trim().matches(columnRegex)) {
            logger.error("column '{}' is invalid.", key);
            key = key.trim();
        }
        assert key != null;
        return super.put(key, value);
    }

    @Override
    public void putAll(Map<? extends String, ? extends Object> m) {
        logger.warn("Method putAll is prohibited.");
    }

    @Override
    public Object putIfAbsent(String key, Object value) {
        logger.warn("Method putIfAbsent is prohibited.");
        return null;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}