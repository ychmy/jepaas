package com.je.paas.document.vo;

import com.je.paas.document.model.bean.BasePojo;

import java.util.Date;

public class DiskViewRelationPowerVO extends BasePojo {

    private Long relationId;
    private Long relationRoleId;
    private Long relationPowerId;
    private String relationBusType;
    private Long relationBusId;
    private Date relationCreateTime;
    private String relationCreateUser;
    private String relationCreateUserName;
    private Date relationModifiedTime;
    private String relationModifiedUser;
    private String relationModifiedUserName;
    private Boolean relationIsDeleted;
    private Long powerId;
    private String powerName;
    private String powerCode;
    private Date powerCreateTime;
    private String powerCreateUser;
    private String powerCreateUserName;
    private Date powerModifiedTime;
    private String powerModifiedUser;
    private String powerModifiedUserName;
    private Boolean powerIsDeleted;

    public Long getRelationId() {
        return relationId;
    }

    public void setRelationId(Long relationId) {
        this.relationId = relationId;
    }

    public Long getRelationRoleId() {
        return relationRoleId;
    }

    public void setRelationRoleId(Long relationRoleId) {
        this.relationRoleId = relationRoleId;
    }

    public Long getRelationPowerId() {
        return relationPowerId;
    }

    public void setRelationPowerId(Long relationPowerId) {
        this.relationPowerId = relationPowerId;
    }

    public String getRelationBusType() {
        return relationBusType;
    }

    public void setRelationBusType(String relationBusType) {
        this.relationBusType = relationBusType;
    }

    public Long getRelationBusId() {
        return relationBusId;
    }

    public void setRelationBusId(Long relationBusId) {
        this.relationBusId = relationBusId;
    }

    public Date getRelationCreateTime() {
        return relationCreateTime;
    }

    public void setRelationCreateTime(Date relationCreateTime) {
        this.relationCreateTime = relationCreateTime;
    }

    public String getRelationCreateUser() {
        return relationCreateUser;
    }

    public void setRelationCreateUser(String relationCreateUser) {
        this.relationCreateUser = relationCreateUser;
    }

    public String getRelationCreateUserName() {
        return relationCreateUserName;
    }

    public void setRelationCreateUserName(String relationCreateUserName) {
        this.relationCreateUserName = relationCreateUserName;
    }

    public Date getRelationModifiedTime() {
        return relationModifiedTime;
    }

    public void setRelationModifiedTime(Date relationModifiedTime) {
        this.relationModifiedTime = relationModifiedTime;
    }

    public String getRelationModifiedUser() {
        return relationModifiedUser;
    }

    public void setRelationModifiedUser(String relationModifiedUser) {
        this.relationModifiedUser = relationModifiedUser;
    }

    public String getRelationModifiedUserName() {
        return relationModifiedUserName;
    }

    public void setRelationModifiedUserName(String relationModifiedUserName) {
        this.relationModifiedUserName = relationModifiedUserName;
    }

    public Boolean getRelationIsDeleted() {
        return relationIsDeleted;
    }

    public void setRelationIsDeleted(Boolean relationIsDeleted) {
        this.relationIsDeleted = relationIsDeleted;
    }

    public Long getPowerId() {
        return powerId;
    }

    public void setPowerId(Long powerId) {
        this.powerId = powerId;
    }

    public String getPowerName() {
        return powerName;
    }

    public void setPowerName(String powerName) {
        this.powerName = powerName;
    }

    public String getPowerCode() {
        return powerCode;
    }

    public void setPowerCode(String powerCode) {
        this.powerCode = powerCode;
    }

    public Date getPowerCreateTime() {
        return powerCreateTime;
    }

    public void setPowerCreateTime(Date powerCreateTime) {
        this.powerCreateTime = powerCreateTime;
    }

    public String getPowerCreateUser() {
        return powerCreateUser;
    }

    public void setPowerCreateUser(String powerCreateUser) {
        this.powerCreateUser = powerCreateUser;
    }

    public String getPowerCreateUserName() {
        return powerCreateUserName;
    }

    public void setPowerCreateUserName(String powerCreateUserName) {
        this.powerCreateUserName = powerCreateUserName;
    }

    public Date getPowerModifiedTime() {
        return powerModifiedTime;
    }

    public void setPowerModifiedTime(Date powerModifiedTime) {
        this.powerModifiedTime = powerModifiedTime;
    }

    public String getPowerModifiedUser() {
        return powerModifiedUser;
    }

    public void setPowerModifiedUser(String powerModifiedUser) {
        this.powerModifiedUser = powerModifiedUser;
    }

    public String getPowerModifiedUserName() {
        return powerModifiedUserName;
    }

    public void setPowerModifiedUserName(String powerModifiedUserName) {
        this.powerModifiedUserName = powerModifiedUserName;
    }

    public Boolean getPowerIsDeleted() {
        return powerIsDeleted;
    }

    public void setPowerIsDeleted(Boolean powerIsDeleted) {
        this.powerIsDeleted = powerIsDeleted;
    }
}
