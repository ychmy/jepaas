package com.je.thrid.importsource;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.je.core.util.SpringContextHolder;
import com.je.core.util.bean.DynaBean;
import com.je.paas.document.model.bo.FileBO;
import com.je.paas.document.service.DocumentBusService;

import cn.hutool.core.io.IoUtil;
import cn.hutool.poi.excel.ExcelReader;

/**
 * @program: je-platform
 * @author: LIULJ
 * @create: 2020-07-13 18:23
 * @description: Excel导入源
 */
public class ExcelImportSource extends AbstractDynaBeanImportSource {

    /**
     * 文件唯一键
     */
    private String fileKey;
    /**
     * 字段名称开始行
     */
    private int columnNameStartLine = 1;
    /**
     * 字段值开始行
     */
    private int columnValueStartLine = 2;
    /**
     * 值完结得列索引
     */
    private int columnEndIndex = 0;
    /**
     * sheet页
     */
    private String sheetIndex = "sheet1";

    public ExcelImportSource(String fileKey) {
        this.fileKey = fileKey;
    }

    public ExcelImportSource(String fileKey, int columnNameStartLine, int columnValueStartLine) {
        this.fileKey = fileKey;
        this.columnNameStartLine = columnNameStartLine;
        this.columnValueStartLine = columnValueStartLine;
    }

    public ExcelImportSource(String fileKey, int columnNameStartLine, int columnValueStartLine, String sheetIndex) {
        this.fileKey = fileKey;
        this.columnNameStartLine = columnNameStartLine;
        this.columnValueStartLine = columnValueStartLine;
        this.sheetIndex = sheetIndex;
    }

    @Override
    public List<DynaBean> read() {
        DocumentBusService documentBusService = SpringContextHolder.getBean(DocumentBusService.class);
        FileBO fileBO = documentBusService.readFile(fileKey);
        InputStream inputStream = fileBO.getFile();
        byte[] bytes = IoUtil.readBytes(inputStream);
        // 读取文件
        ExcelReader reader = new ExcelReader(IoUtil.toStream(bytes), sheetIndex, true);
        List<List<Object>> lists = new ArrayList<>();
        lists = reader.read(columnNameStartLine - 1);
        int count = lists.size();
        if (count == 0) {
            throw new RuntimeException(sheetIndex+"页没有找到数据，请检查Excel表Sheet名称或数据！");
        }
        List<Object> columnNames = lists.get(0);
        lists.remove(columnNames);
        List<DynaBean> beans = new ArrayList<>();
        for (List<Object> list : lists) {
            DynaBean bean = new DynaBean();
            for (int i = 0; i < list.size(); i++) {
                Object name = columnNames.get(i);
                Object value = list.get(i);
                if (Objects.isNull(name) || Objects.isNull(value)) {
                    throw new RuntimeException(i + "行数据异常，请检查");
                }
                bean.set(String.valueOf(name), value);
            }
            beans.add(bean);
        }
        return beans;
    }

    public String getFileKey() {
        return fileKey;
    }

    public void setFileKey(String fileKey) {
        this.fileKey = fileKey;
    }

    public int getColumnNameStartLine() {
        return columnNameStartLine;
    }

    public void setColumnNameStartLine(int columnNameStartLine) {
        this.columnNameStartLine = columnNameStartLine;
    }

    public int getColumnValueStartLine() {
        return columnValueStartLine;
    }

    public void setColumnValueStartLine(int columnValueStartLine) {
        this.columnValueStartLine = columnValueStartLine;
    }

    public int getColumnEndIndex() {
        return columnEndIndex;
    }

    public void setColumnEndIndex(int columnEndIndex) {
        this.columnEndIndex = columnEndIndex;
    }

    public String getSheetIndex() {
        return sheetIndex;
    }

    public void setSheetIndex(String sheetIndex) {
        this.sheetIndex = sheetIndex;
    }
}
