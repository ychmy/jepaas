package com.je.core.entity.extjs;

import java.io.Serializable;

/**
 * Extjs Model 总信息类
 * @author yanfabu
 * 2012-1-6 下午03:48:35
 */
public class ModelInfo implements Serializable {

	private static final long serialVersionUID = -1627658453924962809L;

	private String models;
	private String excludes;
	public String getModels() {
		return models;
	}
	public void setModels(String models) {
		this.models = models;
	}
	public String getExcludes() {
		return excludes;
	}
	public void setExcludes(String excludes) {
		this.excludes = excludes;
	}
	
}
