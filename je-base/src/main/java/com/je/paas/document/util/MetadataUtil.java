package com.je.paas.document.util;

import com.alibaba.fastjson.JSONObject;
import com.je.core.util.SecurityUserHolder;
import com.je.paas.document.model.MetadataEnum;
import org.apache.commons.lang3.StringUtils;

/**
 * MetadataUtil
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/9/26
 */
public class MetadataUtil {

    public static JSONObject setCreateMetadata() {
        JSONObject metadata = new JSONObject();
        return setCreateMetadata(metadata);
    }

    /**
     * 设置创建人信息
     *
     * @param metadata 元数据对象
     */
    public static JSONObject setCreateMetadata(JSONObject metadata) {

        //补充业务元数据
        if (StringUtils.isNotEmpty(SecurityUserHolder.getCurrentUser().getZhId())) {
            metadata.put(MetadataEnum.tenantId.getCode(), SecurityUserHolder.getCurrentUser().getZhId());
        }
        if (StringUtils.isNotEmpty(SecurityUserHolder.getCurrentUser().getUsername())) {
            metadata.put(MetadataEnum.createUser.getCode(), SecurityUserHolder.getCurrentUser().getUserId());
        }
        if (StringUtils.isNotEmpty(SecurityUserHolder.getCurrentUser().getUsername())) {
            metadata.put(MetadataEnum.createUserName.getCode(), SecurityUserHolder.getCurrentUser().getUsername());
        }
        if (StringUtils.isNotEmpty(SecurityUserHolder.getCurrentUser().getDeptId())) {
            metadata.put(MetadataEnum.createUserDept.getCode(), SecurityUserHolder.getCurrentUser().getDeptId());
        }
        if (StringUtils.isNotEmpty(SecurityUserHolder.getCurrentUser().getDeptName())) {
            metadata.put(MetadataEnum.createUserDeptName.getCode(), SecurityUserHolder.getCurrentUser().getDeptName());
        }
        return metadata;
    }
}