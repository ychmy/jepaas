package com.je.disconf.controller;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.je.core.mapper.MetaMapper;
import com.je.core.util.SpringContextHolder;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 平台公共接口实现
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/12/6
 */
@Controller
@RequestMapping(value = "/je/disconf")
public class DisConfController {

    @Autowired
    private MetaMapper mapper;

    @RequestMapping(value = "/push", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public void loadAllTable() {
        List<String> tableCodes = rbacTables();

        //获取mapper实例
        //获取所有资源表
        List<Map<String, Object>> tables = mapper.selectSql(null, ConditionsWrapper.builder()
                .apply("select * from JE_CORE_RESOURCETABLE where ")//RESOURCETABLE_TYPE != 'MODULE' AND RESOURCETABLE_TABLECODE = 'JE_CORE_ENDUSER'
                .ne("RESOURCETABLE_TYPE", "MODULE")
                .in("RESOURCETABLE_TABLECODE", tableCodes));
        if (tables != null && !tables.isEmpty()) {
            tables.forEach(table -> {
                //列
                List<Map<String, Object>> columns = mapper.selectSql(null, ConditionsWrapper.builder().apply("select * from JE_CORE_TABLECOLUMN where ")
                        .eq("TABLECOLUMN_RESOURCETABLE_ID", table.get("JE_CORE_RESOURCETABLE_ID"))
                        .eq("TABLECOLUMN_ISCREATE", "1").orderByAsc("TABLECOLUMN_CLASSIFY", "TABLECOLUMN_CODE"));
                //主键外键
                List<Map<String, Object>> keys = mapper.selectSql(null, ConditionsWrapper.builder().apply("select * from JE_CORE_TABLEKEY where ")
                        .eq("TABLEKEY_RESOURCETABLE_ID", table.get("JE_CORE_RESOURCETABLE_ID"))
                        .eq("TABLEKEY_ISCREATE", "1").orderByAsc("SY_ORDERINDEX"));
                //索引
                List<Map<String, Object>> indexs = mapper.selectSql(null, ConditionsWrapper.builder().apply("select * from JE_CORE_TABLEINDEX where ")
                        .eq("TABLEINDEX_RESOURCETABLE_ID", table.get("JE_CORE_TABLEINDEX"))
                        .eq("TABLEINDEX_ISCREATE", "1").orderByAsc("SY_ORDERINDEX"));

                table.put("__columns", columns);
                table.put("__keys", keys);
                table.put("__indexs", indexs);
                String json = JSON.toJSONString(table);
                sendConfig(table.get("RESOURCETABLE_TABLECODE").toString(), json);
            });
        }
    }

    private List<String> rbacTables() {

        List<java.lang.String> tableCodes = new ArrayList<>();
        tableCodes.add("je_core_admin_perm    ".toUpperCase().trim());
        tableCodes.add("je_core_admin_user    ".toUpperCase().trim());
        tableCodes.add("je_core_admininfo     ".toUpperCase().trim());
        tableCodes.add("je_core_adminperm     ".toUpperCase().trim());
        tableCodes.add("je_core_cytree        ".toUpperCase().trim());
        tableCodes.add("je_core_department    ".toUpperCase().trim());
        tableCodes.add("je_core_deptreg       ".toUpperCase().trim());
        tableCodes.add("je_core_enduser       ".toUpperCase().trim());
        tableCodes.add("je_core_jdgsinfo      ".toUpperCase().trim());
        tableCodes.add("je_core_publicperm    ".toUpperCase().trim());
        tableCodes.add("je_core_role          ".toUpperCase().trim());
        tableCodes.add("je_core_role_perm     ".toUpperCase().trim());
        tableCodes.add("je_core_role_user     ".toUpperCase().trim());
        tableCodes.add("je_core_rolegroup     ".toUpperCase().trim());
        tableCodes.add("je_core_rolegroup_perm".toUpperCase().trim());
        tableCodes.add("je_core_sentry        ".toUpperCase().trim());
        tableCodes.add("je_core_sentry_user   ".toUpperCase().trim());
        tableCodes.add("je_core_user_cj       ".toUpperCase().trim());
        tableCodes.add("je_core_user_leader   ".toUpperCase().trim());
        tableCodes.add("je_core_userinfo      ".toUpperCase().trim());
        tableCodes.add("je_core_userproxy     ".toUpperCase().trim());
        tableCodes.add("je_core_vdeptuser     ".toUpperCase().trim());
        tableCodes.add("je_core_vdeptuser_im  ".toUpperCase().trim());
        tableCodes.add("je_core_vdeptuserjoin ".toUpperCase().trim());
        tableCodes.add("je_core_vjtgsuser     ".toUpperCase().trim());
        tableCodes.add("je_core_vleader       ".toUpperCase().trim());
        tableCodes.add("je_core_vroleuser     ".toUpperCase().trim());
        tableCodes.add("je_core_vsentryuser   ".toUpperCase().trim());
        tableCodes.add("je_core_workexp       ".toUpperCase().trim());
        tableCodes.add("je_core_workgroup     ".toUpperCase().trim());
        tableCodes.add("je_core_workgroup_user".toUpperCase().trim());
        tableCodes.add("je_core_workgroupnew  ".toUpperCase().trim());
        tableCodes.add("je_core_ywcj          ".toUpperCase().trim());
        tableCodes.add("je_cp_dept            ".toUpperCase().trim());
        tableCodes.add("je_cp_deptuser        ".toUpperCase().trim());
        tableCodes.add("je_cp_kkan            ".toUpperCase().trim());
        tableCodes.add("je_cp_news            ".toUpperCase().trim());
        tableCodes.add("je_cp_oaxx            ".toUpperCase().trim());
        tableCodes.add("je_cp_user            ".toUpperCase().trim());
        tableCodes.add("je_mp_user            ".toUpperCase().trim());
        tableCodes.add("v_common_user         ".toUpperCase().trim());
        tableCodes.add("v_dept_cjuser         ".toUpperCase().trim());
        tableCodes.add("v_workgroup_user      ".toUpperCase().trim());
        return tableCodes;
    }

    private void sendConfig(String tableCode, String json) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("app", "jeboot-rbac");
        paramMap.put("env", "local");
        paramMap.put("version", "v1.0.0");
        paramMap.put("configure_code", "business_configure");
        paramMap.put("configure_name", "业务配置");
        paramMap.put("business_type_code", "tableCache");
        paramMap.put("business_type_name", "资源表");
        paramMap.put("fileName", tableCode);
        paramMap.put("fileContent", json);
        String post = HttpUtil.post("http://192.168.20.18:9999/api/config/uploadBusinessConfig", paramMap);
        System.out.println(String.format("%s\\t%s", tableCode, post));
    }


}