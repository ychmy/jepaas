package com.project.demo.controller;

import com.google.common.collect.Lists;
import com.je.core.base.AbstractPlatformController;
import com.je.core.base.MethodArgument;
import com.je.core.exception.PlatformException;
import com.je.core.exception.PlatformExceptionEnum;
import com.je.core.result.BaseRespResult;
import com.je.core.util.bean.DynaBean;
import com.je.ibatis.extension.plugins.pagination.Page;
import com.project.demo.service.DemoDictService;
import com.project.demo.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * 重写父类 AbstractPlatformController 的增删改查方法，在原来封装的方法上加需要的业务逻辑
 * 注意事项：controller请求路径“/je/demoDyna”要与功能配置里面Action项对应，增删改查方法页面不做操作，后台只需继承 AbstractPlatformController 重写父类方法即可。
 * 基本的五个方法：load，doSave，doUpdate，doRemove，getInfoById
 * 本次事例只在增删改的方法上增加其他的业务逻辑处理
 */
@Controller
@RequestMapping(value = "/je/demoDyna")
public class DemoDynaBeanController extends AbstractPlatformController {
    @Autowired
    private DemoService demoService;

    /**
     * 重写保存方法
     *
     * @param param
     */
    @RequestMapping(value = "/doSave", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    @Override
    public BaseRespResult doSave(MethodArgument param) {

        try {
            return BaseRespResult.successResult(demoService.doSave(param).getValues());
        } catch (PlatformException e) {
            throw e;
        } catch (Exception e) {
            throw new PlatformException("数据保存失败！", PlatformExceptionEnum.UNKOWN_ERROR, e);
        }
    }


    /**
     * 重写修改方法
     *
     * @param param 请求参数封装对象
     */
    @RequestMapping(value = "/doUpdate", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    @Override
    public BaseRespResult doUpdate(MethodArgument param) {

        try {
            return BaseRespResult.successResult(manager.doUpdate(param).getValues());
        } catch (PlatformException e) {
            throw e;
        } catch (Exception e) {
            throw new PlatformException("数据修改失败！", PlatformExceptionEnum.UNKOWN_ERROR, e);
        }
    }


    /**
     * 重写删除方法(真实删除)
     *
     * @param param
     */
    @RequestMapping(value = "/doRemove", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    @Override
    public BaseRespResult doRemove(MethodArgument param) {

        try {
            int i = manager.doRemove(param);
            return BaseRespResult.successResult(String.format("%s 条记录被删除", i));
        } catch (PlatformException e) {
            throw e;
        } catch (Exception e) {
            throw new PlatformException("数据删除失败！", PlatformExceptionEnum.UNKOWN_ERROR, e);
        }
    }


    /**
     * 根据主键获取
     *
     * @param param 请求参数封装对象
     */
    @RequestMapping(value = "/getInfoById", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    @Override
    public Map getInfoById(MethodArgument param) {

        try {
            DynaBean infoById = manager.getInfoById(param);
            if (infoById == null) {
                return new HashMap<>();
            }
            return infoById.getValues();
        } catch (PlatformException e) {
            throw e;
        } catch (Exception e) {
            throw new PlatformException("数据加载失败！", PlatformExceptionEnum.UNKOWN_ERROR, e);
        }

    }


    /**
     * 默认的列表读取
     *
     * @param param 请求参数封装对象
     */
    @RequestMapping(value = "/load", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    @Override
    public void load(MethodArgument param) {
        try {
            Page page = manager.load(param);
            toWrite(BaseRespResult.successResultPage(Lists.newArrayList(), 0L), param);
        } catch (PlatformException e) {
            throw e;
        } catch (Exception e) {
            throw new PlatformException("数据加载失败！", PlatformExceptionEnum.UNKOWN_ERROR, e);
        }
    }


}
