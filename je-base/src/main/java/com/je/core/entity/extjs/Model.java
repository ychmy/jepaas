package com.je.core.entity.extjs;

import java.io.Serializable;

/**
 * Extjs4.0的Model
 * @author YUNFENCGHENG
 * 2011-12-30 下午01:19:33
 */
public class Model implements Serializable {

	private static final long serialVersionUID = 6687931753404976495L;

	private String name;
	private String type;

	public Model(String name, String type) {
		this.name = name;
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
