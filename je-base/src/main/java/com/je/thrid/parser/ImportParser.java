package com.je.thrid.parser;


import com.je.thrid.vo.ParseVo;
import net.sf.json.JSONObject;

/**
 * @program: je-platform
 * @author: LIULJ
 * @create: 2020-07-13 18:47
 * @description: 导入解析器
 */
public interface ImportParser<T,U> {

    /**
     * 解析操作
     * @param u
     * @return
     */
    ParseVo parse(U u, JSONObject customParameters);

}
