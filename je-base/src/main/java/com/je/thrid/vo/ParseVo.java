package com.je.thrid.vo;

import com.je.core.util.bean.DynaBean;

/**
 * @author 格式化bean返回VO
 * 
 *         insert 参数判断是否需要创建，有可能初始化的时候就需要进行创建
 */

public class ParseVo {
    /**
     * 创建的Bean
     */
    public DynaBean dynaBean;
    /**
     * 是否创建
     */
    private Boolean insert;

    public ParseVo(DynaBean dynaBean) {
        this.dynaBean = dynaBean;
        this.insert = true;
    }

    public ParseVo(DynaBean dynaBean, Boolean insert) {
        this.dynaBean = dynaBean;
        this.insert = insert;
    }

    public DynaBean getDynaBean() {
        return dynaBean;
    }

    public void setDynaBean(DynaBean dynaBean) {
        this.dynaBean = dynaBean;
    }

    public void setInsert(Boolean insert) {
        this.insert = insert;
    }

    public Boolean isInsert() {
        return insert;
    }
}
