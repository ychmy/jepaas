package com.project.demo.service;

import com.je.core.base.MethodArgument;
import com.je.core.util.bean.DynaBean;
import com.je.wf.processVo.WfEventSubmitInfo;

public interface DemoService {

    /**
     * 流程事件
     *
     * @param eventInfo 事件执行VO，可以获取业务bean
     */
    public void doDemoService(WfEventSubmitInfo eventInfo);

    /**
     * 提交之后
     *
     * @param eventInfo 事件执行VO，可以获取业务bean
     */
    public void doTaskEvent(WfEventSubmitInfo eventInfo);

    /**
     * 重写deSave
     *
     * @param param 前端传参
     * @return 业务bean
     */
    public DynaBean doSave(MethodArgument param);

}
