package com.je.table.service;

/**
 * 转化数据库操作层次
 * @author zhangshuaipeng
 *
 */
public interface DataBaseManager {

	/**
	 * 生成数据库SQL
	 * @param dbName 数据库名称
	 */
	void generateSql(String dbName);

	/**
	 * 同步数据
	 * @throws Exception
	 */
	void syncOracleData() throws Exception;

	/**
	 * 同步mySql数据
	 * @throws Exception
	 */
	void syncMySqlData() throws Exception;

	/**
	 * 同步jbpm数据
	 */
	void syncOracleJbpm();

	/**
	 * 转oracle
	 */
	void toOracle();

	/**
	 * 转Myql
	 */
	void toMysql();
}
