package com.je.core.dialect;

import org.hibernate.Hibernate;
import org.hibernate.dialect.Dialect;


/**
 * 神通数据库方言
 * @author huxuanhua
 *
 */

import java.sql.SQLException;
import org.hibernate.dialect.function.NoArgSQLFunction;
import org.hibernate.dialect.function.PositionSubstringFunction;
import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.dialect.function.VarArgsSQLFunction;
import org.hibernate.exception.JDBCExceptionHelper;
import org.hibernate.exception.TemplatedViolatedConstraintNameExtracter;
import org.hibernate.exception.ViolatedConstraintNameExtracter;
import org.hibernate.id.SequenceGenerator;
import org.hibernate.util.StringHelper;

public class OscarDialect extends Dialect {
    private static ViolatedConstraintNameExtracter EXTRACTER = new TemplatedViolatedConstraintNameExtracter() {
        public String extractConstraintName(SQLException var1) {
            try {
                int var2 = Integer.valueOf(JDBCExceptionHelper.extractSqlState(var1));
                switch(var2) {
                    case 23001:
                        return null;
                    case 23502:
                        return this.extractUsingTemplate("null value in column \"", "\" violates not-null constraint", var1.getMessage());
                    case 23503:
                        return this.extractUsingTemplate("violates foreign key constraint \"", "\"", var1.getMessage());
                    case 23505:
                        return this.extractUsingTemplate("violates unique constraint \"", "\"", var1.getMessage());
                    case 23514:
                        return this.extractUsingTemplate("violates check constraint \"", "\"", var1.getMessage());
                    default:
                        return null;
                }
            } catch (NumberFormatException var3) {
                return null;
            }
        }
    };

    public OscarDialect() {
        this.registerColumnType(-7, "number(1,0)");
        this.registerColumnType(-5, "bigint");
        this.registerColumnType(5, "smallint");
        this.registerColumnType(-6, "tinyint");
        this.registerColumnType(4, "int");
        this.registerColumnType(1, "char(1)");
        this.registerColumnType(12, "varchar($l)");
        this.registerColumnType(6, "float4");
        this.registerColumnType(8, "float8");
        this.registerColumnType(91, "date");
        this.registerColumnType(92, "time");
        this.registerColumnType(93, "timestamp");
        this.registerColumnType(-3, "varbinary($l)");
        this.registerColumnType(2005, "clob");
        this.registerColumnType(2004, "blob");
        this.registerColumnType(2, "numeric($p, $s)");
        this.registerColumnType(16, "boolean");
        this.registerColumnType(3, "decimal($p, $s)");
        this.registerColumnType(-4, "blob");
        this.registerColumnType(-1, "text");
        this.registerFunction("lpad", new StandardSQLFunction("lpad", Hibernate.STRING));
        this.registerFunction("abs", new StandardSQLFunction("abs"));
        this.registerFunction("sign", new StandardSQLFunction("sign", Hibernate.INTEGER));
        this.registerFunction("acos", new StandardSQLFunction("acos", Hibernate.DOUBLE));
        this.registerFunction("asin", new StandardSQLFunction("asin", Hibernate.DOUBLE));
        this.registerFunction("atan", new StandardSQLFunction("atan", Hibernate.DOUBLE));
        this.registerFunction("cos", new StandardSQLFunction("cos", Hibernate.DOUBLE));
        this.registerFunction("cot", new StandardSQLFunction("cot", Hibernate.DOUBLE));
        this.registerFunction("exp", new StandardSQLFunction("exp", Hibernate.DOUBLE));
        this.registerFunction("ln", new StandardSQLFunction("ln", Hibernate.DOUBLE));
        this.registerFunction("log", new StandardSQLFunction("log", Hibernate.DOUBLE));
        this.registerFunction("sin", new StandardSQLFunction("sin", Hibernate.DOUBLE));
        this.registerFunction("sqrt", new StandardSQLFunction("sqrt", Hibernate.DOUBLE));
        this.registerFunction("cbrt", new StandardSQLFunction("cbrt", Hibernate.DOUBLE));
        this.registerFunction("tan", new StandardSQLFunction("tan", Hibernate.DOUBLE));
        this.registerFunction("radians", new StandardSQLFunction("radians", Hibernate.DOUBLE));
        this.registerFunction("degrees", new StandardSQLFunction("degrees", Hibernate.DOUBLE));
        this.registerFunction("stddev", new StandardSQLFunction("stddev", Hibernate.DOUBLE));
        this.registerFunction("variance", new StandardSQLFunction("variance", Hibernate.DOUBLE));
        this.registerFunction("rand", new NoArgSQLFunction("rand", Hibernate.DOUBLE));
        this.registerFunction("round", new StandardSQLFunction("round"));
        this.registerFunction("trunc", new StandardSQLFunction("trunc"));
        this.registerFunction("ceil", new StandardSQLFunction("ceil"));
        this.registerFunction("floor", new StandardSQLFunction("floor"));
        this.registerFunction("chr", new StandardSQLFunction("chr", Hibernate.CHARACTER));
        this.registerFunction("lower", new StandardSQLFunction("lower"));
        this.registerFunction("upper", new StandardSQLFunction("upper"));
        this.registerFunction("substr", new StandardSQLFunction("substr", Hibernate.STRING));
        this.registerFunction("initcap", new StandardSQLFunction("initcap"));
        this.registerFunction("to_ascii", new StandardSQLFunction("to_ascii"));
        this.registerFunction("quote_ident", new StandardSQLFunction("quote_ident", Hibernate.STRING));
        this.registerFunction("quote_literal", new StandardSQLFunction("quote_literal", Hibernate.STRING));
        this.registerFunction("ascii", new StandardSQLFunction("ascii", Hibernate.INTEGER));
        this.registerFunction("length", new StandardSQLFunction("length", Hibernate.LONG));
        this.registerFunction("char_length", new StandardSQLFunction("char_length", Hibernate.LONG));
        this.registerFunction("bit_length", new StandardSQLFunction("bit_length", Hibernate.LONG));
        this.registerFunction("octet_length", new StandardSQLFunction("octet_length", Hibernate.LONG));
        this.registerFunction("current_date", new NoArgSQLFunction("current_date", Hibernate.DATE, false));
        this.registerFunction("current_time", new NoArgSQLFunction("current_time", Hibernate.TIME, false));
        this.registerFunction("current_timestamp", new NoArgSQLFunction("current_timestamp", Hibernate.TIMESTAMP, false));
        this.registerFunction("localtime", new NoArgSQLFunction("localtime", Hibernate.TIME, false));
        this.registerFunction("localtimestamp", new NoArgSQLFunction("localtimestamp", Hibernate.TIMESTAMP, false));
        this.registerFunction("now", new NoArgSQLFunction("now", Hibernate.TIMESTAMP));
        this.registerFunction("timeofday", new NoArgSQLFunction("timeofday", Hibernate.STRING));
        this.registerFunction("age", new StandardSQLFunction("age"));
        this.registerFunction("current_user", new NoArgSQLFunction("current_user", Hibernate.STRING, false));
        this.registerFunction("session_user", new NoArgSQLFunction("session_user", Hibernate.STRING, false));
        this.registerFunction("user", new NoArgSQLFunction("user", Hibernate.STRING, false));
        this.registerFunction("current_database", new NoArgSQLFunction("current_database", Hibernate.STRING, true));
        this.registerFunction("current_schema", new NoArgSQLFunction("current_schema", Hibernate.STRING, true));
        this.registerFunction("to_char", new StandardSQLFunction("to_char", Hibernate.STRING));
        this.registerFunction("to_date", new StandardSQLFunction("to_date", Hibernate.DATE));
        this.registerFunction("to_timestamp", new StandardSQLFunction("to_timestamp", Hibernate.TIMESTAMP));
        this.registerFunction("to_number", new StandardSQLFunction("to_number", Hibernate.BIG_DECIMAL));
        this.registerFunction("concat", new VarArgsSQLFunction(Hibernate.STRING, "(", "||", ")"));
        this.registerFunction("locate", new PositionSubstringFunction());
        this.registerFunction("str", new StandardSQLFunction("to_char", Hibernate.STRING));
        this.getDefaultProperties().setProperty("hibernate.jdbc.batch_size", "15");
    }

    public String getAddColumnString() {
        return "add column";
    }

    public String getSequenceNextValString(String var1) {
        return "select " + this.getSelectSequenceNextValString(var1);
    }

    public String getSelectSequenceNextValString(String var1) {
        return "nextval ('" + var1 + "')";
    }

    public String getCreateSequenceString(String var1) {
        return "create sequence " + var1;
    }

    public String getDropSequenceString(String var1) {
        return "drop sequence " + var1;
    }

    public String getCascadeConstraintsString() {
        return " cascade";
    }

    public boolean dropConstraints() {
        return true;
    }

    public boolean supportsSequences() {
        return true;
    }

    public String getQuerySequencesString() {
        return "select relname from sys_class where relkind='S'";
    }

    public boolean supportsLimit() {
        return true;
    }

    public String getLimitString(String var1, boolean var2) {
        return (new StringBuffer(var1.length() + 20)).append(var1).append(var2 ? " limit ? offset ?" : " limit ?").toString();
    }

    public boolean bindLimitParametersInReverseOrder() {
        return true;
    }

    public boolean doesReadCommittedCauseWritersToBlockReaders() {
        return true;
    }

    public boolean doesRepeatableReadCauseReadersToBlockWriters() {
        return true;
    }

    public boolean supportsIdentityColumns() {
        return false;
    }

    public String getForUpdateString(String var1) {
        return this.getForUpdateString() + " of " + var1;
    }

    public String getIdentitySelectString(String var1, String var2, int var3) {
        return "select currval('" + var1 + '_' + var2 + "_seq')";
    }

    public String getIdentityColumnString(int var1) {
        return var1 == -5 ? "bigserial not null" : "serial not null";
    }

    public boolean hasDataTypeInIdentityColumn() {
        return false;
    }

    public String getNoColumnsInsertString() {
        return "default values";
    }

    public Class getNativeIdentifierGeneratorClass() {
        return SequenceGenerator.class;
    }

    public boolean supportsOuterJoinForUpdate() {
        return false;
    }

    public boolean supportsUnionAll() {
        return true;
    }

    public boolean supportsCommentOn() {
        return true;
    }

    public boolean supportsTemporaryTables() {
        return true;
    }

    public String getCreateTemporaryTableString() {
        return "create local temporary table";
    }

    public boolean supportsCurrentTimestampSelection() {
        return true;
    }

    public boolean isCurrentTimestampSelectStringCallable() {
        return false;
    }

    public String getCurrentTimestampSelectString() {
        return "select now()";
    }

    public String toBooleanValueString(boolean var1) {
        return var1 ? "1" : "0";
    }

    public ViolatedConstraintNameExtracter getViolatedConstraintNameExtracter() {
        return EXTRACTER;
    }

    public boolean supportsSubselectAsInPredicateLHS() {
        return false;
    }

    public boolean supportsEmptyInList() {
        return false;
    }

    public boolean supportsExpectedLobUsagePattern() {
        return true;
    }

    public boolean supportsLobValueChangePropogation() {
        return false;
    }

    public boolean supportsPooledSequences() {
        return true;
    }

    public boolean supportsTupleDistinctCounts() {
        return false;
    }
    public String getAddForeignKeyConstraintString(String constraintName, String[] foreignKey, String referencedTable, String[] primaryKey, boolean referencesPrimaryKey) {
        String cols = StringHelper.join(", ", foreignKey);
        return (new StringBuffer(30)).append(" add index ").append(constraintName).append(" (").append(cols).append("), add constraint ").append(constraintName).append(" foreign key (").append(cols).append(") references ").append(referencedTable).append(" (").append(StringHelper.join(", ", primaryKey)).append(')').toString();
    }
}
