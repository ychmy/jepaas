package com.project.demo.service;

import com.je.core.util.bean.DynaBean;
import com.je.thrid.service.AbstractDynaBeanImportService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: je-platform
 * @author: LIULJ
 * @create: 2020/7/26
 * @description:
 */
@Component
public class DepartmentImportService extends AbstractDynaBeanImportService {

    @Override
    public List<DynaBean> doSort(List<DynaBean> dynaBeans) {
        List<DynaBean> newList = new ArrayList<>();
        String oneParentId = "000000000";
        for (DynaBean dynaBean : dynaBeans) {
            if (dynaBean.getStr("GROUP_PARENT_ID").equals(oneParentId)) {
                newList.add(dynaBean);
                oneParentId = dynaBean.getStr("GROUP_ID");
                break;
            }
        }
        getChildDepts(dynaBeans, oneParentId, newList);
        for(DynaBean dynaBean : dynaBeans){
            Boolean isExist = true;
            for(DynaBean newDynaBean : newList){
                if(dynaBean.getStr("GROUP_ID").equals(newDynaBean.getStr("GROUP_ID"))){
                    isExist = false;
                    break;
                }
            }
            if(isExist){
                newList.add(dynaBean);
            }
        }
        return newList;
    }

    private void getChildDepts(List<DynaBean> dynaBeans, String parentId, List<DynaBean> newList) {
        for (DynaBean dynaBean : dynaBeans) {
            if (dynaBean.getStr("GROUP_PARENT_ID").equals(parentId)) {
                newList.add(dynaBean);
                getChildDepts(dynaBeans, dynaBean.getStr("GROUP_ID"), newList);
            }
        }
    }

}
