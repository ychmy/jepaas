package com.je.core.lock;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * Redis 锁注解
 *
 * @author wangmm@ketr.com.cn
 * @date 2020/12/16
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DistributedLock {

    /**
     * 锁的名称, 默认为包名+类名+方法名
     */
    String value() default "";

    /**
     * 时间单位(默认毫秒)
     */
    TimeUnit unit() default TimeUnit.MILLISECONDS;

    /**
     * 锁的有效时间,默认30秒
     */
    long timeout() default 30000L;

    /**
     * 重试次数
     */
    int retries() default 150;

    /**
     * 重试间隔时间
     */
    long waitingTime() default 300;

}


