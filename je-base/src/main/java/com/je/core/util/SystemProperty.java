package com.je.core.util;

/**
 * 系统属性
 *
 * @author wangmm@ketr.com.cn
 * @date 2020/8/31
 */
public class SystemProperty {

    /**
     * 是否是jar启动
     */
    public static final String IS_JAR = "je.isJar";
    /**
     * 外置根目录，存储临时文件等
     */
    public static final String ROOT_PATH = "je.path.root";
    /**
     * 自定义 license 地址
     */
    public static final String LICENSE_PATH = "je.path.license";


    public static String getRootPath() {
        return System.getProperty(ROOT_PATH);
    }

    public static void setRootPath(String value) {
        System.setProperty(ROOT_PATH, value.endsWith("/") ? value : (value + "/"));
        System.setProperty("jeplus.webapp", getRootPath());
    }

    public static String getLicensePath() {
        return System.getProperty(LICENSE_PATH);
    }

    public static void setLicensePath(String value) {
        System.setProperty(LICENSE_PATH, value);
    }

    public static boolean isJar() {
        return "1".equals(System.getProperty(IS_JAR));
    }

    public static void isJar(boolean value) {
        System.setProperty(IS_JAR, value ? "1" : "0");
    }

}
