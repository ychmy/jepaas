package com.je.paas.document.vo;

import com.je.paas.document.model.bean.BasePojo;

import java.util.Date;

public class DiskFileVO extends BasePojo {

    /**
     * 文件/目录id
     */
    private Long nodeId;
    /**
     * 网盘类型
     */
    private String diskType;
    /**
     * 文件/文件夹名称
     */
    private String nodeName;
    /**
     * 文件后缀
     */
    private String fileSuffix;
    /**
     * 文件唯一标识
     */
    private String fileKey;
    /**
     * 文件标签
     */
    private String nodeTag;
    /**
     * 文件类型
     */
    private String nodeType;
    /**
     * 文件url
     */
    private String fileUrl;
    /**
     * 父节点id
     */
    private Long parent;
    /**
     * 父路径id
     */
    private String parentPath;
    /**
     * 父路径名称
     */
    private String parentPathName;
    /**
     * 文件大小
     */
    private Long fileSize;
    /**
     * 创建人id
     */
    private String createUserId;
    /**
     * 创建人姓名
     */
    private String createUserName;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date modifiedTime;
    /**
     * 修改人id
     */
    private String modifiedUserId;
    /**
     * 修改人姓名
     */
    private String modifiedUserName;
    /**
     * 所属人id
     */
    private String ownerId;
    /**
     * 所属人姓名
     */
    private String ownerName;
    /**
     * 角色code
     */
    private String roleRoleCode;
    /**
     * 人员类型
     */
    private String setRoleType;


    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getFileSuffix() {
        return fileSuffix;
    }

    public void setFileSuffix(String fileSuffix) {
        this.fileSuffix = fileSuffix;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getNodeId() {
        return nodeId;
    }

    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }

    public String getDiskType() {
        return diskType;
    }

    public void setDiskType(String diskType) {
        this.diskType = diskType;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public Long getParent() {
        return parent;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }

    public String getParentPath() {
        return parentPath;
    }

    public void setParentPath(String parentPath) {
        this.parentPath = parentPath;
    }

    public String getParentPathName() {
        return parentPathName;
    }

    public void setParentPathName(String parentPathName) {
        this.parentPathName = parentPathName;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public String getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    public String getModifiedUserName() {
        return modifiedUserName;
    }

    public void setModifiedUserName(String modifiedUserName) {
        this.modifiedUserName = modifiedUserName;
    }

    public String getNodeTag() {
        return nodeTag;
    }

    public void setNodeTag(String nodeTag) {
        this.nodeTag = nodeTag;
    }

    public String getNodeType() {
        return nodeType;
    }

    public void setNodeType(String nodeType) {
        this.nodeType = nodeType;
    }

    public String getFileKey() {
        return fileKey;
    }

    public void setFileKey(String fileKey) {
        this.fileKey = fileKey;
    }

    public String getRoleRoleCode() {
        return roleRoleCode;
    }

    public void setRoleRoleCode(String roleRoleCode) {
        this.roleRoleCode = roleRoleCode;
    }

    public String getSetRoleType() {
        return setRoleType;
    }

    public void setSetRoleType(String setRoleType) {
        this.setRoleType = setRoleType;
    }
}
