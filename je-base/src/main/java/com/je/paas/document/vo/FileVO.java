package com.je.paas.document.vo;

import com.je.paas.document.model.bean.BasePojo;

import java.util.Date;

/**
 * 文件上传返回数据对象
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/8/27
 */
public class FileVO extends BasePojo {

    /**
     * 文件唯一标识
     */
    private String fileKey;

    /**
     * 文件真实名称,je_document_rel.name
     */
    private String relName;

    /**
     * 文件后缀
     */
    private String suffix;

    /**
     * 是否包含缩略图
     */
    private Boolean hasThumbnail;

    /**
     * 文件访问地址(私有文件为null)
     */
    private String fullUrl;

    /**
     * 文件大小
     */
    private Long size;

    /**
     * 上传人
     */
    private String createUserName;

    /**
     * 上传人部门
     */
    private String createUserDeptName;

    /**
     * 上传时间
     */
    private Date createTime;

    /**
     * 字典ID 附件子功能使用
     */
    private String dicId;
    /**
     * 字典code 附件子功能使用
     */
    private String dicCode;
    /**
     * 字典名称 附件子功能使用
     */
    private String dicName;
    /**
     * 附件备注 附件子功能使用
     */
    private String remarks;

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDicId() {
        return dicId;
    }

    public void setDicId(String dicId) {
        this.dicId = dicId;
    }

    public String getDicCode() {
        return dicCode;
    }

    public void setDicCode(String dicCode) {
        this.dicCode = dicCode;
    }

    public String getDicName() {
        return dicName;
    }

    public void setDicName(String dicName) {
        this.dicName = dicName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getCreateUserDeptName() {
        return createUserDeptName;
    }

    public void setCreateUserDeptName(String createUserDeptName) {
        this.createUserDeptName = createUserDeptName;
    }

    public String getFileKey() {
        return fileKey;
    }

    public void setFileKey(String fileKey) {
        this.fileKey = fileKey;
    }

    public String getRelName() {
        return relName;
    }

    public void setRelName(String relName) {
        this.relName = relName;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public Boolean getHasThumbnail() {
        return hasThumbnail;
    }

    public void setHasThumbnail(Boolean hasThumbnail) {
        this.hasThumbnail = hasThumbnail;
    }

    public String getFullUrl() {
        return fullUrl;
    }

    public void setFullUrl(String fullUrl) {
        this.fullUrl = fullUrl;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}