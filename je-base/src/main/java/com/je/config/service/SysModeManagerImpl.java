package com.je.config.service;

import com.je.core.service.PCDynaServiceTemplate;
import com.je.core.service.PCServiceTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 系统模式
 *
 * @author zhangshuaipeng
 */
@Component("sysModeManager")
public class SysModeManagerImpl implements SysModeManager {
    private PCDynaServiceTemplate serviceTemplate;
    private PCServiceTemplate pcServiceTemplate;

    @Resource(name = "PCDynaServiceTemplate")
    public void setServiceTemplate(PCDynaServiceTemplate serviceTemplate) {
        this.serviceTemplate = serviceTemplate;
    }

    @Resource(name = "PCServiceTemplateImpl")
    public void setPcServiceTemplate(PCServiceTemplate pcServiceTemplate) {
        this.pcServiceTemplate = pcServiceTemplate;
    }
}
