package com.je.thrid.service;


import com.je.core.util.bean.DynaBean;
import com.je.thrid.importsource.AbstractDynaBeanImportSource;
import com.je.thrid.importsource.ImportSource;
import com.je.thrid.parser.AbstractDynaBeanImportParser;
import com.je.thrid.parser.ImportParser;
import net.sf.json.JSONObject;

import java.util.List;

/**
 * @program: je-platform
 * @author: LIULJ
 * @create: 2020-07-13 15:38
 * @description: 导入服务定义
 */
public interface ImportService<T> {

    /**
     * 获取ImportSource
     * @return
     */
    ImportSource getImportSource();

    /**
     * 获取转换器
     * @return
     */
    ImportParser getImportParser();

    /**
     * 进行转换
     * @return
     */
    void doReadAndConvert();

    /**
     * 进行排序
     * @return
     */
    T doSort(T t);

    /**
     * 自定义导入完成方法
     * @param customParameters
     */
    void customMethod(List<DynaBean> list, JSONObject customParameters);

    /**
     * 写入操作
     * @return
     */
    void doImport(AbstractDynaBeanImportSource dynaBeanImportSource,
                  AbstractDynaBeanImportParser dynaBeanImportParser, JSONObject customParameters);

}
