package com.project.demo.controller;

import com.je.core.base.AbstractPlatformController;
import com.je.core.base.MethodArgument;
import com.je.core.result.BaseRespResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * DemoController
 */
@Controller
@RequestMapping(value = "/je/product/demo")
public class DemoReturnInfoController  extends AbstractPlatformController {

    @ResponseBody
    @RequestMapping(value = "/returnInfo",method = RequestMethod.POST)
    public BaseRespResult returnInfo(MethodArgument param){
       return BaseRespResult.successResult("你好，欢迎使用JEPAAS。这是后台返回数据");
    }

}
