package com.je.micromail.vo;

import com.je.micromail.dto.MicromailDto;

/**
 * @Description //微邮列表entity
 * @Auther: yuchunhui
 * @Date: 14 16
 * @Description:
 */

public class MicromailListVo {
  /**
   * @Description //主题
   **/
  private String zt;
  /**
   * @Description //最后修改时间
   **/
  private String updateTime;
  /**
   * @Description //发件人
   **/
  private String senderUserName;
  /**
   * @Description //发件人id
   **/
  private String sendUserId;
  /**
   * @Description //功能Code
   **/
  private String funcCode;
  /**
   * @Description //功能名称
   **/
  private String funcName;
  /**
   * @Description //点赞数量
   **/
  private int dzNum;
  /**
   * @Description //点赞
   **/
  private Boolean isZan;
  /**
   * @Description //是否收藏
   **/
  private Boolean focus;
  /**
   * @Description //水印
   **/
  private Boolean isSy;
  /**
   * @Description //数据id
   **/
  private String modelId;
  /**
   * @Description //数据名称
   **/
  private String modelName;
  /**
   * @Description //最后评论数据
   **/
  private String lastMsg;
  /**
   * @Description //最后评论人名
   **/
  private String lastUserName;
  /**
   * @Description //最后评论人ID
   **/
  private String lastUserId;
  /**
   * @Description //主键ID
   **/
  private String id;
  /**
   * @Description //微邮是否阅读
   **/
  private Boolean isRead;
  /**
   * @Description //评论是否阅读
   **/
  private Boolean isMsgRead;
  /**
   * @Description //功能id
   **/
  String funcId;
  /**
   * @Description //表名
   **/
  String tableCode;
  /**
   * @Description //是否有附件
   **/
  Boolean isMsgFile;

  public static MicromailListVo build(MicromailDto mto) {
    MicromailListVo micromailListVo = new MicromailListVo();
    micromailListVo.setDzNum(mto.getZanNumber());
    micromailListVo.setFocus(mto.getGz());
    micromailListVo.setSy(mto.getSy());
    micromailListVo.setZan(mto.getZan());
    micromailListVo.setRead(mto.getSfyd());
    micromailListVo.setMsgRead(mto.getPlyd());
    micromailListVo.setModelId(mto.getModelid());
    micromailListVo.setModelName(mto.getModelName());
    micromailListVo.setLastMsg(mto.getLastMsg());
    micromailListVo.setLastUserName(mto.getLastMsgUserName());
    micromailListVo.setLastUserId(mto.getLastUserId());
    micromailListVo.setZt(mto.getZt());
    micromailListVo.setUpdateTime(mto.getUpdateTime());
    micromailListVo.setSenderUserName(mto.getCreateUserName());
    micromailListVo.setSendUserId(mto.getCreateUserId());
    micromailListVo.setFuncName(mto.getFuncName());
    micromailListVo.setId(mto.getId());
    micromailListVo.setFuncCode(mto.getFuncCode());
    micromailListVo.setFuncId(mto.getFuncId());
    if (mto.getFile().size() > 0) {
      micromailListVo.setMsgFile(true);
    } else {
      micromailListVo.setMsgFile(false);
    }
    return micromailListVo;
  }

  public String getZt() {
    return zt;
  }

  public void setZt(String zt) {
    this.zt = zt;
  }

  public String getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(String updateTime) {
    this.updateTime = updateTime;
  }

  public String getSenderUserName() {
    return senderUserName;
  }

  public void setSenderUserName(String senderUserName) {
    this.senderUserName = senderUserName;
  }

  public String getSendUserId() {
    return sendUserId;
  }

  public void setSendUserId(String sendUserId) {
    this.sendUserId = sendUserId;
  }

  public String getFuncCode() {
    return funcCode;
  }

  public void setFuncCode(String funcCode) {
    this.funcCode = funcCode;
  }

  public String getFuncName() {
    return funcName;
  }

  public void setFuncName(String funcName) {
    this.funcName = funcName;
  }

  public int getDzNum() {
    return dzNum;
  }

  public void setDzNum(int dzNum) {
    this.dzNum = dzNum;
  }

  public Boolean getZan() {
    return isZan;
  }

  public void setZan(Boolean zan) {
    isZan = zan;
  }

  public Boolean getFocus() {
    return focus;
  }

  public void setFocus(Boolean focus) {
    this.focus = focus;
  }

  public Boolean getSy() {
    return isSy;
  }

  public void setSy(Boolean sy) {
    isSy = sy;
  }

  public String getModelId() {
    return modelId;
  }

  public void setModelId(String modelId) {
    this.modelId = modelId;
  }

  public String getModelName() {
    return modelName;
  }

  public void setModelName(String modelName) {
    this.modelName = modelName;
  }

  public String getLastMsg() {
    return lastMsg;
  }

  public void setLastMsg(String lastMsg) {
    this.lastMsg = lastMsg;
  }

  public String getLastUserName() {
    return lastUserName;
  }

  public void setLastUserName(String lastUserName) {
    this.lastUserName = lastUserName;
  }

  public String getLastUserId() {
    return lastUserId;
  }

  public void setLastUserId(String lastUserId) {
    this.lastUserId = lastUserId;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Boolean getRead() {
    return isRead;
  }

  public void setRead(Boolean read) {
    isRead = read;
  }

  public Boolean getMsgRead() {
    return isMsgRead;
  }

  public void setMsgRead(Boolean msgRead) {
    isMsgRead = msgRead;
  }

  public String getFuncId() {
    return funcId;
  }

  public void setFuncId(String funcId) {
    this.funcId = funcId;
  }

  public String getTableCode() {
    return tableCode;
  }

  public void setTableCode(String tableCode) {
    this.tableCode = tableCode;
  }

  public Boolean getMsgFile() {
    return isMsgFile;
  }

  public void setMsgFile(Boolean msgFile) {
    isMsgFile = msgFile;
  }
}
