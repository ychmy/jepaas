package com.project.demo.controller;

import com.google.common.base.Strings;
import com.je.core.base.MethodArgument;
import com.je.core.controller.PlatformController;
import com.je.core.result.BaseRespResult;
import com.je.core.util.SpringContextHolder;
import com.je.thrid.importsource.DataSourceImportSource;
import com.project.demo.parser.DepartmentImportParser;
import com.project.demo.parser.EndUserImportParser;
import com.project.demo.service.DepartmentImportService;
import com.project.demo.service.UserImportService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * @program: je-platform
 * @author: LIULJ
 * @create: 2020/7/26
 * @description: 三方数据源导入部门和人员
 */
@Controller
@RequestMapping(value = "/je/demo/import")
public class ImportController extends PlatformController {
    @Autowired
    DepartmentImportParser departmentImportParser;
    @Autowired
    DepartmentImportService departmentImportService;
    @Autowired
    UserImportService userImportService;


    @RequestMapping(value = {"/doImportDepartment"}, method = {RequestMethod.POST},
        produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public BaseRespResult doImportDepartment(MethodArgument param) throws SQLException {
        //数据源的名称
        String dsName = param.getRequest().getParameter("dsName");
        //插入数据的表
        String tabelCode = param.getRequest().getParameter("tableCode");
        //自定义参数，可以在parser中获取
        JSONObject customParameters = new JSONObject();
        if (Strings.isNullOrEmpty(dsName)) {
            return BaseRespResult.errorResult("请指定数据源！");
        }
        DataSource departDataSource = null;
        if (departDataSource == null) {
            return BaseRespResult.errorResult("没有找到指定数据源！");
        }
        // 查询三方库的sql
        String sql = "SELECT * FROM JE_CORE_DEPARTMENT WHERE DEPTNAME!='ROOT'";
        DataSourceImportSource dataSourceImportSource = new DataSourceImportSource(departDataSource, sql, tabelCode);
        //自定义格式化方法
        DepartmentImportParser departmentImportParser = new DepartmentImportParser();
        departmentImportService.doImport(dataSourceImportSource, departmentImportParser, customParameters);
        return BaseRespResult.successResult("导入成功！");
    }

    @RequestMapping(value = {"/doImportUser"}, method = {RequestMethod.POST},
        produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public BaseRespResult doImportUser(MethodArgument param) throws SQLException {
        //数据源名称
        String dsName = param.getRequest().getParameter("dsName");
        //导入数据的表明
        String tableCode = param.getRequest().getParameter("tableCode");
        JSONObject customParameters = new JSONObject();
        if (Strings.isNullOrEmpty(dsName)) {
            return BaseRespResult.errorResult("请指定数据源！");
        }
        DataSource departDataSource = SpringContextHolder.getBean(dsName);
        if (departDataSource == null) {
            return BaseRespResult.errorResult("没有找到指定数据源！");
        }
        //查询的sql
        String sql = "SELECT * FROM JE_CORE_ENDUSER  WHERE USERCODE!='admin'";
        DataSourceImportSource dataSourceImportSource = new DataSourceImportSource(departDataSource, sql, tableCode);
        //自定义格式化方法
        EndUserImportParser endUserImportParser = new EndUserImportParser();
        userImportService.doImport(dataSourceImportSource, endUserImportParser, customParameters);
        return BaseRespResult.successResult("导入成功！");
    }


}
