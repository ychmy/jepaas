package com.je.rbac.service;

import com.je.core.util.bean.DynaBean;

import java.util.Map;

public interface EndUserCustomAttributesService {

    /**
     * 添加自定义用户参数，前端可以通过JE.user.customAttributes直接拿到
     *
     * @param customAttributes 添加，返回
     * @return
     */
    public Map<String, Object> addCustomAttributesService(DynaBean user, Map<String, Object> customAttributes);

}
