package com.je.rbac.service;

import com.google.common.base.Strings;
import com.je.core.util.SpringContextHolder;
import com.je.core.util.WebUtils;
import com.je.core.util.bean.DynaBean;

import java.util.Map;

public class DefaultEndUserCustomAttributesServiceImpl implements EndUserCustomAttributesService {

    @Override
    public Map<String, Object> addCustomAttributesService(DynaBean user, Map<String, Object> customAttributes) {
        String customAttributesStr = WebUtils.getBackVar("CUSTOM_ATTRIBUTES");
        try {
            if (!Strings.isNullOrEmpty(customAttributesStr)) {
                String[] cusz = customAttributesStr.split("~");
                if (cusz.length == 2) {
                    String[] keys = cusz[0].split(",");
                    String[] value = cusz[1].split(",");
                    for (int i = 0; i < keys.length; i++) {
                        customAttributes.put(keys[i], user.get(value[i]));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        EndUserCustomAttributesService endUserCustomAttributesService = SpringContextHolder.getBean(EndUserCustomAttributesService.class);
        if (endUserCustomAttributesService != null) {
            return endUserCustomAttributesService.addCustomAttributesService(user, customAttributes);
        }
        return customAttributes;
    }
}
