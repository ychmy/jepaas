package net.sf.ehcache.store;

import net.sf.ehcache.bootstrap.BootstrapCacheLoaderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

/**
 * 二级缓存启动加载器工厂
 *
 * @ProjectName: je-saas-platform
 * @Package: net.sf.ehcache
 * @ClassName: SecondLevelStoreBootstrapCacheLoaderFactory
 * @Description: 二级缓存启动加载器工厂
 * @Author: LIULJ
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class SecondLevelStoreBootstrapCacheLoaderFactory extends BootstrapCacheLoaderFactory<SecondLevelStoreBootstrapCacheLoader> {

    private static final Logger LOG = LoggerFactory.getLogger(SecondLevelStoreBootstrapCacheLoaderFactory.class);

    @Override
    public SecondLevelStoreBootstrapCacheLoader createBootstrapCacheLoader(Properties properties) {
        return new SecondLevelStoreBootstrapCacheLoader(extractBootstrapAsynchronously(properties));
    }
}
