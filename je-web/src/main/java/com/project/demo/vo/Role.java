package com.project.demo.vo;

import com.google.common.base.Strings;
import com.je.core.util.bean.BeanUtils;
import com.je.core.util.bean.DynaBean;
import com.sun.istack.internal.NotNull;
import net.sf.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Role {
    /**
     * 角色ID，如果为空，默认自动生成
     */
    private String roleId;
    /**
     * 角色名称
     */
    @NotNull
    private String roleName;
    /**
     * 角色code
     */
    private String roleCode;
    /**
     * 上级角色名称
     */
    private String parentName;
    /**
     * 上级角色编码
     */
    private String parentCode;
    /**
     * 上级角色id,如果为空默认name,code都为root
     */
    private String parent;

    public static List<DynaBean> buildRole(List<JSONObject> roles) {
        List<DynaBean> list = new ArrayList<>();
        for (JSONObject jbRole : roles) {
            Role role = (Role) JSONObject.toBean(jbRole,Role.class);
            if (Objects.isNull(role)) {
                continue;
            }
            DynaBean dynaBean = new DynaBean();
            dynaBean.setStr(BeanUtils.KEY_TABLE_CODE, "JE_CORE_ROLE");
            dynaBean.setStr("", role.getRoleName());
            if (!Strings.isNullOrEmpty(role.getRoleId())) {
                dynaBean.setStr("ROLEID", role.getRoleId());
            }
            dynaBean.setStr("ROLENAME", role.getRoleName());
            if (!Strings.isNullOrEmpty(role.getRoleCode())) {
                dynaBean.setStr("ROLECODE", role.getRoleCode());
            } else {
                dynaBean.setStr("ROLECODE", "<系统自动生成>");
            }
            dynaBean.setStr("ROLERANK", "SYS");
            dynaBean.setStr("SAAS", "0");
            dynaBean.setStr("DEVELOP", "0");
            dynaBean.setStr("ROLETYPE", "ROLE");
            if (!Strings.isNullOrEmpty(role.getParentName())) {
                dynaBean.setStr("PARENTNAME", role.getParentName());
            } else {
                dynaBean.setStr("PARENT", "ROOT");
                dynaBean.setStr("PARENTCODE", "ROOT");
                dynaBean.setStr("PARENTNAME", "ROOT");
                dynaBean.setStr("PATH", "/ROOT");
            }
            dynaBean.setStr("ICONCLS", "jeicon jeicon-role");
            dynaBean.setStr("STATUS", "1");
            list.add(dynaBean);
        }
        return list;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }
}
