package com.je.micromail.service;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.je.cache.redis.RedisCache;
import com.je.core.constants.app.ExecuteType;
import com.je.core.constants.push.PushAct;
import com.je.core.constants.push.PushType;
import com.je.core.facade.extjs.JsonAssist;
import com.je.core.facade.extjs.JsonBuilder;
import com.je.core.service.PCDynaServiceTemplate;
import com.je.core.service.PCServiceTemplate;
import com.je.core.util.JEUUID;
import com.je.core.util.SecurityUserHolder;
import com.je.core.util.bean.DynaBean;
import com.je.message.service.UserMsgManaer;
import com.je.message.util.PushMsgHttpUtil;
import com.je.message.vo.DwrMsgVo;
import com.je.message.vo.app.PayloadInfo;
import com.je.message.vo.app.UserMsgAppInfo;
import com.je.micromail.dto.MicromailDto;
import com.je.micromail.vo.CommentVo;
import com.je.micromail.vo.MicromailListVo;
import com.je.micromail.vo.MicromailVo;
import com.je.micromail.vo.ReadInfoVo;
import com.je.portal.service.PortalManager;
import com.je.portal.vo.PushActVo;
import com.je.rbac.model.EndUser;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @Description //微邮service实现
 * @Auther: yuchunhui
 * @Date 2019/10/10
 * @Param null:
 * @return: null
 **/
@Component("micromailManager")
public class MicromailManagerImpl implements MicromailManager {
    @Autowired
    private PCDynaServiceTemplate serviceTemplate;
    @Autowired
    private PCServiceTemplate pcServiceTemplate;
    @Autowired
    private PortalManager portalManager;
    @Autowired
    private UserMsgManaer userMsgManaer;
    @Autowired
    private RedisCache redisCache;

    private final static String TABLECODE = "JE_CORE_MICROMAIL";

    private final static String MICROMAILREDIS = "MICROMAILREDIS";

    @Override
    public Map<String, Object> getMicromailTotalTypeCount(String userId, String modelId) {
        Map<String, Object> map = new HashedMap();
        int in = getINMicromailTotalCount(modelId);
        int send = getSendMicromailTotalCount(userId, modelId);
        int noread = getNoINMicromailTotalCount(modelId);
        map.put("receive", in);
        map.put("send", send);
        map.put("history", in - noread);
        return map;
    }

    @Override
    public int getMicromailTotalCount(String userId, String modelId) {
        Map<String, Object> map = new HashedMap();
        int in = getINMicromailTotalCount(modelId);
        int send = getSendMicromailTotalCount(userId, modelId);
        return in + send;
    }

    //收到的总数
    private int getINMicromailTotalCount(String modelId) {
        String countGroupBySql = " GROUP BY MICROMAIL_MODELID ";
        StringBuffer countSql = getCountSql("1", "1", modelId, false);
        Map countMap = getCountsMap(countSql.toString() + countGroupBySql);
        int num = Integer.parseInt(countMap.get("COUNT").toString());
        return num;
    }

    //收到的未读总数
    private int getNoINMicromailTotalCount(String modelId) {
        String countGroupBySql = " GROUP BY MICROMAIL_MODELID ";
        StringBuffer countSql = getCountSql("1", "2", modelId, false);
        Map countMap = getCountsMap(countSql.toString() + countGroupBySql);
        int num = Integer.parseInt(countMap.get("COUNT").toString());
        return num;
    }

    //发出的总数
    private int getSendMicromailTotalCount(String userId, String modelId) {
        String countGroupBySql = " GROUP BY MICROMAIL_MODELID ";
        StringBuffer countSql = getCountSql("2", "1", modelId, false);
        countSql.append(" WHERE 1=1 ");
        //全部
        countSql.append(String.format(" AND SY_CREATEUSERID='%s'", userId));
        Map countMap = getCountsMap(countSql.toString() + countGroupBySql);
        int num = Integer.parseInt(countMap.get("COUNT").toString());
        return num;
    }

    @Override
    public Map<String, Integer> getMicromailCount(String modelIds) {
        String userId = SecurityUserHolder.getCurrentUser().getUserId();
        List<Map> list1 = getINMicromailTotalCountLable(modelIds);
        List<Map> list2 = getSendMicromailTotalCountLable(userId, modelIds);
        Map<String, Integer> micMap = new HashedMap();
        for (Map map : list1) {
            String key = (String) map.get("MICROMAIL_MODELID");
            micMap.put(key, Integer.parseInt(map.get("COUNT").toString()));
        }

        for (Map map2 : list2) {
            String key = (String) map2.get("MICROMAIL_MODELID");
            if (Objects.isNull(micMap.get(key))) {
                micMap.put(key, Integer.parseInt(map2.get("COUNT").toString()));
            } else {
                Integer value = micMap.get(key);
                micMap.put(key, Integer.parseInt(map2.get("COUNT").toString()) + value);
            }
        }
        return micMap;
    }

    //收到的总数lable
    private List<Map> getINMicromailTotalCountLable(String modelId) {
        String countGroupBySql = " GROUP BY MICROMAIL_MODELID ";
        StringBuffer countSql = getCountSql("1", "1", modelId, true);
        List<Map> countMap = serviceTemplate.queryMapBySql(countSql.toString() + countGroupBySql);
        return countMap;
    }

    //发出的总数lable
    private List<Map> getSendMicromailTotalCountLable(String userId, String modelId) {
        String countGroupBySql = " GROUP BY MICROMAIL_MODELID ";
        StringBuffer countSql = getCountSql("2", "1", modelId, true);
        countSql.append(" WHERE 1=1 ");
        //全部
        countSql.append(String.format(" AND SY_CREATEUSERID='%s'", userId));
        List<Map> countMap = serviceTemplate.queryMapBySql(countSql.toString() + countGroupBySql);
        return countMap;
    }

    @Override
    public Map<String, Object> getNoReadCount(String userId, String modelId) {
        Map<String, Object> map = new HashedMap();
        long sc = getReceiveNumber(modelId);
        long send = getSendNumber(modelId);
//    long in = getAttentionNumber(modelId);
        //接收未读数量
        map.put("receive", sc);
        //发送未读数量
        map.put("send", send);
        return map;
    }

    @Override
    public long getNoReadsCount(String userId) {
        long sc = getReceiveNumber("");
        long send = getSendNumber("");
        return sc + send;
    }


    @Override
    public Map<String, Object> getMyMicromailLists(String userId, String modelId, String keyWord, String startTime, String endTime, String readType, String type, String pageNumber, String limit) {
        Map<String, Object> result = new HashedMap();
        if (type.equals("1")) {
            result = getReceiveList(modelId, pageNumber, limit, keyWord, startTime, endTime, readType);
        } else if (type.equals("2")) {
            result = getSendList(modelId, pageNumber, limit, keyWord, startTime, endTime, readType);
        }
        return result;
    }

    @Override
    public Map<String, Object> createMicromail(String id, Map<String, Map<String, Object>> recipient,
                                               Boolean bm, Boolean fbfs, Boolean kftl, Boolean sy,
                                               String zsr, String zsrId, String zt, String zw, String file, String funcId, String funcCode, String funcName, String tableCode, String modelId, String modelName) {
        Map<String, Object> map = new HashedMap();
        if (fbfs) {
            List<Map<String, Map<String, Object>>> recipients = new ArrayList<>();
            for (Map.Entry<String, Map<String, Object>> entry : recipient.entrySet()) {
                Map<String, Object> mapEntry = entry.getValue();
                Map<String, Map<String, Object>> rec = new HashedMap();
                rec.put(entry.getKey(), mapEntry);
                recipients.add(rec);
            }
            for (Map rec : recipients) {
                map = createMicromailMethod(id, rec, bm, fbfs, kftl, sy, zsr, zsrId, zt, zw, file, funcId, funcCode, funcName, tableCode, modelId, modelName);
            }
        } else {
            map = createMicromailMethod(id, recipient, bm, fbfs, kftl, sy, zsr, zsrId, zt, zw, file, funcId, funcCode, funcName, tableCode, modelId, modelName);
        }
        return map;
    }

    private Map<String, Object> createMicromailMethod(String id, Map<String, Map<String, Object>> recipient,
                                                      Boolean bm, Boolean fbfs, Boolean kftl, Boolean sy,
                                                      String zsr, String zsrId, String zt, String zw, String file, String funcId, String funcCode, String funcName, String tableCode, String modelId, String modelName) {
        DynaBean dynaBean = new DynaBean(TABLECODE, false);
        Map<String, Object> result = new HashedMap();
        EndUser currentUser = SecurityUserHolder.getCurrentUser();
        if (Strings.isNullOrEmpty(id)) {
            serviceTemplate.buildModelCreateInfo(dynaBean);
        } else {
            dynaBean = serviceTemplate.selectOneByPk(TABLECODE, id);
            if (!currentUser.getUserId().equals(dynaBean.getStr("SY_CREATEUSERID"))) {
                result.put("success", false);
                result.put("msg", "不是创建者，不能修改！");
                return result;
            }
            //删除权限表
            serviceTemplate.deleteByWehreSql("JE_CORE_MICROMAIL_INBOX", " AND JE_CORE_MICROMAIL_ID='" + id + "'");
        }
        dynaBean.setStr("MICROMAIL_FUNCID", funcId);//功能id
        dynaBean.setStr("MICROMAIL_FUNCCODE", funcCode);//功能Code
        dynaBean.setStr("MICROMAIL_TABLECODE", tableCode);//表名
        dynaBean.setStr("MICROMAIL_MODELID", modelId);//数据id
        dynaBean.setStr("MICROMAIL_FUNCNAME", funcName);//功能名称
        dynaBean.setStr("MICROMAIL_ZT", zt);//主题
        dynaBean.setStr("MICROMAIL_ZW", zw);//正文
        dynaBean.setStr("MICROMAIL_FJ", file);//附件
        if (sy) {
            dynaBean.setStr("MICROMAIL_SY", "1");//水印
        } else {
            dynaBean.setStr("MICROMAIL_SY", "0");//水印
        }
        if (bm) {
            dynaBean.setStr("MICROMAIL_BM", "1");//保密
        } else {
            dynaBean.setStr("MICROMAIL_BM", "0");//保密
        }
        if (fbfs) {
            dynaBean.set("MICROMAIL_FBFS", "1");//分别发送
        } else {
            dynaBean.set("MICROMAIL_FBFS", "0");//分别发送
        }
        if (kftl) {
            dynaBean.set("MICROMAIL_KFTL", "1");//开放讨论
        } else {
            dynaBean.set("MICROMAIL_KFTL", "0");//开放讨论
        }
        dynaBean.setStr("MICROMAIL_MODELNAME", modelName);//数据名称
        dynaBean.setStr("MICROMAIL_UPDATETIME", DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));//修改时间

        if (Strings.isNullOrEmpty(id)) {
            dynaBean = serviceTemplate.insert(dynaBean);
            addReadMicromail(dynaBean.getPkValue(), currentUser.getUserId(), "1");
        } else {
            String updateInfoSql = String.format("UPDATE JE_CORE_MICROMAIL_INFO SET INFO_PLYD='%s',INFO_SFYD='0' WHERE INFO_WYID='%s' AND SY_CREATEUSERID!='%s'", '0', id, currentUser.getUserId());
            serviceTemplate.executeSql(updateInfoSql);
            dynaBean = serviceTemplate.update(dynaBean);
        }
        if (Strings.isNullOrEmpty(dynaBean.getStr("JE_CORE_MICROMAIL_ID"))) {
            result.put("success", false);
            result.put("msg", "创建失败！");
            return result;
        } else {
            //创建权限数据
            createPermission(dynaBean.getStr("JE_CORE_MICROMAIL_ID"), recipient, modelId);
            //通知
        }
        result.put("success", true);
        String title = "微邮通知";
        String text = "";
        if (Strings.isNullOrEmpty(id)) {
            result.put("msg", "创建成功！");
            text = currentUser.getUsername() + "发布了新的微邮，请及时查看！";
        } else {
            result.put("msg", "修改成功！");
            text = currentUser.getUsername() + "修改了微邮，请及时查看！";
        }
        sendWeiYouMessage(title, text, dynaBean.getPkValue(), "");
        return result;
    }

    private void sendWeiYouMessage(String title, String text, String pkValue, String sendUserId) {
        StringBuffer whereSql2 = new StringBuffer();
        whereSql2.append(String.format(" AND JE_CORE_MICROMAIL_ID='%s'", pkValue));
        if (Strings.isNullOrEmpty(sendUserId)) {
            List<DynaBean> receiveUser = serviceTemplate.selectList("JE_CORE_MICROMAIL_INBOX", whereSql2.toString(), "INBOX_TYPE,SY_ORDERINDEX,INBOX_JSR,INBOX_JSRID,INBOX_SFSZSR");
            //获取人数
            List<DynaBean> listUser = getUserList(receiveUser, pkValue, "1");//未读
            StringBuffer sendUserIds = new StringBuffer();
            for (DynaBean by : listUser) {
                if (Strings.isNullOrEmpty(sendUserIds.toString())) {
                    sendUserIds.append(by.getStr("USERID"));
                } else {
                    sendUserIds.append("," + by.getStr("USERID"));
                }
            }
            sendUserId = sendUserIds.toString();
        }
        addSendMsg(title, text, sendUserId, pkValue);
    }

    @Override
    public MicromailVo getMicromailById(String id) {
        String userId = SecurityUserHolder.getCurrentUser().getUserId();
        DynaBean dynaBean = serviceTemplate.selectOneByPk("JE_CORE_MICROMAIL", id);
        StringBuffer whereSql = new StringBuffer();
        whereSql.append(String.format(" AND JE_CORE_MICROMAIL_ID='%s' ORDER BY SY_ORDERINDEX ASC", id));
        List<DynaBean> receiveUser = serviceTemplate.selectList("JE_CORE_MICROMAIL_INBOX", whereSql.toString(), "INBOX_TYPE,SY_ORDERINDEX,INBOX_JSR,INBOX_JSRID,INBOX_SFSZSR");
        //获取人数
        long i = getUserNumber(receiveUser);
        dynaBean.setLong("TOTALNUM", i);
        //点赞数量,阅读数量
        StringBuffer numSql = new StringBuffer("");
        numSql.append(String.format("SELECT (SELECT COUNT(1) FROM JE_CORE_MICROMAIL_INFO WHERE INFO_WYID='%s' AND INFO_SFDZ='1') AS DZNUM,", id));
        numSql.append(String.format("(SELECT COUNT(1) FROM JE_CORE_MICROMAIL_INFO WHERE INFO_WYID='%s' AND INFO_SFYD='1') AS YDNUM", id));
        numSql.append(String.format(" FROM JE_CORE_MICROMAIL_INFO WHERE INFO_WYID='%s' GROUP BY INFO_WYID", id));
        List<Map> map = serviceTemplate.queryMapBySql(numSql.toString());
        if (map.size() > 0) {
            Map<String, Object> numMap = map.get(0);
            String dzNumStr = numMap.get("DZNUM").toString();
            String ydNumStr = numMap.get("YDNUM").toString();
            dynaBean.setLong("DZNUM", Long.valueOf(dzNumStr).longValue());
            //判断阅读数，如果是自己发的并且没有在收件人，剔除
            String createUserid = dynaBean.getStr("SY_CREATEUSERID");
            long createUserReadCount = serviceTemplate.selectCount("JE_CORE_MICROMAIL_INFO", String.format(" AND INFO_WYID='%s' AND SY_CREATEUSERID='%s'", id, createUserid));
            if (createUserReadCount > 0) {
                String jsrSql = getWhereOrSql(id, createUserid, "1");//去除创建者阅读数量
                long myINBOX = 0;
                if (Strings.isNullOrEmpty(jsrSql)) {
                    serviceTemplate.selectCount("JE_CORE_MICROMAIL_INBOX", " AND JE_CORE_MICROMAIL_ID='" + id + "'");
                } else {
                    serviceTemplate.selectCount("JE_CORE_MICROMAIL_INBOX", " AND (" + jsrSql + ") AND JE_CORE_MICROMAIL_ID='" + id + "'");
                }
                if (myINBOX == 0) {
                    dynaBean.setLong("YDNUM", Long.valueOf(ydNumStr).longValue() - 1);
                } else {
                    dynaBean.setLong("YDNUM", Long.valueOf(ydNumStr).longValue());
                }
            } else {
                dynaBean.setLong("YDNUM", Long.valueOf(ydNumStr).longValue());
            }
        }
        MicromailVo vo = MicromailVo.build(dynaBean, receiveUser);
        return vo;
    }

    private long getUserNumber(List<DynaBean> users) {
        StringBuffer dept = new StringBuffer();
        StringBuffer user = new StringBuffer();
        StringBuffer role = new StringBuffer();
        StringBuffer company = new StringBuffer();
        EndUser currentUser = SecurityUserHolder.getCurrentUser();
        long count = 0;
        for (DynaBean qx : users) {
            String type = qx.getStr("INBOX_TYPE");
            if (type.equals("all")) {
                String zhid = currentUser.getZhId();
                String whereSql = String.format(" AND ZHID='%s' AND STATUS='1'", zhid);
                return serviceTemplate.selectCount("JE_CORE_ENDUSER", whereSql);
            } else if (type.equals("dept")) {
                dept.append(String.format(" OR DEPTID='%s' ", qx.getStr("INBOX_JSRID")));
            } else if (type.equals("user")) {
                user.append(String.format(" OR USERID='%s' ", qx.getStr("INBOX_JSRID")));
            } else if (type.equals("role")) {
                role.append(String.format(" OR ROLEIDS like '%s' ", "%" + qx.getStr("INBOX_JSRID") + "%"));
            } else if (type.equals("company")) {
                company.append(String.format(" OR JTGSID='%s' ", qx.getStr("INBOX_JSRID")));
            }
        }
        StringBuffer whereSql = new StringBuffer(" AND (STATUS!='0' OR STATUS IS NULL) AND (1!=1 ");
        whereSql.append(dept);
        whereSql.append(user);
        whereSql.append(role);
        whereSql.append(company);
        whereSql.append(")");
        count = serviceTemplate.selectCount("JE_CORE_ENDUSER", whereSql.toString());
        return count;
    }

    private List getUserList(List<DynaBean> users, String id, String readType) {
        String userId = SecurityUserHolder.getCurrentUser().getUserId();
        StringBuffer dept = new StringBuffer();
        StringBuffer user = new StringBuffer();
        StringBuffer role = new StringBuffer();
        StringBuffer company = new StringBuffer();
        EndUser currentUser = SecurityUserHolder.getCurrentUser();
        for (DynaBean qx : users) {
            String type = qx.getStr("INBOX_TYPE");
            if (type.equals("all")) {
                String zhid = currentUser.getZhId();
                dept.append(String.format(String.format(" OR ZHID='%s'", zhid)));
            } else if (type.equals("dept")) {
                dept.append(String.format(" OR DEPTID='%s' ", qx.getStr("INBOX_JSRID")));
            } else if (type.equals("user")) {
                user.append(String.format(" OR USERID='%s' ", qx.getStr("INBOX_JSRID")));
            } else if (type.equals("role")) {
                role.append(String.format(" OR ROLEIDS like '%s' ", "%" + qx.getStr("INBOX_JSRID") + "%"));
            } else if (type.equals("company")) {
                company.append(String.format(" OR JTGSID='%s' ", qx.getStr("INBOX_JSRID")));
            }
        }
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT USERNAME,USERID,3 AS READNUMBER ");
        sql.append(" FROM  JE_CORE_ENDUSER where 1=1 ");
        sql.append(" AND (1!=1");
        sql.append(dept);
        sql.append(user);
        sql.append(role);
        sql.append(company);
        if (readType.equals("1")) {//未读
            sql.append(String.format(") AND STATUS='1' AND USERID NOT IN (SELECT SY_CREATEUSERID FROM JE_CORE_MICROMAIL_INFO WHERE INFO_WYID='%s' and INFO_SFYD='1')", id));
        } else if (readType.equals("2")) {//全部
            sql.append(String.format(") AND STATUS='1' AND USERID NOT IN (SELECT SY_CREATEUSERID FROM JE_CORE_MICROMAIL_INFO WHERE INFO_WYID='%s' )", id));
        }
        List<Map> userList = serviceTemplate.queryMapBySql(sql.toString());
        List<DynaBean> listDy = new ArrayList<>();
        for (Map map : userList) {
            String userid = map.get("USERID").toString();
            String userName = map.get("USERNAME").toString();
            String number = map.get("READNUMBER").toString();
            DynaBean dynaBean = new DynaBean();
            dynaBean.setStr("USERID", userid);
            dynaBean.setStr("USERNAME", userName);
            dynaBean.setStr("NUMBER", number);
            listDy.add(dynaBean);
        }
        return listDy;
    }

    @Override
    public Map<String, Object> getMicromailUpsById(String id, String pageNumber, String limit) {
        int start = Integer.parseInt(pageNumber);
        int limitInt = Integer.parseInt(limit);
        String whereSql = String.format("AND INFO_WYID='%s' AND INFO_SFDZ='1' ORDER BY INFO_DZSJ DESC", id);
        if (limitInt == -1) {
            start = 0;
            limitInt = 20000;
        }
        List<DynaBean> lists = serviceTemplate.selectList("JE_CORE_MICROMAIL_INFO", whereSql, start, limitInt);
        long l = serviceTemplate.selectCount("JE_CORE_MICROMAIL_INFO", whereSql);
        List<ReadInfoVo> readInfoVoList = new ArrayList<>();
        for (DynaBean d : lists) {
            ReadInfoVo readInfoVo = ReadInfoVo.build(d, "2");
            readInfoVoList.add(readInfoVo);
        }
        Map<String, Object> result = new HashedMap();
        result.put("list", readInfoVoList);
        result.put("totalCount", l);
        return result;
    }

    @Override
    public Map<String, Object> getReadMicromailList(String id, String lasgMsgIdId, String limit) {
        Map<String, Object> result = new HashedMap();
        //判断阅读数，如果是自己发的并且没有在收件人，剔除
        String userId = SecurityUserHolder.getCurrentUser().getUserId();
        DynaBean dynaBean = serviceTemplate.selectOneByPk("JE_CORE_MICROMAIL", id, "SY_CREATEUSERID");
        String createUserid = dynaBean.getStr("SY_CREATEUSERID");
        //剔除sql
        String userSql = "";
        String jsrSql = getWhereOrSql(id, createUserid, "1");
        long myINBOX = 0;
        if (Strings.isNullOrEmpty(jsrSql)) {
            myINBOX = serviceTemplate.selectCount("JE_CORE_MICROMAIL_INBOX", " AND JE_CORE_MICROMAIL_ID='" + id + "'");
        } else {
            myINBOX = serviceTemplate.selectCount("JE_CORE_MICROMAIL_INBOX", " AND (" + jsrSql + ") AND JE_CORE_MICROMAIL_ID='" + id + "'");
        }
        if (myINBOX == 0) {
            userSql = String.format(" AND SY_CREATEUSERID!='%s'", createUserid);
        }
        String whereSql = String.format("AND INFO_WYID='%s' AND INFO_SFYD='1' %s ORDER BY INFO_YDSJ DESC", id, userSql);
        int start = Integer.parseInt(lasgMsgIdId);
        int limitInt = Integer.parseInt(limit);
        start = (start - 1) * limitInt;
        if (limitInt == -1) {
            start = 0;
            limitInt = 20000;
        }
        List<DynaBean> lists = serviceTemplate.selectList("JE_CORE_MICROMAIL_INFO", whereSql, start, limitInt);
        long l = serviceTemplate.selectCount("JE_CORE_MICROMAIL_INFO", whereSql);
        List<ReadInfoVo> readInfoVoList = new ArrayList<>();
        for (DynaBean d : lists) {
            ReadInfoVo readInfoVo = ReadInfoVo.build(d, "1");
            readInfoVoList.add(readInfoVo);
        }
        result.put("list", readInfoVoList);
        result.put("totalCount", l);
        return result;
    }

    @Override
    public void addReadMicromail(String id, String userId, String type) {
        DynaBean micDynaBean = serviceTemplate.selectOneByPk("JE_CORE_MICROMAIL", id);
        String createUserid = micDynaBean.getStr("SY_CREATEUSERID");
        String nowTime = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        String whereSql = String.format(" AND SY_CREATEUSERID='%s' AND INFO_WYID='%s'", userId, id);
        DynaBean dynaBean = serviceTemplate.selectOne("JE_CORE_MICROMAIL_INFO", whereSql);
        if (Objects.isNull(dynaBean)) {
            dynaBean = new DynaBean("JE_CORE_MICROMAIL_INFO", true);
            if (type.equals("3")) {
                dynaBean.setStr("INFO_SFGZ", "1");
                dynaBean.setStr("INFO_WYID", id);
                serviceTemplate.buildModelCreateInfo(dynaBean);
                serviceTemplate.insert(dynaBean);
            } else {
                dynaBean.setStr("INFO_WYID", id);
                if (type.equals("1")) {
                    dynaBean.setStr("INFO_YDSJ", nowTime);
                    dynaBean.setStr("INFO_SFYD", "1");
                    dynaBean.setStr("INFO_PLYD", "1");
                    String title = "微邮通知";
                    String text = String.format("%s查看了您的微邮！", SecurityUserHolder.getCurrentUser().getUsername());
                    sendWeiYouMessage(title, text, dynaBean.getPkValue(), createUserid);
                }
                if (type.equals("2")) {
                    dynaBean.setStr("INFO_WYID", id);
                    dynaBean.setStr("INFO_SFDZ", "1");
                    dynaBean.setStr("INFO_DZSJ", nowTime);
                }
                serviceTemplate.buildModelCreateInfo(dynaBean);
                serviceTemplate.insert(dynaBean);
            }
        } else {
            if (type.equals("3")) {
                if (dynaBean.getStr("INFO_SFGZ").equals("1")) {
                    dynaBean.setStr("INFO_SFGZ", "0");
                } else {
                    dynaBean.setStr("INFO_SFGZ", "1");
                }
                serviceTemplate.update(dynaBean);
            } else {
                if (type.equals("2")) {
                    if (dynaBean.getStr("INFO_SFDZ").equals("1")) {
                        dynaBean.setStr("INFO_SFDZ", "0");
                    } else {
                        dynaBean.setStr("INFO_SFDZ", "1");
                    }
                    dynaBean.setStr("INFO_DZSJ", nowTime);
                } else if (type.equals("1")) {
                    dynaBean.setStr("INFO_SFYD", "1");
                    dynaBean.setStr("INFO_PLYD", "1");
                    //如果阅读时间为空，说明没有阅读，发送通知加时间
                    if (Strings.isNullOrEmpty(dynaBean.getStr("INFO_YDSJ"))) {
                        dynaBean.setStr("INFO_YDSJ", nowTime);
                        String title = "微邮通知";
                        String text = String.format("%s查看了您的微邮！", SecurityUserHolder.getCurrentUser().getUsername());
                        sendWeiYouMessage(title, text, dynaBean.getPkValue(), createUserid);
                    }
                }
                serviceTemplate.update(dynaBean);
            }
        }
    }

    @Override
    public Map<String, Object> getNoReadMicromailList(String id, String lasgMsgIdId, String limit) {
        Map<String, Object> result = new HashedMap();
        int start = Integer.parseInt(lasgMsgIdId);
        int limitInt = Integer.parseInt(limit);
        start = (start - 1) * limitInt;
        if (limitInt == -1) {
            start = 0;
            limitInt = 20000;
        }
        StringBuffer whereSql2 = new StringBuffer();
        whereSql2.append(String.format(" AND JE_CORE_MICROMAIL_ID='%s'", id));
        List<DynaBean> receiveUser = serviceTemplate.selectList("JE_CORE_MICROMAIL_INBOX", whereSql2.toString(), "INBOX_TYPE,SY_ORDERINDEX,INBOX_JSR,INBOX_JSRID,INBOX_SFSZSR");
        //获取人数
        List<DynaBean> listUser = getUserList(receiveUser, id, "1");//未读
        List<ReadInfoVo> readInfoVoList = new ArrayList<>();
        for (DynaBean d : listUser) {
            ReadInfoVo readInfoVo = ReadInfoVo.buildUser(d);
            readInfoVoList.add(readInfoVo);
        }
        result.put("list", readInfoVoList);
        result.put("totalCount", listUser.size());
        return result;
    }

    @Override
    public Map<String, Object> toMicromailRemind(String id, String userIdList) {
        String[] userids = userIdList.split(",");
        Map<String, Object> result = new HashedMap();
        int okInt = 0;
        StringBuffer msg = new StringBuffer();
        for (String userId : userids) {
            DynaBean user = serviceTemplate.selectOne("JE_CORE_ENDUSER", String.format("AND USERID='%s'", userId), "USERNAME");
            String whereSql = String.format(" AND REMINDNUM_WYID='%s' AND REMINDNUM_USERID='%s'", id, userId);
            DynaBean dynaBean = serviceTemplate.selectOne("JE_CORE_MICROMAIL_REMINDNUM", whereSql);
            //如果没有查到记录
            if (Objects.isNull(dynaBean)) {
                String INFOWhereSql = String.format(" AND INFO_SFYD='%s' AND INFO_WYID='%s' AND SY_CREATEUSERID='%s'", "1", id, userId);
                DynaBean INFO = serviceTemplate.selectOne("JE_CORE_MICROMAIL_INFO", INFOWhereSql);
                //查找有没有查看记录，如果没有添加提醒
                if (Objects.isNull(INFO)) {
                    dynaBean = new DynaBean("JE_CORE_MICROMAIL_REMINDNUM", true);
                    //提醒成功
                    okInt++;
                    serviceTemplate.buildModelCreateInfo(dynaBean);
                    dynaBean.setStr("REMINDNUM_USERID", userId);
                    dynaBean.setStr("REMINDNUM_WYID", id);
                    dynaBean.setStr("REMINDNUM_NUMBER", "2");
                    serviceTemplate.insert(dynaBean);
                } else {//提醒失败,该用户已阅读
                    if (msg.toString().length() == 0) {
                        msg.append(user.getStr("USERNAME"));
                    } else {
                        msg.append("、" + user.getStr("USERNAME"));
                    }
                }
            } else {
                String count = dynaBean.getStr("REMINDNUM_NUMBER");
                if (!Strings.isNullOrEmpty(count) && Integer.parseInt(count) > 0) {//成功
                    dynaBean.setStr("REMINDNUM_NUMBER", String.valueOf(Integer.parseInt(count) - 1));
                    serviceTemplate.update(dynaBean);
                    okInt++;
                } else {//失败
                    if (msg.toString().length() == 0) {
                        msg.append(user.getStr("USERNAME"));
                    } else {
                        msg.append("、" + user.getStr("USERNAME"));
                    }
                }
            }
        }
        result.put("success", true);
        if (msg.toString().length() == 0) {
            result.put("msg", String.format("提醒成功%s人", okInt));
        } else {
            result.put("msg", String.format("提醒成功%s人,%s提醒失败！", okInt, msg.toString()));
        }
        String title = "微邮通知";
        String text = String.format("%s已再次提醒您，请及时查看微邮！", SecurityUserHolder.getCurrentUser().getUsername());
        sendWeiYouMessage(title, text, id, userIdList);
        return result;
    }

    @Override
    public Map<String, Object> getMicromailComments(String id, String pageNumber, String limit) {
        Map<String, Object> result = new HashedMap();
        int start = Integer.parseInt(pageNumber);
        int limitInt = Integer.parseInt(limit);
        start = (start - 1) * limitInt;
        if (limitInt == -1) {
            start = 0;
            limitInt = 20000;
        }
        List<CommentVo> lists = new ArrayList<>();
        String whereSql = String.format(" AND JE_CORE_MICROMAIL_ID = '%s' AND (SY_STATUS !='0' OR SY_STATUS IS NULL) ORDER BY COMMENT_UPDATETIME DESC", id);
        List<DynaBean> dynaBeanList = serviceTemplate.selectList("JE_CORE_MICROMAIL_COMMENT", whereSql, start, limitInt);
        long l = serviceTemplate.selectCount("JE_CORE_MICROMAIL_COMMENT", whereSql);
        for (DynaBean dy : dynaBeanList) {
            CommentVo cv = CommentVo.build(dy);
            lists.add(cv);
        }
        result.put("list", lists);
        result.put("totalCount", l);
        return result;
    }

    @Override
    public Map<String, Object> deleteMicromail(String id) {
        Map<String, Object> result = new HashedMap();
        DynaBean dynaBean = serviceTemplate.selectOneByPk("JE_CORE_MICROMAIL", id);
        EndUser currentUser = SecurityUserHolder.getCurrentUser();
        String createUserid = dynaBean.getStr("SY_CREATEUSERID");
        String msg = "";
        if (createUserid.equals(currentUser.getUserId())) {
            dynaBean.setStr("SY_STATUS", "0");
            serviceTemplate.update(dynaBean);
            msg = "删除成功！";
            result.put("success", true);
        } else {
            result.put("success", false);
            msg = "不是微邮创建者，删除失败！";
        }
        result.put("msg", msg);
        String wyTitle = dynaBean.getStr("MICROMAIL_ZT");
        if (wyTitle.length() > 20) {
            wyTitle = dynaBean.getStr("MICROMAIL_ZT").substring(0, 19) + "...";
        }
        String title = "微邮通知";
        String text = String.format("%s已删除微邮:%s！", currentUser.getUsername(), wyTitle);
        sendWeiYouMessage(title, text, id, "");
        return result;
    }

    @Override
    public Map<String, Object> deleteComment(String id) {
        Map<String, Object> result = new HashedMap();
        DynaBean dynaBean = serviceTemplate.selectOneByPk("JE_CORE_MICROMAIL_COMMENT", id);
        EndUser currentUser = SecurityUserHolder.getCurrentUser();
        String createUserid = dynaBean.getStr("SY_CREATEUSERID");
        String msg = "";
        if (createUserid.equals(currentUser.getUserId())) {
            dynaBean.setStr("SY_STATUS", "0");
            serviceTemplate.update(dynaBean);
            String micId = dynaBean.getStr("JE_CORE_MICROMAIL_ID");
            String whereSql = String.format(" SELECT * FROM JE_CORE_MICROMAIL_COMMENT WHERE JE_CORE_MICROMAIL_ID='%s' AND (SY_STATUS!='0' OR SY_STATUS IS NULL) ORDER BY COMMENT_UPDATETIME DESC", micId);
            List<Map> list = pcServiceTemplate.queryMapBySql(whereSql, 0, 1);
            DynaBean micBean = serviceTemplate.selectOneByPk("JE_CORE_MICROMAIL", micId);
            if (!Objects.isNull(list) && list.size() > 0) {
                Map com = list.get(0);
                micBean.setStr("MICROMAIL_LASTMSG", com.get("COMMENT_ZW").toString());
                micBean.setStr("MICROMAIL_LASTUSER", com.get("SY_CREATEUSERNAME").toString());
                micBean.setStr("MICROMAIL_LASTUSERID", com.get("SY_CREATEUSERID").toString());
            } else {
                micBean.setStr("MICROMAIL_LASTMSG", "");
                micBean.setStr("MICROMAIL_LASTUSER", "");
                micBean.setStr("MICROMAIL_LASTUSERID", "");
            }
            serviceTemplate.update(micBean);
            msg = "删除成功！";
            result.put("success", true);
        } else {
            result.put("success", false);
            msg = "不是评论创建者，删除失败！";
        }
        result.put("msg", msg);

        return result;
    }

    @Override
    public CommentVo createComment(String id, String hfr, String hfrId, String file, String zw, String fjId) {
        String userId = SecurityUserHolder.getCurrentUser().getUserId();
        DynaBean dynaBean = new DynaBean("JE_CORE_MICROMAIL_COMMENT", true);
        serviceTemplate.buildModelCreateInfo(dynaBean);
        String createTime = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        dynaBean.setStr("COMMENT_ATR", hfr);
        dynaBean.setStr("JE_CORE_MICROMAIL_ID", id);
        dynaBean.setStr("COMMENT_CREATEDTIME", createTime);
        dynaBean.setStr("COMMENT_ATRID", hfrId);
        dynaBean.setStr("COMMENT_FJ", file);
        dynaBean.setStr("COMMENT_ZW", zw);
        dynaBean.setStr("COMMENT_FJID", fjId);
        dynaBean.setStr("COMMENT_UPDATETIME", createTime);
        dynaBean = serviceTemplate.insert(dynaBean);
        String isMsgFile = "0";
        if (file.length() > 0) {
            isMsgFile = "1";
        }
        CommentVo commentVo = CommentVo.build(dynaBean);
        serviceTemplate.executeSql(String.format("UPDATE JE_CORE_MICROMAIL SET MICROMAIL_UPDATETIME='%s',MICROMAIL_LASTUSER='%s'" +
                        ",MICROMAIL_LASTUSERID='%s',MICROMAIL_LASTMSG='%s',MICROMAIL_XXFJ='%s' WHERE JE_CORE_MICROMAIL_ID='%s'",
                createTime, dynaBean.getStr("SY_CREATEUSERNAME"), dynaBean.getStr("SY_CREATEUSERID"), zw, isMsgFile, id));
        //评论阅读改为0
        String updateInfoSql = String.format("UPDATE JE_CORE_MICROMAIL_INFO SET INFO_PLYD='%s',INFO_SFYD='0' WHERE INFO_WYID='%s' AND SY_CREATEUSERID!='%s'", '0', id, userId);
        //如果是自己发的评论,自动置为已读
//    DynaBean readInfo = serviceTemplate.selectOne("JE_CORE_MICROMAIL_INFO",String.format(" AND SY_CREATEUSERID='%s'",userId));
//    if(Objects.isNull(readInfo)){
//      readInfo = new DynaBean("JE_CORE_MICROMAIL_INFO",true);
//      serviceTemplate.buildModelCreateInfo(readInfo);
//      readInfo.setStr("","");
//    }
        serviceTemplate.executeSql(updateInfoSql);
        return commentVo;
    }

    /**
     * @Description //创建权限数据
     * @Date 2019/10/10
     * @return: void
     **/
    private void createPermission(String id, Map<String, Map<String, Object>> recipient, String modelId) {
        List<Map<String, Object>> dept = new ArrayList<>();
        List<Map<String, Object>> user = new ArrayList<>();
        List<Map<String, Object>> role = new ArrayList<>();
        List<Map<String, Object>> all = new ArrayList<>();
        List<Map<String, Object>> company = new ArrayList<>();
        for (Map.Entry<String, Map<String, Object>> entry : recipient.entrySet()) {
            Map<String, Object> map = entry.getValue();
            String type = String.valueOf(map.get("type"));
            if (type.equals("dept")) {
                dept.add(map);
            }
            if (type.equals("user")) {
                user.add(map);
            }
            if (type.equals("role")) {
                role.add(map);
            }
            if (type.equals("all")) {
                all.add(map);
            }
            if (type.equals("company")) {
                company.add(map);
            }
        }
        if (!Objects.isNull(all)) {//所有人
            for (Map<String, Object> m : all) {
                if (Objects.isNull(m.get("order"))) {
                    m.put("order", 0);
                }
                String zsr = "false";
                Double order = (Double) m.get("order");
                savePermission("all", String.valueOf(m.get("name")), zsr, id, "all",
                        modelId, order.intValue(),
                        String.valueOf(m.get("code")), String.valueOf(m.get("photo")), String.valueOf(m.get("photoUrl")));
            }
        }
        if (!Objects.isNull(company)) {//分公司
            for (Map<String, Object> m : company) {
                if (Objects.isNull(m.get("order"))) {
                    m.put("order", 0);
                }
                String zsr = "false";
                Double order = (Double) m.get("order");
                savePermission(String.valueOf(m.get("id")), String.valueOf(m.get("name")), zsr, id, "company", modelId,
                        order.intValue(), String.valueOf(m.get("code")), String.valueOf(m.get("photo")), String.valueOf(m.get("photoUrl")));
            }
        }
        if (!Objects.isNull(dept)) {//部门
            for (Map<String, Object> m : dept) {
                if (Objects.isNull(m.get("order"))) {
                    m.put("order", 0);
                }
                String zsr = "false";
                Double order = (Double) m.get("order");
                savePermission(String.valueOf(m.get("id")), String.valueOf(m.get("name")), zsr, id, "dept", modelId,
                        order.intValue(), String.valueOf(m.get("code")), String.valueOf(m.get("photo")), String.valueOf(m.get("photoUrl")));
            }
        }
        if (!Objects.isNull(user)) {//人员
            for (Map<String, Object> m : user) {
                if (Objects.isNull(m.get("order"))) {
                    m.put("order", 0);
                }
                Double order = 0.0;
                try {
                    order = (Double) m.get("order");
                } catch (Exception e) {
                    Integer i = (Integer) m.get("order");
                    order = i.doubleValue();
                }
                savePermission(String.valueOf(m.get("id")), String.valueOf(m.get("name")), m.get("sfszsr").toString(), id, "user",
                        modelId, order.intValue(), String.valueOf(m.get("code")), String.valueOf(m.get("photo")), String.valueOf(m.get("photoUrl")));
            }
        }
        if (!Objects.isNull(role)) {//角色
            for (Map<String, Object> m : role) {
                if (Objects.isNull(m.get("order"))) {
                    m.put("order", 0);
                }
                String zsr = "false";
                Double order = (Double) m.get("order");
                savePermission(String.valueOf(m.get("id")), String.valueOf(m.get("name")), zsr, id, "role",
                        modelId, order.intValue(), String.valueOf(m.get("code")), String.valueOf(m.get("photo")), String.valueOf(m.get("photoUrl")));
            }
        }
    }

    /**
     * @Description //微邮权限表
     * @Date 2019/10/11
     * @Param jsrId: 接收人id
     * @Param jsr: 接收人
     * @Param sfszsr: 是否是主送人
     * @Param wyid: 微邮id
     * @Param type:类型
     * @Param modelId:
     * @return: void
     **/
    private void savePermission(String jsrId, String jsr, String sfszsr, String wyid, String type, String modelId, int order, String code, String photo, String photoUrl) {
        DynaBean dynaBean = new DynaBean("JE_CORE_MICROMAIL_INBOX", false);
        dynaBean.setStr("INBOX_JSR", jsr);
        dynaBean.setStr("INBOX_JSRID", jsrId);
        if (sfszsr.equals("true")) {
            dynaBean.set("INBOX_SFSZSR", "1");
        } else {
            dynaBean.set("INBOX_SFSZSR", "0");
        }
        dynaBean.setStr("JE_CORE_MICROMAIL_ID", wyid);
        dynaBean.setStr("INBOX_TYPE", type);
        dynaBean.setStr("INBOX_MODELID", modelId);
        dynaBean.setInt("SY_ORDERINDEX", order);
        dynaBean.setStr("INBOX_CODE", code);
        dynaBean.setStr("INBOX_TX", photo);
        dynaBean.setStr("INBOX_TXTP", photoUrl);
        serviceTemplate.buildModelCreateInfo(dynaBean);
        serviceTemplate.insert(dynaBean);
    }


    /**
     * @Description //获取微邮sql
     * @Date 2019/10/21
     * @Param type: 1 我收到的 2 我发出的 3我收藏的
     * @return: id：数据id主键
     **/
    private StringBuffer getSql(String type, String id) {
        String userId = SecurityUserHolder.getCurrentUser().getUserId();
        //获取当前登录人的权限
        String whereOrSql = getWhereOrSql(id, userId, "0");
        StringBuffer sql = new StringBuffer();
        sql.append("select MIC1.MICROMAIL_LASTMSG,MIC1.MICROMAIL_ZT,MIC1.MICROMAIL_TABLECODE,MIC1.MICROMAIL_KFTL,MIC1.MICROMAIL_FJ,MIC1.MICROMAIL_LASTUSER,MIC1.SY_CREATEUSERNAME,");
        sql.append("MIC1.MICROMAIL_MODELID,MIC1.MICROMAIL_FUNCNAME,MIC1.SY_CREATEUSERID,MIC1.MICROMAIL_FBFS,MIC1.MICROMAIL_FUNCID,MIC1.JE_CORE_MICROMAIL_ID,");
        sql.append("MIC1.MICROMAIL_LASTUSERID,MIC1.MICROMAIL_FUNCCODE,MIC1.MICROMAIL_BM,MIC1.MICROMAIL_MODELNAME,MIC1.MICROMAIL_ZW,MIC1.MICROMAIL_UPDATETIME,MIC1.MICROMAIL_SY,");
        sql.append("MIC2.INFO_SFYD AS INFO_SFYD,MIC2.INFO_YDSJ,MIC2.INFO_DZSJ,MIC2.INFO_PLYD AS INFO_PLYD,MIC2.INFO_SFGZ AS INFO_SFGZ,MIC2.INFO_SFDZ AS INFO_SFDZ,");
        sql.append("MIC2.INFO_WYID AS INFO_WYID,MIC2.DZSL FROM JE_CORE_MICROMAIL MIC1,(SELECT * FROM ");
        sql.append("(SELECT a.SY_CREATEUSERID,a.JE_CORE_MICROMAIL_ID,a.MICROMAIL_UPDATETIME,b.INFO_SFYD AS INFO_SFYD,");
        sql.append("b.INFO_YDSJ,b.INFO_DZSJ,b.INFO_PLYD AS INFO_PLYD,b.INFO_SFGZ AS INFO_SFGZ,b.INFO_SFDZ AS INFO_SFDZ,b.INFO_WYID AS INFO_WYID,");
        sql.append("(SELECT count(*) AS num FROM JE_CORE_MICROMAIL_INFO WHERE INFO_WYID = a.JE_CORE_MICROMAIL_ID AND INFO_SFDZ = '1' ) AS DZSL ");
        sql.append("FROM ( SELECT JE_CORE_MICROMAIL.SY_STATUS,JE_CORE_MICROMAIL.SY_CREATEUSERID,JE_CORE_MICROMAIL.JE_CORE_MICROMAIL_ID,MICROMAIL_UPDATETIME,MICROMAIL_SY,INBOX_JSRID,MICROMAIL_BM,MICROMAIL_MODELID ");
        sql.append("FROM JE_CORE_MICROMAIL LEFT JOIN JE_CORE_MICROMAIL_INBOX ON JE_CORE_MICROMAIL.JE_CORE_MICROMAIL_ID = JE_CORE_MICROMAIL_INBOX.JE_CORE_MICROMAIL_ID ) a ");
        sql.append("LEFT JOIN ( SELECT INFO_SFYD, INFO_YDSJ, INFO_DZSJ, INFO_PLYD, INFO_SFGZ, INFO_SFDZ, INFO_WYID, ");
        sql.append(String.format("SY_CREATEUSERID AS YDUSERID FROM JE_CORE_MICROMAIL_INFO WHERE SY_CREATEUSERID = '%s' )", userId));
        sql.append("b ON a.JE_CORE_MICROMAIL_ID = b.INFO_WYID ");
        sql.append(" WHERE (");
        if (type.equals("1")) {//我收到的，权限在收件人中的
            sql.append(whereOrSql);
        } else if (type.equals("2")) {//我发出的，是我创建的
            sql.append(String.format(" SY_CREATEUSERID = '%s' ", userId));
        }
        if (!Strings.isNullOrEmpty(id)) {
            sql.append(" OR MICROMAIL_BM='0' ");
        }
        if (type.equals("1") && !Strings.isNullOrEmpty(id)) {
            sql.append(String.format(" AND SY_CREATEUSERID!='%s' ", userId));
        }
        String groupBySql = " GROUP BY JE_CORE_MICROMAIL_ID,SY_CREATEUSERID,MICROMAIL_UPDATETIME,INFO_SFYD,INFO_YDSJ,INFO_DZSJ,INFO_PLYD, INFO_SFGZ, INFO_SFDZ,INFO_WYID ";
        if (Strings.isNullOrEmpty(id)) {
            sql.append(String.format(" ) AND (SY_STATUS!='0' OR SY_STATUS IS NULL) %s ) weiyou  ) mic2 where mic1.JE_CORE_MICROMAIL_ID = mic2.JE_CORE_MICROMAIL_ID  AND 1=1 ", groupBySql));
        } else {
            sql.append(String.format(" ) AND (SY_STATUS!='0' OR SY_STATUS IS NULL) AND MICROMAIL_MODELID='%s' %s ) weiyou  ) mic2 where mic1.JE_CORE_MICROMAIL_ID = mic2.JE_CORE_MICROMAIL_ID  AND 1=1 ", id, groupBySql));
        }
        return sql;
    }


    /**
     * @Description //获取数量
     * type: 1 我收到的 2 我发出的
     * readType ： 1所有 2未读
     * lable:是否是获取列表所有 true 是  false 不是
     **/
    private StringBuffer getCountSql(String type, String readType, String id, Boolean lable) {
        String userId = SecurityUserHolder.getCurrentUser().getUserId();
        String whereOrSql = getWhereOrSql(id, userId, "0");
        StringBuffer sql = new StringBuffer();
        sql.append("select count(DISTINCT JE_CORE_MICROMAIL_ID) as COUNT,MICROMAIL_MODELID FROM ( select a.MICROMAIL_LASTMSG,a.MICROMAIL_ZT,a.MICROMAIL_TABLECODE,a.MICROMAIL_KFTL,a.MICROMAIL_FJ,a.MICROMAIL_LASTUSER,a.SY_CREATEUSERNAME,a.MICROMAIL_MODELID,");
        sql.append(" a.MICROMAIL_FUNCNAME,a.SY_CREATEUSERID,a.MICROMAIL_FBFS,a.MICROMAIL_FUNCID,a.JE_CORE_MICROMAIL_ID,a.MICROMAIL_LASTUSERID,a.MICROMAIL_FUNCCODE,a.MICROMAIL_BM,a.MICROMAIL_MODELNAME,");
        sql.append(" a.MICROMAIL_ZW,a.MICROMAIL_UPDATETIME,a.MICROMAIL_SY,a.INBOX_JSRID,b.INFO_SFYD as INFO_SFYD,b.INFO_YDSJ,b.INFO_DZSJ,b.INFO_PLYD AS INFO_PLYD,b.INFO_SFGZ AS INFO_SFGZ,b.INFO_SFDZ as INFO_SFDZ,b.INFO_WYID as INFO_WYID, b.YDUSERID,(select count(*) as num FROM JE_CORE_MICROMAIL_INFO WHERE INFO_WYID=a.JE_CORE_MICROMAIL_ID  AND INFO_SFDZ='1') as DZSL FROM");
        sql.append(" (SELECT JE_CORE_MICROMAIL.SY_STATUS,JE_CORE_MICROMAIL.MICROMAIL_LASTMSG,MICROMAIL_ZT,MICROMAIL_TABLECODE,MICROMAIL_KFTL,MICROMAIL_FJ,MICROMAIL_LASTUSER,JE_CORE_MICROMAIL.SY_CREATEUSERNAME,JE_CORE_MICROMAIL.MICROMAIL_MODELID,");
        sql.append(" MICROMAIL_FUNCNAME,JE_CORE_MICROMAIL.SY_CREATEUSERID,MICROMAIL_FBFS,MICROMAIL_FUNCID,JE_CORE_MICROMAIL.JE_CORE_MICROMAIL_ID,MICROMAIL_LASTUSERID,MICROMAIL_FUNCCODE,MICROMAIL_BM,MICROMAIL_MODELNAME,");
        sql.append(" MICROMAIL_ZW,MICROMAIL_UPDATETIME,MICROMAIL_SY,INBOX_JSRID");
        sql.append(" FROM JE_CORE_MICROMAIL LEFT JOIN JE_CORE_MICROMAIL_INBOX  ON JE_CORE_MICROMAIL.JE_CORE_MICROMAIL_ID=JE_CORE_MICROMAIL_INBOX.JE_CORE_MICROMAIL_ID) a");
        sql.append(String.format(" LEFT JOIN (SELECT INFO_SFYD,INFO_YDSJ,INFO_DZSJ,INFO_PLYD,INFO_SFGZ,INFO_SFDZ,INFO_WYID, SY_CREATEUSERID as YDUSERID FROM JE_CORE_MICROMAIL_INFO WHERE  SY_CREATEUSERID='%s') b", userId));
        sql.append(" ON a.JE_CORE_MICROMAIL_ID = b.INFO_WYID");
        sql.append(" WHERE (");
        if (type.equals("1")) {//我收到的
            sql.append(whereOrSql);
        } else if (type.equals("2")) {//我发出的
            sql.append(String.format(" SY_CREATEUSERID = '%s' ", userId));
        }
        if (!Strings.isNullOrEmpty(id)) {
            sql.append(" OR MICROMAIL_BM='0' ");
        }
        String modelIdSql = "";
        if (!Strings.isNullOrEmpty(id)) {
            if (lable) {
                modelIdSql = String.format(" AND MICROMAIL_MODELID in(%s)", id);
            } else {
                modelIdSql = String.format(" AND MICROMAIL_MODELID='%s'", id);
            }
        }
        if (type.equals("1") && !Strings.isNullOrEmpty(id)) {
            sql.append(String.format(" AND SY_CREATEUSERID!='%s' ", userId));
        }
        String INFOSql = "";
        if (readType.equals("2")) {
            INFOSql = " WHERE (INFO_SFYD!='1' OR INFO_SFYD IS NULL) ";
        }
        sql.append(String.format(" ) and (SY_STATUS!='0' OR SY_STATUS IS NULL) %s) weiyou %s ", modelIdSql, INFOSql));
        return sql;
    }

    //获取人员权限 type 0 获取userID的，1 获取当前登录人的，主要用于发送消息验证
    private String getWhereOrSql(String id, String userId, String type) {
        StringBuffer orSql = new StringBuffer();
        EndUser currentUser = SecurityUserHolder.getCurrentUser();
        if (type.equals("1")) {//查询创建着信息
            DynaBean userDy = serviceTemplate.selectOne("JE_CORE_ENDUSER", String.format(" AND USERID='%s'", userId),
                    "JTGSID,USERID,DEPTID,MONITORDEPTCODE,ROLEIDS");
            if (userDy == null) {
                return "";
            }
            currentUser.setJtgsId(userDy.getStr("JTGSID"));
            currentUser.setUserId(userDy.getStr("USERID"));
            currentUser.setDeptId(userDy.getStr("DEPTID"));
            currentUser.setRoleIds(userDy.getStr("ROLEIDS"));
            currentUser.setMonitorDeptCode(userDy.getStr("MONITORDEPTCODE"));
        }
        String gs = currentUser.getJtgsId();//公司id
        String deptId = currentUser.getDeptId();//部门
        String roleIds = currentUser.getRoleIds();//角色
        String userid = currentUser.getUserId();
        orSql.append(String.format("INBOX_JSRID='%s' ", gs));
        StringBuffer monsAndDeptAndRole = new StringBuffer();
        monsAndDeptAndRole.append(userid);
        if (!Strings.isNullOrEmpty(deptId)) {
            monsAndDeptAndRole.append("," + deptId);
        }
        if (!Strings.isNullOrEmpty(roleIds)) {
            monsAndDeptAndRole.append("," + roleIds);
        }
        if (!Strings.isNullOrEmpty(id)) {//如果数据不为空，添加监管部门权限
            StringBuffer monitorDept = new StringBuffer();
            String deptCode = currentUser.getMonitorDeptCode();
            String[] deptCodes = deptCode.split(",");
            StringBuffer codes = new StringBuffer();
            for (String code : deptCodes) {
                if (Strings.isNullOrEmpty(codes.toString())) {
                    codes.append("'" + code + "'");
                } else {
                    codes.append(",'" + code + "'");
                }
            }
            if (deptCodes.length > 0) {
                List<DynaBean> depts = serviceTemplate.selectList("JE_CORE_DEPARTMENT", String.format(" AND DEPTCODE in(%s)", codes.toString()), "DEPTID");
                for (DynaBean dept : depts) {
                    String deptid = dept.getStr("DEPTID");
                    monitorDept.append("," + deptid);
                }
            }
            if (!Strings.isNullOrEmpty(monitorDept.toString())) {
                String[] jgDepts = monitorDept.toString().split(",");
                for (String monid : jgDepts) {
                    if (!Strings.isNullOrEmpty(monid)) {
                        monsAndDeptAndRole.append("," + monid);
                    }
                }
            }
        }
        String[] ids = monsAndDeptAndRole.toString().split(",");
        for (String s : ids) {
            orSql.append(String.format("OR INBOX_JSRID='%s' ", s));
        }
        orSql.append("OR INBOX_JSRID = 'ALL'");
        return orSql.toString();
    }

    //我收到的
    private Map<String, Object> getReceiveList(String id, String pageNumber, String limit, String keyWord, String startTime, String endTime, String readType) {
        //type 1 我收到的
        String countGroupBySql = " GROUP BY MICROMAIL_MODELID ";
        StringBuffer sql = getSql("1", id);
        Map<String, Object> map = new HashedMap();
        int start = Integer.parseInt(pageNumber);
        int limitInt = Integer.parseInt(limit);
        start = (start - 1) * limitInt;
        StringBuffer countSql = getCountSql("1", "1", id, false);
        countSql.append(" WHERE 1=1 ");
        //业务id
        if (!Strings.isNullOrEmpty(id)) {
            sql.append(String.format(" AND MICROMAIL_MODELID='%s' ", id));
            countSql.append(String.format(" AND MICROMAIL_MODELID='%s' ", id));
        }
        //关键字
        String keyWordSql = "";
        if (!Strings.isNullOrEmpty(keyWord)) {
            keyWordSql = String.format(" AND (MICROMAIL_ZT LIKE '%s' OR SY_CREATEUSERNAME like '%s')", "%" + keyWord + "%", "%" + keyWord + "%");
        }
        //开始时间
        String startTimeSql = "";
        String sqlStartTimeSql = "";
        if (!Strings.isNullOrEmpty(startTime)) {
            startTimeSql = String.format(" AND MICROMAIL_UPDATETIME>'%s'", startTime + " 00:00:00");
            sqlStartTimeSql = String.format(" AND MIC1.MICROMAIL_UPDATETIME>'%s'", startTime + " 00:00:00");
        }
        //结束时间
        String endTimeSql = "";
        String sqlEndTimeSql = "";
        if (!Strings.isNullOrEmpty(endTime)) {
            endTimeSql = String.format(" AND MICROMAIL_UPDATETIME<'%s'", endTime + " 23:59:59");
            sqlEndTimeSql = String.format(" AND MIC1.MICROMAIL_UPDATETIME<'%s'", endTime + " 23:59:59");
        }
        String readTypeSql = "";
        if (readType.equals("2")) {//已读
            readTypeSql = " AND INFO_SFYD='1' ";
        } else if (readType.equals("3")) {//未读
            readTypeSql = " AND (INFO_SFYD!='1' OR INFO_SFYD IS NULL)";
        } else if (readType.equals("4")) {//关注
            readTypeSql = " AND INFO_SFGZ='1'";
        } else if (readType.equals("5")) {//关注已读
            readTypeSql = " AND INFO_SFGZ='1'  AND INFO_SFYD='1' ";
        } else if (readType.equals("6")) {//关注未读
            readTypeSql = " AND INFO_SFGZ='1'  AND (INFO_SFYD!='1' OR INFO_SFYD IS NULL)";
        }
        sql.append(String.format(" %s %s %s %s ORDER BY MICROMAIL_UPDATETIME DESC ", readTypeSql, keyWordSql, sqlStartTimeSql, sqlEndTimeSql));
        int weiduNum = 0;
        int guanzhuNum = 0;
        if (!Strings.isNullOrEmpty(id)) {
            if (readType.equals("1") || readType.equals("2") || readType.equals("3")) {//全部的
                //1.未读总数
                String readNoSql = String.format("%s AND (INFO_SFYD!='1' OR INFO_SFYD IS NULL) %s %s %s %s", countSql, keyWordSql, startTimeSql, endTimeSql, countGroupBySql);
                Map countweiduMap = getCountsMap(readNoSql);
                weiduNum = Integer.parseInt(countweiduMap.get("COUNT").toString());
            } else if (readType.equals("4") || readType.equals("5") || readType.equals("6")) {//关注的
                //1.未读总数
                String readNoSql = String.format("%s AND INFO_SFGZ='1' AND (INFO_SFYD!='1' OR INFO_SFYD IS NULL) %s %s %s %s", countSql, keyWordSql, startTimeSql, endTimeSql, countGroupBySql);
                Map countweiduMap = getCountsMap(readNoSql);
                weiduNum = Integer.parseInt(countweiduMap.get("COUNT").toString());
            }
            //2.关注总数
            String readgzSql = String.format("%s AND INFO_SFGZ='1' %s %s %s %s", countSql, keyWordSql, startTimeSql, endTimeSql, countGroupBySql);
            Map countguanzhuMap = getCountsMap(readgzSql);
            guanzhuNum = Integer.parseInt(countguanzhuMap.get("COUNT").toString());
        }
        countSql.append(String.format(" %s %s %s %s %s", readTypeSql, keyWordSql, startTimeSql, endTimeSql, countGroupBySql));
        List<Map> emails = pcServiceTemplate.queryMapBySql(sql.toString(), start, limitInt);
        List<MicromailDto> dtos = new ArrayList<>();
        for (Map email : emails) {
            dtos.add(MicromailDto.build(email));
        }
        List<MicromailListVo> list = new ArrayList<>();
        for (MicromailDto d : dtos) {
            MicromailListVo vo = MicromailListVo.build(d);
            list.add(vo);
        }
        Map countMap = getCountsMap(countSql.toString());
        int num = Integer.parseInt(countMap.get("COUNT").toString());
        map.put("totalCount", num);
        map.put("list", list);
        map.put("readCount", 0);
        map.put("noReadCount", weiduNum);
        map.put("gzCount", guanzhuNum);
        return map;
    }

    //我发送的
    private Map<String, Object> getSendList(String id, String pageNumber, String limit, String keyWord, String startTime, String endTime, String readType) {
        String countGroupBySql = " GROUP BY MICROMAIL_MODELID ";
        String userId = SecurityUserHolder.getCurrentUser().getUserId();
        StringBuffer sql = getSql("2", id);
        Map<String, Object> map = new HashedMap();
        int start = Integer.parseInt(pageNumber);
        int limitInt = Integer.parseInt(limit);
        start = (start - 1) * limitInt;
        StringBuffer countSql = getCountSql("2", "1", id, false);
        countSql.append(" WHERE 1=1 ");
        if (!Strings.isNullOrEmpty(id)) {
            sql.append(String.format(" AND MICROMAIL_MODELID='%s' ", id));
            countSql.append(String.format(" AND MICROMAIL_MODELID='%s' ", id));
        }
        String keyWordSql = "";
        if (!Strings.isNullOrEmpty(keyWord)) {
            keyWordSql = String.format(" AND (MICROMAIL_ZT LIKE '%s' OR SY_CREATEUSERNAME like '%s')", "%" + keyWord + "%", "%" + keyWord + "%");
        }
        String startTimeSql = "";
        String sqlStartTimeSql = "";
        if (!Strings.isNullOrEmpty(startTime)) {
            startTimeSql = String.format(" AND MICROMAIL_UPDATETIME>'%s'", startTime + " 00:00:00");
            sqlStartTimeSql = String.format(" AND MIC1.MICROMAIL_UPDATETIME>'%s'", startTime + " 00:00:00");
        }

        String endTimeSql = "";
        String sqlEndTimeSql = "";
        if (!Strings.isNullOrEmpty(endTime)) {
            endTimeSql = String.format(" AND MICROMAIL_UPDATETIME<'%s'", endTime + " 23:59:59");
            sqlEndTimeSql = String.format(" AND MIC1.MICROMAIL_UPDATETIME<'%s'", endTime + " 23:59:59");
        }
        String readTypeSql = "";
        if (readType.equals("2")) {//已读
            readTypeSql = " AND INFO_SFYD='1' ";
        } else if (readType.equals("3")) {//未读
            readTypeSql = " AND (INFO_SFYD!='1' OR INFO_SFYD IS NULL)";
        } else if (readType.equals("4")) {//关注
            readTypeSql = " AND INFO_SFGZ='1'";
        } else if (readType.equals("5")) {//关注已读
            readTypeSql = " AND INFO_SFGZ='1'  AND INFO_SFYD='1' ";
        } else if (readType.equals("6")) {//关注未读
            readTypeSql = " AND INFO_SFGZ='1'  AND (INFO_SFYD!='1' OR INFO_SFYD IS NULL)";
        }
        //全部
        sql.append(String.format(" AND mic1.SY_CREATEUSERID='%s' %s %s %s %s ORDER BY MICROMAIL_UPDATETIME DESC ", userId, keyWordSql, sqlStartTimeSql, sqlEndTimeSql, readTypeSql));
        int weiduNum = 0;
        int guanzhuNum = 0;
        if (!Strings.isNullOrEmpty(id)) {
            if (readType.equals("1") || readType.equals("2") || readType.equals("3")) {//全部的
                //1.未读总数
                String noReadSql = " AND (INFO_SFYD!='1' OR INFO_SFYD IS NULL)";
                String readTypeweiduSql = String.format("%s AND SY_CREATEUSERID='%s' %s %s %s %s %s", countSql, userId, keyWordSql, startTimeSql, endTimeSql, noReadSql, countGroupBySql);
                Map countweiduMap = getCountsMap(readTypeweiduSql);
                weiduNum = Integer.parseInt(countweiduMap.get("COUNT").toString());
            } else if (readType.equals("4") || readType.equals("5") || readType.equals("6")) {//关注的
                //1.未读总数
                String noReadSql = " AND (INFO_SFYD!='1' OR INFO_SFYD IS NULL) AND INFO_SFGZ='1'";
                String readTypeweiduSql = String.format("%s AND SY_CREATEUSERID='%s' %s %s %s %s %s", countSql, userId, keyWordSql, startTimeSql, endTimeSql, noReadSql, countGroupBySql);
                Map countweiduMap = getCountsMap(readTypeweiduSql);
                weiduNum = Integer.parseInt(countweiduMap.get("COUNT").toString());
            }
            //2.关注总数
            String noReadSql = " AND INFO_SFGZ='1'";
            String readTypeweiduSql = String.format("%s AND SY_CREATEUSERID='%s' %s %s %s %s %s", countSql, userId, keyWordSql, startTimeSql, endTimeSql, noReadSql, countGroupBySql);
            Map countweiduMap = getCountsMap(readTypeweiduSql);
            guanzhuNum = Integer.parseInt(countweiduMap.get("COUNT").toString());
        }
        countSql.append(String.format("   AND SY_CREATEUSERID='%s' %s %s %s %s %s", userId, keyWordSql, startTimeSql, endTimeSql, readTypeSql, countGroupBySql));
        List<Map> emails = pcServiceTemplate.queryMapBySql(sql.toString(), start, limitInt);
        List<MicromailDto> dtos = new ArrayList<>();
        for (Map email : emails) {
            dtos.add(MicromailDto.build(email));
        }
        List<MicromailListVo> list = new ArrayList<>();
        for (MicromailDto d : dtos) {
            MicromailListVo vo = MicromailListVo.build(d);
            list.add(vo);
        }
        Map countMap = getCountsMap(countSql.toString());
        int num = Integer.parseInt(countMap.get("COUNT").toString());
        map.put("totalCount", num);
        map.put("list", list);
        map.put("readCount", 0);
        map.put("noReadCount", weiduNum);
        map.put("gzCount", guanzhuNum);
        return map;
    }

    private Map getCountsMap(String sql) {
        List<Map> list = serviceTemplate.queryMapBySql(sql);
        Map map = new HashMap();
        int counts = 0;
        if (list.size() > 0) {
            for (Map count : list) {
                counts += Integer.parseInt(count.get("COUNT").toString());
            }
            map.put("COUNT", counts);
        } else {
            map.put("COUNT", "0");
        }
        return map;
    }

    //接收未读数量
    private long getReceiveNumber(String id) {
        String countGroupBySql = " GROUP BY MICROMAIL_MODELID ";
        StringBuffer countSql = getCountSql("1", "1", id, false);
        countSql.append(" WHERE 1=1 ");
        if (!Strings.isNullOrEmpty(id)) {
            countSql.append(String.format(" AND MICROMAIL_MODELID='%s' ", id));
        }
        countSql.append(" AND (INFO_SFYD!='1' OR INFO_SFYD IS NULL)");
        Map countMap = getCountsMap(countSql.toString() + countGroupBySql);
        long num = Long.parseLong(countMap.get("COUNT").toString());
        return num;
    }

    //发送未读数量
    private long getSendNumber(String id) {
        String countGroupBySql = " GROUP BY MICROMAIL_MODELID ";
        String userId = SecurityUserHolder.getCurrentUser().getUserId();
        StringBuffer countSql = getCountSql("2", "1", id, false);
        countSql.append(" WHERE 1=1 ");
        if (!Strings.isNullOrEmpty(id)) {
            countSql.append(String.format(" AND MICROMAIL_MODELID='%s' ", id));
        }
        countSql.append(String.format(" AND SY_CREATEUSERID='%s' AND (INFO_SFYD!='1' OR INFO_SFYD IS NULL)", userId));
        Map countMap = getCountsMap(countSql.toString() + countGroupBySql);
        long num = Long.parseLong(countMap.get("COUNT").toString());
        return num;
    }

    //发送微邮通知
    @Override
    public void sendMsg() {
        Map<Object, Object> sendMessages = redisCache.hashGet(MICROMAILREDIS);
        int i = 0;
        for (Map.Entry<Object, Object> entry : sendMessages.entrySet()) {
            String key = entry.getKey().toString();
            Map value = (Map) JSON.parse(entry.getValue().toString());
            String url = key.split(",")[1];
            try {
                PushMsgHttpUtil.postToImServer(url, value);
            } catch (Exception e) {
                e.printStackTrace();
            }
            redisCache.hashDelete(MICROMAILREDIS, key);
            i++;
            if (i > 1000) {
                break;
            }
        }
    }

    //添加微邮通知定时任务
    private void addSendMsg(String title, String context, String sendUserIds, String pkValue) {
        String userId = SecurityUserHolder.getCurrentUser().getUserId();
        //发件人和收件人如果是同一个人，移除
        if (sendUserIds.equals(userId) || Strings.isNullOrEmpty(sendUserIds)) {
            return;
        }
        if (sendUserIds.indexOf(userId) != -1) {
            sendUserIds.replace(userId, "");
        }
        //添加消息
        for (String i : sendUserIds.split(",")) {
            List<UserMsgAppInfo> userMsgAppInfos = new ArrayList<>();
            net.sf.json.JSONObject params = new net.sf.json.JSONObject();
            params.put("pkValue", pkValue);
            UserMsgAppInfo appInfo = new UserMsgAppInfo("查看微邮", "PLUGIN", "JE-PLUGIN-MICROMAIL", "form", params);
            userMsgAppInfos.add(appInfo);
            userMsgManaer.sendMsg(i, title, context, "WY", pkValue, userMsgAppInfos);
        }
        //type为4的通知 右下角弹出
        Map<String, String> paramMap = new HashMap<>();
        DwrMsgVo dwrMsgVo = new DwrMsgVo(sendUserIds, title, context);
        paramMap.put("targetIds", sendUserIds);
        paramMap.put("text", JsonBuilder.getInstance().toJson(dwrMsgVo));
        paramMap.put("userId", "SYSTEM");
        paramMap.put("must", "0");
        String s = JSONObject.toJSONString(paramMap);
        redisCache.hashPut(MICROMAILREDIS, JEUUID.uuid() + ",/instant/message/pushMessage", s);
        //个推
        net.sf.json.JSONObject params = new net.sf.json.JSONObject();
        params.put("type", "form");
        params.put("pkValue", pkValue);
        PayloadInfo payload = new PayloadInfo(ExecuteType.FUNC, "JE-PLUGIN-MICROMAIL", params);
        Map<String, String> paramMapgetui = Maps.newHashMap();
        paramMapgetui.put("appId", "");
        paramMapgetui.put("title", title);
        paramMapgetui.put("content", context);
        paramMapgetui.put("targetType", "USERIDS");
        paramMapgetui.put("targetValue", sendUserIds);
        paramMapgetui.put("onlySocket", "0");
        paramMapgetui.put("params", JSON.toJSONString(payload));
        String s2 = JSONObject.toJSONString(paramMapgetui);
        redisCache.hashPut(MICROMAILREDIS, JEUUID.uuid() + ",/instant/phonePush/pushMsg", s2);
        //角标
        portalManager.push(sendUserIds, new String[]{PushType.WY}, new String[]{PushAct.ALL});
        List<PushActVo> pushActVos = new ArrayList<>();
        PushActVo pushActVo = new PushActVo();
        pushActVo.setType("MICROMAIL");
        pushActVo.setCode("JE-PLUGIN-MICROMAIL");
        pushActVo.setRefresh(true);
        pushActVo.setRefreshNum(true);
        pushActVos.add(pushActVo);
        JsonAssist jsonAssist = JsonAssist.getInstance();
        String context2 = jsonAssist.buildListPageJson(new Long(pushActVos.size()), pushActVos, new String[]{}, false);
        dwrMsgVo.setTitle("");
        dwrMsgVo.setCallFunction("JE.doPushInfo");
        dwrMsgVo.setBatchCallFunction("JE.showBatchMsg");
        dwrMsgVo.setLoginHistory(false);
        dwrMsgVo.setLogin(false);
        dwrMsgVo.setContext(context2);
        Map<String, String> paramMap3 = new HashMap<>();
        paramMap3.put("targetIds", sendUserIds);
        paramMap3.put("text", JsonBuilder.getInstance().toJson(dwrMsgVo));
        paramMap3.put("userId", "SYSTEM");
        paramMap3.put("must", dwrMsgVo.getLoginHistory() ? "1" : "0");
        String s3 = JSONObject.toJSONString(paramMap3);
        redisCache.hashPut(MICROMAILREDIS, JEUUID.uuid() + ",/instant/message/pushMessage", s3);
        System.out.println("paramMap3=" + paramMap3);
    }
}
