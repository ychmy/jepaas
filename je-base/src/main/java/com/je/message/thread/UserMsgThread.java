package com.je.message.thread;

import com.je.core.service.PCDynaServiceTemplate;
import com.je.core.util.SpringContextHolder;
import com.je.core.util.bean.DynaBean;

import java.util.ArrayList;
import java.util.List;

public class UserMsgThread extends Thread {
    private List<DynaBean> userMsgs=new ArrayList<>();
    public UserMsgThread(List<DynaBean> userMsgs) {
        this.userMsgs = userMsgs;
    }
    @Override
    public void run() {
        // TODO Auto-generated method stub
        if (userMsgs == null) {
            return;
        }
        PCDynaServiceTemplate serviceTemplate = SpringContextHolder.getBean("PCDynaServiceTemplate");
        for(DynaBean userMsg:userMsgs){
            serviceTemplate.insert(userMsg);
        }
    }
}
