package com.project.demo.vo;

import com.google.common.base.Strings;
import com.je.core.util.bean.BeanUtils;
import com.je.core.util.bean.DynaBean;
import com.sun.istack.internal.NotNull;
import net.sf.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Dept {
    /**
     * 部门名称
     */
    @NotNull
    private String deptName;
    /**
     * 部门code
     */
    @NotNull
    private String deptCode;
    /**
     * 上级部门名称
     */
    private String parentName;
    /**
     * 上级部门编码
     */
    private String parentCode;
    /**
     * 当前路径
     */
    private String path;
    /**
     * 上级部门id
     */
    private String parent;
    /**
     * 部门id
     */
    private String deptId;

    public static List<DynaBean> buildDept(List<JSONObject> depts) {
        List<DynaBean> list = new ArrayList<>();
        for (JSONObject jbDept : depts) {
            Dept dept = (Dept) JSONObject.toBean(jbDept, Dept.class);
            if (Objects.isNull(dept)) {
                continue;
            }
            DynaBean dynaBean = new DynaBean();
            if (Strings.isNullOrEmpty(dept.getParentName())) {
                dynaBean.setStr("PARENT", "ROOT");
                dynaBean.setStr("PATH", "/ROOT");
                dynaBean.setStr("PARENTCODE", "ROOT");
                dynaBean.setStr("PARENTNAME", "ROOT");
            }else {
                dynaBean.setStr("PARENTNAME",dept.getParentName());
            }
            dynaBean.setStr(BeanUtils.KEY_TABLE_CODE, "JE_CORE_DEPARTMENT");
            if (!Strings.isNullOrEmpty(dept.getDeptId())) {
                dynaBean.setStr("deptId", dept.getDeptId());
            }
            dynaBean.setStr("STATUS", "1");
            dynaBean.setStr("RANKNAME", "部门");
            dynaBean.setStr("RANKCODE", "BM");
            dynaBean.setStr("DEPTNAME",dept.getDeptName());
            dynaBean.setStr("DEPTCODE",dept.getDeptCode());
            list.add(dynaBean);
        }
        return list;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }
}
