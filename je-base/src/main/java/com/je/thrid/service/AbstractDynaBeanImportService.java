package com.je.thrid.service;

import java.util.List;

import com.je.thrid.thread.ThridAddTread;
import com.je.thrid.importsource.AbstractDynaBeanImportSource;
import com.je.thrid.importsource.ImportSource;
import com.je.thrid.vo.ThridAddThreadVo;
import com.je.thrid.parser.AbstractDynaBeanImportParser;
import com.je.thrid.parser.ImportParser;

import com.je.core.util.bean.DynaBean;
import net.sf.json.JSONObject;

/**
 * @program: je-platform
 * @author: LIULJ
 * @create: 2020/7/26
 * @description: 抽象数据源导入服务
 */
public abstract class AbstractDynaBeanImportService implements ImportService<List<DynaBean>> {

    private AbstractDynaBeanImportSource dynaBeanImportSource;
    private AbstractDynaBeanImportParser dynaBeanImportParser;
    private JSONObject customParameters;

    @Override
    public ImportSource getImportSource() {
        return dynaBeanImportSource;
    }

    @Override
    public ImportParser getImportParser() {
        return dynaBeanImportParser;
    }

    @Override
    public void doReadAndConvert() {
        ThridAddThreadVo thridAddThreadVo = new ThridAddThreadVo(this);
        ThridAddTread thridAddTread = new ThridAddTread(thridAddThreadVo);
        thridAddTread.start();
    }

    @Override
    public void doImport(AbstractDynaBeanImportSource dynaBeanImportSource,
        AbstractDynaBeanImportParser dynaBeanImportParser, JSONObject customParameters) {
        this.dynaBeanImportParser = dynaBeanImportParser;
        this.dynaBeanImportSource = dynaBeanImportSource;
        this.customParameters = customParameters;
        doReadAndConvert();
    }

    @Override
    public List<DynaBean> doSort(List<DynaBean> dynaBeans) {
        return dynaBeans;
    }

    @Override
    public void customMethod(List<DynaBean> list, JSONObject customParameters) {

    }

    public JSONObject getCustomParameters() {
        return customParameters;
    }
}
