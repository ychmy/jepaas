package com.je.message.thread;

import com.je.message.util.PushMsgHttpUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 个推配置推送线程
 * 
 * @author zhangshuaipeng
 *
 */
public class AppGtConfigTread extends Thread {
    private String appName;
    private String apkId;
    private String appId;
    private String appKey;
    private String appSecret;
    private String appMastSeret;

    public AppGtConfigTread(String appName, String apkId, String appId, String appKey, String appSecret, String appMastSeret) {
        this.apkId = apkId;
        this.appName = appName;
        this.appId = appId;
        this.appKey = appKey;
        this.appSecret = appSecret;
        this.appMastSeret = appMastSeret;
    }

    @Override
    public void run() {
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("appName", appName);
        paramMap.put("apkId", apkId);
        paramMap.put("appId", appId);
        paramMap.put("appKey", appKey);
        paramMap.put("appSecret", appSecret);
        paramMap.put("appMastSeret", appMastSeret);
        try {
            PushMsgHttpUtil.postToImServer("/instant/phonePush/updateAppGtConfig", paramMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
