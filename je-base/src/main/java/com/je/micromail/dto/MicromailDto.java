package com.je.micromail.dto;

import com.google.common.base.Strings;
import net.sf.json.JSONArray;

import java.util.Map;
import java.util.Objects;

/**
 * @Description //微邮评论entity
 * @Auther: yuchunhui
 * @Date: 14 16
 * @Description:
 */

public class MicromailDto {

  private String id;
  private String lastMsg;
  private String zt;
  private String tableCode;
  private JSONArray file;
  private String lastMsgUserName;
  private String createUserName;
  private String modelid;
  private String funcName;
  private String createUserId;
  private String funcId;
  private String lastUserId;
  private String funcCode;
  private String readTime;
  private String zanTime;
  private String modelName;
  private String zw;
  private String updateTime;
  private Boolean isBm;
  private Boolean isKftl;
  private Boolean isFbfs;
  private Boolean isSy;
  private Boolean isSfyd;
  private Boolean isPlyd;
  private Boolean isGz;
  private Boolean isZan;
  private int zanNumber;

  public static MicromailDto build(Map map) {
    MicromailDto mc = new MicromailDto();
    mc.setId((String) map.get("JE_CORE_MICROMAIL_ID"));
    mc.setLastMsg((String) map.get("MICROMAIL_LASTMSG"));
    mc.setZt((String) map.get("MICROMAIL_ZT"));
    mc.setTableCode((String) map.get("MICROMAIL_TABLECODE"));
    if(!Strings.isNullOrEmpty((String)map.get("MICROMAIL_FJ"))){
      String fjJson = (String)map.get("MICROMAIL_FJ");
      JSONArray fileArray = JSONArray.fromObject(fjJson);
      mc.setFile(fileArray);
    }else {
      JSONArray fileArray =new JSONArray();
      mc.setFile(fileArray);
    }
    mc.setLastMsgUserName((String) map.get("MICROMAIL_LASTUSER"));
    mc.setCreateUserName((String) map.get("SY_CREATEUSERNAME"));
    mc.setModelid((String) map.get("MICROMAIL_MODELID"));
    mc.setFuncName((String) map.get("MICROMAIL_FUNCNAME"));
    mc.setCreateUserId((String) map.get("SY_CREATEUSERID"));
    mc.setFuncId((String) map.get("MICROMAIL_FUNCID"));
    mc.setLastUserId((String) map.get("MICROMAIL_LASTUSERID"));
    mc.setFuncCode((String) map.get("MICROMAIL_FUNCCODE"));
    mc.setReadTime((String) map.get("INFO_YDSJ"));
    mc.setZanTime((String) map.get("INFO_DZSJ"));
    mc.setModelName((String) map.get("MICROMAIL_MODELNAME"));
    mc.setZw((String) map.get("MICROMAIL_ZW"));
    mc.setUpdateTime((String) map.get("MICROMAIL_UPDATETIME"));
    mc.setBm(getStringToBoolean(map.get("MICROMAIL_BM")));
    mc.setKftl(getStringToBoolean(map.get("MICROMAIL_KFTL")));
    mc.setFbfs(getStringToBoolean(map.get("MICROMAIL_FBFS")));
    mc.setSy(getStringToBoolean(map.get("MICROMAIL_SY")));
    mc.setSfyd(getStringToBoolean(map.get("INFO_SFYD")));
    if(Strings.isNullOrEmpty(mc.getLastMsg())){
      mc.setPlyd(true);
    }else{
      mc.setPlyd(getStringToBoolean(map.get("INFO_PLYD")));
    }
    mc.setGz(getStringToBoolean(map.get("INFO_SFGZ")));
    mc.setZan(getStringToBoolean(map.get("INFO_SFDZ")));
    mc.setZanNumber( Integer.parseInt(map.get("DZSL").toString()));
    return mc;
  }

  private static Boolean getStringToBoolean(Object value){
    if(!Objects.isNull(value) && value.toString().equals("1")){
      return true;
    }else{
      return false;
    }
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getLastMsg() {
    return lastMsg;
  }

  public void setLastMsg(String lastMsg) {
    this.lastMsg = lastMsg;
  }

  public String getZt() {
    return zt;
  }

  public void setZt(String zt) {
    this.zt = zt;
  }

  public String getTableCode() {
    return tableCode;
  }

  public void setTableCode(String tableCode) {
    this.tableCode = tableCode;
  }

  public JSONArray getFile() {
    return file;
  }

  public void setFile(JSONArray file) {
    this.file = file;
  }

  public String getLastMsgUserName() {
    return lastMsgUserName;
  }

  public void setLastMsgUserName(String lastMsgUserName) {
    this.lastMsgUserName = lastMsgUserName;
  }

  public String getCreateUserName() {
    return createUserName;
  }

  public void setCreateUserName(String createUserName) {
    this.createUserName = createUserName;
  }

  public String getModelid() {
    return modelid;
  }

  public void setModelid(String modelid) {
    this.modelid = modelid;
  }

  public String getFuncName() {
    return funcName;
  }

  public void setFuncName(String funcName) {
    this.funcName = funcName;
  }

  public String getCreateUserId() {
    return createUserId;
  }

  public void setCreateUserId(String createUserId) {
    this.createUserId = createUserId;
  }

  public String getFuncId() {
    return funcId;
  }

  public void setFuncId(String funcId) {
    this.funcId = funcId;
  }

  public String getLastUserId() {
    return lastUserId;
  }

  public void setLastUserId(String lastUserId) {
    this.lastUserId = lastUserId;
  }

  public String getFuncCode() {
    return funcCode;
  }

  public void setFuncCode(String funcCode) {
    this.funcCode = funcCode;
  }

  public String getReadTime() {
    return readTime;
  }

  public void setReadTime(String readTime) {
    this.readTime = readTime;
  }

  public String getZanTime() {
    return zanTime;
  }

  public void setZanTime(String zanTime) {
    this.zanTime = zanTime;
  }

  public String getModelName() {
    return modelName;
  }

  public void setModelName(String modelName) {
    this.modelName = modelName;
  }

  public String getZw() {
    return zw;
  }

  public void setZw(String zw) {
    this.zw = zw;
  }

  public String getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(String updateTime) {
    this.updateTime = updateTime;
  }

  public Boolean getBm() {
    return isBm;
  }

  public void setBm(Boolean bm) {
    isBm = bm;
  }

  public Boolean getKftl() {
    return isKftl;
  }

  public void setKftl(Boolean kftl) {
    isKftl = kftl;
  }

  public Boolean getFbfs() {
    return isFbfs;
  }

  public void setFbfs(Boolean fbfs) {
    isFbfs = fbfs;
  }

  public Boolean getSy() {
    return isSy;
  }

  public void setSy(Boolean sy) {
    isSy = sy;
  }

  public Boolean getSfyd() {
    return isSfyd;
  }

  public void setSfyd(Boolean sfyd) {
    isSfyd = sfyd;
  }

  public Boolean getPlyd() {
    return isPlyd;
  }

  public void setPlyd(Boolean plyd) {
    isPlyd = plyd;
  }

  public Boolean getGz() {
    return isGz;
  }

  public void setGz(Boolean gz) {
    isGz = gz;
  }

  public Boolean getZan() {
    return isZan;
  }

  public void setZan(Boolean zan) {
    isZan = zan;
  }

  public int getZanNumber() {
    return zanNumber;
  }

  public void setZanNumber(int zanNumber) {
    this.zanNumber = zanNumber;
  }
}
