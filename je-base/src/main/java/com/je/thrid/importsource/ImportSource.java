package com.je.thrid.importsource;

import java.sql.SQLException;

/**
 * @program: je-platform
 * @author: LIULJ
 * @create: 2020-07-13 15:45
 * @description: 定义导入源
 */
public interface ImportSource<T> {

    /**
     * 读取操作
     * @return 读取的结果集
     * @throws SQLException
     */
    T read() throws SQLException;

}
