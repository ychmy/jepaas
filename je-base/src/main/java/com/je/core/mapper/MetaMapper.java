package com.je.core.mapper;

import com.je.ibatis.extension.mapper.MetaBaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * MetaMapper
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/12/2
 */
@Mapper
public interface MetaMapper extends MetaBaseMapper {
}
