package com.je.core.mapper.query;

import java.io.Serializable;

/**
 * 条件
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/12/9
 */
public class Condition implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 字段名
     */
    private String code;
    /**
     * 条件类型
     */
    private String type;
    /**
     * 值
     */
    private Object value;
    /**
     * Condition  and 或 or
     */
    private String cn;

    public Condition() {
    }

    public Condition(String code, String type, Object value) {
        this.code = code;
        this.type = type;
        this.value = value;
    }

    public Condition(String code, String type, Object value, String cn) {
        this.code = code;
        this.type = type;
        this.value = value;
        this.cn = cn;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }
}