package com.je.core.entity.extjs;

import java.io.Serializable;

public class DbIndex implements Serializable {

    private static final long serialVersionUID = -664167140686356893L;

    /**
     * 索引名称
     */
    private String indexName;
    /**
     * 列名称
     */
    private String columnName;
    public String getIndexName() {
        return indexName;
    }
    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }
    public String getColumnName() {
        return columnName;
    }
    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

}
