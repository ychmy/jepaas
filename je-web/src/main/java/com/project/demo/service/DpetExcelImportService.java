package com.project.demo.service;

import com.je.core.service.MetaService;
import com.je.core.util.bean.DynaBean;
import com.je.thrid.service.AbstractDynaBeanImportService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 自定义service
 * 
 * @author
 */
@Component
public class DpetExcelImportService extends AbstractDynaBeanImportService {
    @Autowired
    MetaService metaService;

    @Override
    public void customMethod(List<DynaBean> list, JSONObject customParameters) {

    }

    @Override
    public List<DynaBean> doSort(List<DynaBean> dynaBeans) {
        List<DynaBean> newList = new ArrayList<>();
        Map<String, String> map = new HashMap<>();
        for (DynaBean dynaBean : dynaBeans) {
            String deptName = dynaBean.getStr("DEPTNAME");
            if (deptName.indexOf(";") >= 0) {
                String[] deptNames = deptName.split(";");
                for (String str : deptNames) {
                    map.put(str, str);
                }
            } else {
                map.put(deptName, deptName);
            }
        }
        for (String key : map.keySet()) {
            DynaBean dynaBean = new DynaBean("JE_CORE_DEPARTMENT", true);
            newList.add(dynaBean.setStr("DEPTNAME", map.get(key)));
        }
        List<DynaBean> list = new ArrayList<>();
        for (String key : map.keySet()) {
            for (DynaBean dynaBean : newList) {
                String deptName = dynaBean.getStr("DEPTNAME");
                if (map.get(key).indexOf(deptName) >= 0 && !map.get(key).equals(deptName)) {
                    list.add(dynaBean);
                }
            }
        }
        newList.removeAll(list);
        for (DynaBean dynaBean : newList) {
            System.out.println(dynaBean.getStr("DEPTNAME"));
        }
        return newList;
    }


}
