package com.je.micromail.vo;

import com.google.common.base.Strings;
import com.je.core.util.bean.DynaBean;
import net.sf.json.JSONArray;

/**
 * @Description //微邮评论entity
 * @Auther: yuchunhui
 * @Date: 14 16
 * @Description:
 */

public class CommentVo {
    /**
     * @Description //评论人
    **/
    private String plr;
    /**
     * @Description //评论人ID
     **/
    private String plrId;
    /**
     * @Description //正文
     **/
    private String zw;
    /**
     * @Description //评论时间
     **/
    private String createTime;
    /**
     * @Description //评论时间
     **/
    private String updateTime;
    /**
     * @Description //是否是回复
     **/
    private Boolean sfhf;
    /**
     * @Description //回复人
     **/
    private String hfr;
    /**
     * @Description //回复人id
     **/
    private String hfrId;
    /**
     * @Description //主键id
    **/
    private String id;
    /**
     * @Description //附件
    **/
    private JSONArray file;

    public static CommentVo build(DynaBean dynaBean){
        CommentVo cv = new CommentVo();
        cv.setPlr(dynaBean.getStr("SY_CREATEUSERNAME"));
        cv.setPlrId(dynaBean.getStr("SY_CREATEUSERID"));
        cv.setZw(dynaBean.getStr("COMMENT_ZW"));
        cv.setCreateTime(dynaBean.getStr("COMMENT_CREATEDTIME"));
        cv.setUpdateTime(dynaBean.getStr("COMMENT_UPDATETIME"));
        cv.setId(dynaBean.getStr("JE_CORE_MICROMAIL_COMMENT_ID"));
        if(!Strings.isNullOrEmpty(dynaBean.getStr("COMMENT_FJ"))){
          String fjJson = dynaBean.getStr("COMMENT_FJ");
          JSONArray fileArray = JSONArray.fromObject(fjJson);
          cv.setFile(fileArray);
        }else{
          JSONArray fileArray =new JSONArray();
          cv.setFile(fileArray);
        }
        if(Strings.isNullOrEmpty(dynaBean.getStr("COMMENT_ATRID"))){
            cv.setSfhf(false);
        }else {
            cv.setSfhf(true);
        }
        cv.setHfr(dynaBean.getStr("COMMENT_ATR"));
        cv.setHfrId(dynaBean.getStr("COMMENT_ATRID"));
        return  cv;
    }

    public String getPlr() {
        return plr;
    }

    public void setPlr(String plr) {
        this.plr = plr;
    }

    public String getPlrId() {
        return plrId;
    }

    public void setPlrId(String plrId) {
        this.plrId = plrId;
    }

    public String getZw() {
        return zw;
    }

    public void setZw(String zw) {
        this.zw = zw;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Boolean getSfhf() {
        return sfhf;
    }

    public void setSfhf(Boolean sfhf) {
        this.sfhf = sfhf;
    }

    public String getHfr() {
        return hfr;
    }

    public void setHfr(String hfr) {
        this.hfr = hfr;
    }

    public String getHfrId() {
        return hfrId;
    }

    public void setHfrId(String hfrId) {
        this.hfrId = hfrId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public JSONArray getFile() {
        return file;
    }

    public void setFile(JSONArray file) {
        this.file = file;
    }
}
