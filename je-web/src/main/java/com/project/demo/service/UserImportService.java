package com.project.demo.service;

import com.je.core.util.bean.DynaBean;
import com.je.thrid.service.AbstractDynaBeanImportService;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @program: je-platform
 * @author: LIULJ
 * @create: 2020/7/26
 * @description:
 */
@Component
public class UserImportService extends AbstractDynaBeanImportService {

    @Override
    public List<DynaBean> doSort(List<DynaBean> dynaBeans) {
        return dynaBeans;
    }

}
