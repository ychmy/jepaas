package com.je.develop.service;

import java.util.List;
import java.util.Map;

import com.je.develop.vo.FuncInfo;
import com.je.develop.vo.FuncRelationField;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.je.core.util.bean.DynaBean;
import com.je.develop.vo.FuncPermVo;

/**
 * TODO未处理
 */
public interface FunInfoManager {
	/**
	 * 导入按钮
	 * @author sunwanxiang
	 * @date 2012-3-16 下午01:17:35
	 * @param funInfo TODO未处理
	 * @param funId TODO未处理
	 */
	void implButton(DynaBean funInfo, String funId);
	/**
	 * 获取json的功能对象
	 * @param funcCode 功能编码
	 * @return
	 */
	FuncInfo getFuncInfo(String funcCode);

	/**
	 * 获取功能对象
	 * @param funcCode 功能编码
	 * @return
	 */
	FuncInfo getFuncInfoNew(String funcCode);
	/**
	 * 根据功能对象获取功能配置信息
	 * @param funInfo TODO未处理
	 * @return
	 */
	String getStrByFunInfo(DynaBean funInfo);

	/**
	 * 获取功能配置对象
	 * @param funcInfo 功能信息
	 * @param zhId 租户ID
	 * @param hasPerm 是否带权限
	 * @return
	 */
	Map<String,Object> getFuncConfigInfo(DynaBean funcInfo,String zhId,boolean hasPerm);

	/**
	 * 构建功能默认信息
	 * @param funcInfo TODO未处理
	 */
	void buildDefaultFuncInfo(DynaBean funcInfo);
	/**
	 * 更新功能信息(包括对按钮导入、软连接、资源表等做处理)
	 * @param funcInfo TODO未处理
	 */
	DynaBean updateFunInfo(DynaBean funcInfo);
	/**
	 * 删除功能   (包括对功能权限 按钮权限清除。 级联更新父节点信息 、删除挂有该功能的子功能信息和软连接关系清除)
	 * @param funcId TODO未处理
	 */
	void  removeFuncInfoById(String funcId);
	/**
	 * 功能复制
	 * @param newFunInfo 新业务数据
	 * @param oldFuncId 老数据ID
	 */
	DynaBean copyFuncInfo(DynaBean newFunInfo, String oldFuncId);
	/**
	 * 软连接复制
	 * @return
	 */
	DynaBean copySoftFuncInfo(DynaBean newFunInfo, DynaBean oldFunInfo);
	/**
	 * 构建主子功能关联字段的查询条件
	 * @param relatedFields TODO未处理
	 * @param dynaBean TODO未处理
	 */
	String buildWhereSql4funcRelation(List<FuncRelationField> relatedFields, DynaBean dynaBean);
	/**
	 * 清空功能
	 * @param funcInfo TODO未处理
	 */
	DynaBean clearFuncInfo(DynaBean funcInfo);
	/**
	 * 初始化功能
	 * @param funcInfo TODO未处理
	 */
	void initFuncInfo(DynaBean funcInfo);
	/**
	 * 解除软连接关系
	 * @param funcRelyonId TODO未处理
	 */
	void removeFuncRelyon(String funcRelyonId);
	/**
	 * 功能拖动
	 * @param dynaBean
	 */
	DynaBean treeMove(DynaBean dynaBean);

	/**
	 * 功能字段配置同步
	 *
	 * @param funcId       当前功能主键
	 * @param configFuncIds 配置信息参考功能主键
	 */
	void syncConfig(String funcId, String configFuncIds);

}
