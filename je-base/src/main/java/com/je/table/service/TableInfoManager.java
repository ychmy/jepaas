package com.je.table.service;

import javax.servlet.http.HttpServletRequest;

import com.je.core.util.bean.DynaBean;

import java.util.List;

/**
 * 表信息修改  主要负责 键 列 索引的维护
 * @author zhangshuaipeng
 *
 */
public interface TableInfoManager {

	/**
	 * 添加列
	 * @param dynaBean
	 */
	DynaBean addField(DynaBean dynaBean);

	/**
	 * 删除列
	 * @param dynaBean 自定义动态类
	 * @param ids TODO 暂不明确
	 * @param isDll TODO 咋不明确
	 * @return
	 */
	Integer removeColumn(DynaBean dynaBean, String ids, Boolean isDll);

	/**
	 * 删除资源表时，如果有视图使用此字段，则也删除
	 * @param tableCode 资源表编码
	 * @param columns 删除列集合
	 */
	void deleteCascadeViewColumn(String tableCode,List<DynaBean> columns);

	/**
	 * 增量导入
	 * @param dynaBean
	 */
	void impNewCols(DynaBean dynaBean);

	/**
	 * 字典辅助添加列
	 */
	void addColumnByDD(HttpServletRequest request);

	/**
	 * 字典辅助添加列
	 */
	void addColumnByDDList(HttpServletRequest request);

	/**
	 * 表辅助添加列
	 */
	Integer addColumnByTable(HttpServletRequest request);

	/**
	 * 原子辅助添加列
	 * @param strData TODO 暂不明确
	 * @param tableCode 表编码
	 * @param pkValue 主键
	 * @return
	 */
	Integer addColumnByAtom(String strData, String tableCode, String pkValue);

	/**
	 * 存为原子
	 * @param strData TODO 暂不明确
	 * @param pkValue 主键
	 * @return
	 */
	Integer addAtomByColumn(String strData, String pkValue);

	/**
	 * 删除索引
	 * @param dynaBean 自定义动态类
	 * @param ids TODO 暂不明确
	 * @return
	 */
	Integer removeIndex(DynaBean dynaBean, String ids);

	/**
	 * 删除键
	 * @param dynaBean 自定义动态类
	 * @param ids TODO 暂不明确
	 * @param ddl TODO 暂不明确
	 * @return
	 */
	Integer removeKey(DynaBean dynaBean, String ids, String ddl);

	/**
	 * 更新树形表数据(路径 层次 顺序 排序字段)
	 * @param tableCode 表编码
	 * @param pkCode 主键code
	 * @param preFix TODO 暂不明确
	 */
	void syncTreePath(String tableCode, String pkCode, String preFix);
}
