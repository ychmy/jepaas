package com.je.core.mapper.query;

import java.io.Serializable;

/**
 * 排序
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/12/9
 */
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 字段名
     */
    private String code;
    /**
     * 条件类型
     */
    private String type = "";

    public Order() {
    }

    public Order(String code, String type) {
        this.code = code;
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}