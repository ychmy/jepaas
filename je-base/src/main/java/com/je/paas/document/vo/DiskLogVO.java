package com.je.paas.document.vo;

import java.util.Date;

public class DiskLogVO {

    private Long id;
    private Long nodeId;
    private String operatorTypeA;
    private String operatorTypeB;
    private String operatorTypeC;
    private String operatorFileNameA;
    private String operatorFileNameB;
    private String createUserId;
    private String createUserName;
    private Date createTime;
    private String modifiedUserId;
    private String modifiedUserName;
    private Date modifiedTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNodeId() {
        return nodeId;
    }

    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }

    public String getOperatorTypeA() {
        return operatorTypeA;
    }

    public void setOperatorTypeA(String operatorTypeA) {
        this.operatorTypeA = operatorTypeA;
    }

    public String getOperatorTypeB() {
        return operatorTypeB;
    }

    public void setOperatorTypeB(String operatorTypeB) {
        this.operatorTypeB = operatorTypeB;
    }

    public String getOperatorTypeC() {
        return operatorTypeC;
    }

    public void setOperatorTypeC(String operatorTypeC) {
        this.operatorTypeC = operatorTypeC;
    }

    public String getOperatorFileNameA() {
        return operatorFileNameA;
    }

    public void setOperatorFileNameA(String operatorFileNameA) {
        this.operatorFileNameA = operatorFileNameA;
    }

    public String getOperatorFileNameB() {
        return operatorFileNameB;
    }

    public void setOperatorFileNameB(String operatorFileNameB) {
        this.operatorFileNameB = operatorFileNameB;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    public String getModifiedUserName() {
        return modifiedUserName;
    }

    public void setModifiedUserName(String modifiedUserName) {
        this.modifiedUserName = modifiedUserName;
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
}
