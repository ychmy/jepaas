package com.je.message.thread;

import com.je.message.util.PushMsgHttpUtil;
import com.je.message.vo.ImMsgVo;

public class ImSendTread extends Thread {
    private ImMsgVo imMsgVo;//消息

    private ImSendTread() {
    }

    public ImSendTread(ImMsgVo imMsgVo) {
        this();
        this.imMsgVo = imMsgVo;
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        if (imMsgVo == null) {
            return;
        }
        try {
            if (imMsgVo.getType().equals("addUser")) {
                PushMsgHttpUtil.addGroupDetail(imMsgVo.getZhid(), imMsgVo.getUserIds());
            } else if (imMsgVo.getType().equals("delUser")) {
                PushMsgHttpUtil.delGroupDetail(imMsgVo.getZhid(),imMsgVo.getUserIds());
            } else if (imMsgVo.getType().equals("syncUser")) {
                PushMsgHttpUtil.doSyncUser(imMsgVo.getUsersInfo().toString());
            } else {
                PushMsgHttpUtil.postToImServer(imMsgVo.getUrl(),imMsgVo.getParams());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
