package com.je.core.dialect;

import org.hibernate.dialect.DmDialect;
import org.hibernate.type.StandardBasicTypes;

import java.sql.Types;

/**
 * 达梦方言
 *
 * @author wangmm
 * @date 2020/4/20
 */
public class PCDmDialect extends DmDialect {

    public PCDmDialect() {
        super();
        registerHibernateType(Types.CLOB, StandardBasicTypes.TEXT.getName());
    }
}