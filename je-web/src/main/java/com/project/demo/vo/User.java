package com.project.demo.vo;

import com.je.core.util.bean.BeanUtils;
import com.je.core.util.bean.DynaBean;
import net.sf.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class User {
    /**
     * 用户名称
     */
    private String userName;
    /**
     * 登录账号
     */
    private String userCode;
    /**
     * 部门多个用,号隔开
     */
    private String deptName;
    /**
     * 角色多个用,号隔开
     */
    private String roleNames;
    /**
     * 手机号
     */
    private String phone;

    public static List<DynaBean> buildUser(List<JSONObject> users) {
        List<DynaBean> list = new ArrayList<>();
        for (JSONObject jbUser : users) {
            User user = (User) JSONObject.toBean(jbUser, User.class);
            if (Objects.isNull(user)) {
                continue;
            }
            DynaBean dynaBean = new DynaBean();
            dynaBean.setStr(BeanUtils.KEY_TABLE_CODE, "JE_CORE_ENDUSER");
            dynaBean.setStr("ISMANAGER", "0");
            dynaBean.setStr("ENDUSER_CJKHZT_CODE", "1");
            dynaBean.setStr("LOGINLOCKED", "0");
            dynaBean.setStr("ISSYSUSER", "1");
            dynaBean.setStr("EXPIRYDATE", "YJ");
            dynaBean.setStr("NATION", "HZ");
            dynaBean.setStr("DEGREE", "BK");
            dynaBean.setStr("MARRIED", "0");
            dynaBean.setStr("ICONCLS", "jeicon jeicon-khgl-pc");
            dynaBean.setStr("SHADOW", "0");
            dynaBean.setStr("STATUS", "1");
            dynaBean.setStr("LOGINNUMBER", "0");
            dynaBean.setStr("USERNAME", user.getUserName());
            dynaBean.setStr("BACKUSERCODE", user.getUserCode());
            dynaBean.setStr("USERCODE", user.getUserCode());
            dynaBean.setStr("DEPTNAME", user.getDeptName());
            dynaBean.setStr("ROLENAMES", user.getRoleNames());
            dynaBean.setStr("PHONE", user.getPhone());
            list.add(dynaBean);
        }
        return list;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getRoleNames() {
        return roleNames;
    }

    public void setRoleNames(String roleNames) {
        this.roleNames = roleNames;
    }
}

