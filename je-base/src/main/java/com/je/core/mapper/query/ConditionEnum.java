package com.je.core.mapper.query;

/**
 * ConditionType
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/12/10
 */
public enum ConditionEnum {
    /**
     * 包含
     */
    IN("in"),
    /**
     * 包含
     */
    IN_SELECT("inSelect"),
    /**
     * 不包含
     */
    NOT_IN("notIn"),
    /**
     * 不包含
     */
    NOT_IN_SELECT("notInSelect"),
    /**
     * 不为空
     */
    NOT_NULL("notNull"),
    /**
     * 为空
     */
    IS_NULL("isNull"),

    /**
     * between
     */
    BETWEEN("between"),
    /**
     * 不等于
     */
    NE("!="),
    /**
     * 大于
     */
    GT(">"),
    /**
     * 大于等于
     */
    GE(">="),
    /**
     * 小于
     */
    LT("<"),
    /**
     * 小于等于
     */
    LE("<="),
    /**
     * 左模糊查询
     */
    LIKE_LEFT("%like"),
    /**
     * 右模糊查询
     */
    LIKE_RIGHT("like%"),
    /**
     * 模糊查询
     */
    LIKE("like"),
    /**
     * 原始like，不添加前后%
     */
    ORIGINALLIKE("originalLike"),
    /**
     * 等于
     */
    EQ("="),
    /**
     * and嵌套
     */
    AND("and"),
    /**
     * or嵌套
     */
    OR("or");

    /**
     * 条件类型
     */
    private String type;

    ConditionEnum(String type) {
        this.type = type;
    }

    /**
     * 获取类型枚举
     *
     * @param type 类型名称
     * @return com.je.core.mapper.query.ConditionEnum
     */
    public static ConditionEnum getCondition(String type) {
        for (ConditionEnum value : ConditionEnum.values()) {
            if (value.getType().equals(type)) {
                return value;
            }
        }
        return null;
    }

    public String getType() {
        return type;
    }
}
