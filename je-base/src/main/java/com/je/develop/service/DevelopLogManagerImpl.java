package com.je.develop.service;

import com.je.core.service.CommonService;
import com.je.core.service.MetaService;
import com.je.core.util.SecurityUserHolder;
import com.je.core.util.WebUtils;
import com.je.core.util.bean.DynaBean;
import com.je.rbac.model.EndUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("developLogManager")
public class DevelopLogManagerImpl implements DevelopLogManager {

    @Autowired
    private MetaService metaService;
    @Autowired
    private CommonService commonService;

    @Override
    public long getMenuNum(String menuCode) {
        long count=0;
        //资源表
        if("SYS_TABLE".equals(menuCode)){
            count = metaService.countBySql("SELECT COUNT(*) FROM JE_CORE_RESOURCETABLE WHERE SY_JECORE!={0} AND RESOURCETABLE_TYPE IN ('PT','VIEW','TREE')","1");
         //功能
        }else if("FUNCCFG_SUBSYSTEM".equals(menuCode)){
            count = metaService.countBySql("SELECT COUNT(*) FROM JE_CORE_FUNCINFO WHERE SY_JECORE!={0} AND FUNCINFO_NODEINFOTYPE IN ('FUNC','FUNCFIELD')","1");
        //菜单
        }else if("SYS_MENU".equals(menuCode)){
            count = metaService.countBySql("SELECT COUNT(*) FROM JE_CORE_MENU WHERE SY_JECORE!={0} AND MENU_NODEINFOTYPE!='MENU'","1");
         //工作流
        }else if("JE_CORE_PROCESSINFO".equals(menuCode)){
            count = metaService.countBySql("SELECT COUNT(*) FROM JE_CORE_PROCESSINFO WHERE PROCESSINFO_LASTVERSION={0}","none");
        //数据字典
        }else if("JE_CORE_DICTIONARY".equals(menuCode)){
            count = metaService.countBySql("SELECT COUNT(*) FROM JE_CORE_DICTIONARY WHERE SY_JECORE!={0}","1");
        }else if("JE_CORE_CHARTS".equals(menuCode)){
            count = metaService.countBySql("SELECT COUNT(*) FROM JE_CORE_REPORT WHERE SY_DISABLED={0} AND SY_JECORE!={1}","0","1");
            long chartCount = metaService.countBySql("SELECT COUNT(*) FROM JE_CORE_CHARTS WHERE SY_DISABLED={0} AND SY_JECORE!={1}","0","1");
            count+=chartCount;
        }else if("JE_SYS_TIMEDTASK".equals(menuCode)){
            count = metaService.countBySql("SELECT COUNT(*) FROM JE_SYS_TIMEDTASK");
        }
        return count;
    }

    @Override
    public void doDevelopLog(String act, String actName, String type, String typeName, String name, String code, String id) {
        if(!"1".equals(WebUtils.getSysVar("JE_SYS_DEVELOPLOG")))return;
        DynaBean log=new DynaBean("JE_CORE_DEVELOPLOG",true);
        EndUser currentUser= SecurityUserHolder.getCurrentUser();
        log.set("DEVELOPLOG_USERNAME",currentUser.getUsername());
        log.set("DEVELOPLOG_USERID",currentUser.getUserId());
        log.set("DEVELOPLOG_TYPE_NAME",typeName);
        log.set("DEVELOPLOG_TYPE_CODE",type);
        log.set("DEVELOPLOG_ACT_NAME",actName);
        log.set("DEVELOPLOG_ACT_CODE",act);
        log.set("DEVELOPLOG_NAME",name);
        log.set("DEVELOPLOG_CODE",code);
        log.set("DEVELOPLOG_ID",id);
        if("LOGIN".equals(act)){
            log.set("DEVELOPLOG_USERNAME",name);
            log.set("DEVELOPLOG_USERID",id);
            log.set("DEVELOPLOG_NAME","");
            log.set("DEVELOPLOG_CODE","");
            log.set("DEVELOPLOG_ID","");
        }

        commonService.buildModelCreateInfo(log);
        metaService.insert(log);
    }
}
