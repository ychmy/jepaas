package com.je.cache.service.rbac;

import com.je.cache.service.EhcacheManager;
import com.je.rbac.model.UserLeader;

import java.util.List;

public class UserLeaderCacheManager {

	private static final String CACHE_NAME = "userLeaderCache";

	/**
	 * 获取缓存值
	 * @param key 用户id
	 * @return
	 */
	public static UserLeader getCacheValue(String key){
		return (UserLeader) EhcacheManager.getCacheValue(CACHE_NAME, key);
	}
	/**
	 * 添加缓存
	 * @param key 用户id
	 * @param value 值
	 */
	public static void putCache(String key, UserLeader value){
		EhcacheManager.putCache(CACHE_NAME, key, value);
	}
	/**
	 * 清空所有缓存
	 */
	public static void clearAllCache(){
		EhcacheManager.clearAllCache(CACHE_NAME);
	}
	/**
	 * 清空指定的缓存
	 * @param key 用户id
	 */
	public static void removeCache(String key){
		EhcacheManager.removeCache(CACHE_NAME, key);
	}

	/**
	 * 一次性清除所有
	 * @param keys
	 */
	public static void removeCache(List<String> keys){
		EhcacheManager.removeCacheKeys(CACHE_NAME,keys);
	}

}
