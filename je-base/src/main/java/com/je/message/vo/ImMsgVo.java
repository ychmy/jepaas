package com.je.message.vo;

import com.alibaba.fastjson.JSONArray;

import java.util.Map;

/**
 * 消息内容
 */
public class ImMsgVo {
	private String userIds;
	private String type;
	private String zhid;
	private String url;
	private JSONArray usersInfo;
	private Map<String, String> params;

	public ImMsgVo(){};
	public ImMsgVo(String zhid, String userIds,  String type){
		this.userIds = userIds;
		this.type = type;
		this.zhid=zhid;
	}

	public ImMsgVo(JSONArray usersInfo, String type){
		this.usersInfo = usersInfo;
		this.type = type;
	}

	public ImMsgVo(String url,Map<String, String> params){
		this.params = params;
		this.url = url;
		this.type="removeCache";
	}

	public String getUserIds() {
		return userIds;
	}

	public void setUserIds(String userIds) {
		this.userIds = userIds;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getZhid() {
		return zhid;
	}

	public void setZhid(String zhid) {
		this.zhid = zhid;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public JSONArray getUsersInfo() {
		return usersInfo;
	}

	public void setUsersInfo(JSONArray usersInfo) {
		this.usersInfo = usersInfo;
	}

	public Map<String, String> getParams() {
		return params;
	}

	public void setParams(Map<String, String> params) {
		this.params = params;
	}
}
