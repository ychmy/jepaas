package com.je.thrid.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.je.core.util.HttpUtils;
import com.je.core.util.StringUtil;
import com.je.core.util.WebUtils;

import java.util.HashMap;
import java.util.Map;

public class Company235Util {
    private static String APP_ID;
    private static String APP_KEY;
    private static String API_URL;
    private static String KEY_TYPE;
    private static String SEARCH_API_URL;
    public static void init(){
        if(StringUtil.isEmpty(APP_ID)){
            APP_ID = WebUtils.getSysVar("COMPANY_253_APPID");
            APP_KEY = WebUtils.getSysVar("COMPANY_253_APPKEY");
            API_URL = WebUtils.getSysVar("COMPANY_253_APIURL");
            KEY_TYPE = WebUtils.getSysVar("COMPANY_253_KEYTYPE");
            SEARCH_API_URL = WebUtils.getSysVar("COMPANY_253_SEARCHAPIURL");
        }
    }

    /**
     * 执行关键字
     * @param name
     * @return
     */
    public static JSONObject invokebusinessLicense(String name) {
        init();
        Map<String, String> params = new HashMap<String, String>();
        params.put("appId", APP_ID);
        params.put("appKey", APP_KEY);
        params.put("companyKey", name); //搜索关键字（公司全名、公司id）
        params.put("keyType", KEY_TYPE); //1-公司名、2-公司key
        String result = HttpUtils.post(API_URL, params);
        // 解析json,并返回结果
        return JSON.parseObject(result);
    }

    /**
     * 执行查询企业信息
     * @param name
     * @return
     */
    public static JSONObject invokeSearch(String name) {
        init();
        Map<String, String> params = Maps.newHashMap();
        params.put("appId", APP_ID);
        params.put("appKey", APP_KEY);
        params.put("name", name);
        String result = HttpUtils.post(SEARCH_API_URL, params);
        // 解析json,并返回结果
        return JSON.parseObject(result);
    }
}
