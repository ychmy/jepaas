package com.project.demo.service;


import com.google.common.base.Strings;
import com.je.cache.redis.RedisCache;
import com.je.cache.service.rbac.AdminPermInfoCacheManager;
import com.je.cache.service.rbac.TokenUserCacheManager;
import com.je.cache.service.rbac.UserErrorPwCacheManager;
import com.je.core.constants.rbac.PermExtendType;
import com.je.core.constants.tree.NodeType;
import com.je.core.exception.PlatformException;
import com.je.core.exception.PlatformExceptionEnum;
import com.je.core.security.service.LoginService;
import com.je.core.service.CommonService;
import com.je.core.service.MetaService;
import com.je.core.service.PCDynaServiceTemplate;
import com.je.core.util.*;
import com.je.core.util.bean.BeanUtils;
import com.je.core.util.bean.DynaBean;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.model.EndUser;
import com.je.rbac.node.RolePermNode;
import com.je.rbac.service.PermissionManager;
import com.je.rbac.service.RoleManager;
import com.project.demo.vo.Dept;
import com.project.demo.vo.Role;
import com.project.demo.vo.User;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.providers.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Component("importService")
public class ImportServiceImpl implements ImportService {
    @Autowired
    private MetaService metaService;
    @Autowired
    protected CommonService commonService;
    @Autowired
    private RoleManager roleManager;
    @Autowired
    private PermissionManager permissionManager;
    @Autowired
    private LoginService loginService;
    @Autowired
    private RedisCache redisCache;


    @Override
    public String buildToken(String userCode) {
        DynaBean user = metaService.selectOne("JE_CORE_ENDUSER", ConditionsWrapper.builder().eq("USERCODE", userCode));
        EndUser currentUser = loginService.buildCurrentUser(user, "");
        //生成平台登录token -复制现有登录逻辑
        String cacheValue = (String) redisCache.get("loginResetPasswordCache_" + user.getStr("USERID"));
        redisCache.remove("loginResetPasswordCache_" + user.getStr("USERID"));
        currentUser.setInitPassWord(cacheValue);
        user.setStr("LOGINTIME", DateUtils.formatDateTime(new Date()));
        metaService.update(user);
        //登陆成功, 在redis中放入信息
        String tokenId = JEUUID.uuid();
        tokenId = loginService.buildTokenId(currentUser.getUserId(), "web", "", tokenId);
        TokenUserCacheManager.putCache(tokenId, currentUser);
        UserErrorPwCacheManager.removeCache(currentUser.getUserCode());
        UserErrorPwCacheManager.removeCache("__CODE__" + currentUser.getUserCode());
        return tokenId;
    }

    @Override
    public String importRole(List<JSONObject> rolesInfos) {
        List<DynaBean> list = Role.buildRole(rolesInfos);
        for (DynaBean dynaBean : list) {
            Date now = new Date();
            String nowDateTime = DateUtils.formatDateTime(now);
            commonService.buildModelCreateInfo(dynaBean);
            //构建创建信息
            EndUser currentUser = SecurityUserHolder.getCurrentUser();
            dynaBean.set("CREATEUSER", currentUser.getUserCode());
            dynaBean.set("CREATEUSERID", currentUser.getUserId());
            dynaBean.set("CREATEUSERNAME", currentUser.getUsername());
            dynaBean.set("CREATEORG", currentUser.getDeptCode());
            dynaBean.set("CREATEORGID", currentUser.getDeptId());
            dynaBean.set("CREATEORGNAME", currentUser.getDeptName());
            dynaBean.set("CREATETIME", DateUtils.formatDateTime(new Date()));
            dynaBean.set("ROLECODE", commonService.buildCode("ROLECODE", "JE_CORE_ROLE", dynaBean));
            //由于系统字段并非SY_开头导致buildModelCreateInfo无法装载初始化字段
            dynaBean.setStr("CREATEUSERNAME", currentUser.getUsername());
            dynaBean.setStr("CREATEUSER", currentUser.getUserCode());
            dynaBean.setStr("CREATETIME", nowDateTime);
            if (!StringUtil.isNotEmpty(dynaBean.getStr("PATH"))) {
                String parent = dynaBean.getStr("PAERNT");
                DynaBean parentDy = metaService.selectOneByPk("JE_CORE_ROLE", parent);
                dynaBean.setStr("PATH", parentDy.getStr("PATH"));
            }
            if (StringUtil.isNotEmpty(dynaBean.getStr("PATH"))) {
                String uuid = JEUUID.uuid();
                dynaBean.setStr(dynaBean.getStr(BeanUtils.KEY_PK_CODE), uuid);
                dynaBean.set("PATH", dynaBean.getStr("PATH") + "/" + uuid);
                dynaBean.set("PARENTPATH", dynaBean.getStr("PATH"));
                dynaBean.set("LAYER", dynaBean.getStr("LAYER"));
                DynaBean parent = metaService.selectOneByPk(dynaBean.getStr(BeanUtils.KEY_TABLE_CODE), dynaBean.getStr("PARENT"));
                if (NodeType.LEAF.equals(parent.getStr("NODETYPE"))) {
                    metaService.executeSql(" UPDATE JE_CORE_ROLE SET NODETYPE={0} where ROLEID = {1} AND NODETYPE != {2}", NodeType.GENERAL, dynaBean.getStr("PARENT"), NodeType.ROOT);
                }
                dynaBean.set("TREEORDERINDEX", parent.get("TREEORDERINDEX"));
                roleManager.generateRoleTreeOrderIndex(dynaBean);
            }
            //是否处理权限组继承关系？
            metaService.insert(dynaBean);
            DynaBean inserted = dynaBean;
            if (!NodeType.ROOT.equalsIgnoreCase(dynaBean.getStr("PARENT"))) {
                //处理父亲的权限
                RolePermNode rootNode = permissionManager.buildRolePermTree(inserted.getStr("PARENT"));
                metaService.executeSql("INSERT INTO JE_CORE_ROLE_PERM(ROLEID,PERID,TYPE,ENABLED) SELECT {0},PERID,'EXTEND',ENABLED from JE_CORE_ROLE_PERM WHERE ROLEID={1}", inserted.getStr("ROLEID"), dynaBean.getStr("PARENT"));
                rootNode.updateGroup("", "", PermExtendType.PERM_SELF, false);
            } else {
                if (StringUtil.isNotEmpty(inserted.getStr("GROUPCODE"))) {
                    //直接为角色添加聚合权限  因为是新加的角色且是ROOT的孩子  所以无任何权限
                    metaService.executeSql(" INSERT INTO JE_CORE_ROLE_PERM(ROLEID,PERID,TYPE,ENABLED) SELECT {0},PERID,'" + PermExtendType.PERM_GROUP + "','1' FROM JE_CORE_ROLEGROUP_PERM WHERE ROLEGROUPID in ({1})", inserted.getStr("ROLEID"), Arrays.asList(inserted.getStr("GROUPCODE").split(",")));
                }
            }
        }
        //清理缓存权限分级
        AdminPermInfoCacheManager.clearAllCache();
        return "添加成功！";
    }

    @Override
    public String importDept(List<JSONObject> deptInfos) {
        List<DynaBean> list = Dept.buildDept(deptInfos);
        for (DynaBean deptMent : list) {
            // 上级部门名称
            String parentDeptName = deptMent.getStr("PARENTNAME");
            List<DynaBean> parentDepts = metaService.select("JE_CORE_DEPARTMENT",
                    ConditionsWrapper.builder().eq("DEPTNAME", parentDeptName), "DEPTCODE,DEPTNAME,DEPTID,PATH,TREEORDERINDEX");
            if (parentDepts.size() == 0) {
                throw new PlatformException(deptMent.getStr("DEPTNAME") + "上级部门，数据没有找到！", PlatformExceptionEnum.JE_CORE_CONTROLLER_ERROR,
                        new Object[]{});
            }
            DynaBean parentDept = parentDepts.get(0);
            deptMent.setStr("DEPTID", JEUUID.uuid());
            deptMent.setStr("PARENTCODE", parentDept.getStr("DEPTCODE"));
            deptMent.setStr("PARENTNAME", parentDept.getStr("DEPTNAME"));
            deptMent.setStr("PARENT", parentDept.getStr("DEPTID"));
            deptMent.setStr("PARENTPATH", parentDept.getStr("PATH"));
            deptMent.setStr("PATH", String.format("%s/%s", parentDept.getStr("PATH"), deptMent.getStr("DEPTID")));
            deptMent.setStr("NODETYPE", "LEAF");
            buildDeptInfo(deptMent, parentDept);
        }
        return "导入成功";
    }

    @Override
    public String importUser(List<JSONObject> userInfos) {
        List<DynaBean> users = User.buildUser(userInfos);
        for (DynaBean user : users) {
            String userCode = user.getStr("USERCODE");
            // 处理登录名重复
            int count = metaService.select("JE_CORE_ENDUSER", ConditionsWrapper.builder().eq("USERCODE", userCode)).size();
            if (count > 0) {
                throw new PlatformException(userCode + "人员编码重复!", PlatformExceptionEnum.JE_CORE_CONTROLLER_ERROR,
                        new Object[]{});
            }
            user.setStr("BACKUSERCODE", userCode);
            user.setStr("USERCODE", userCode);
            // 性别
            user.setStr("GENDER", "MAN");
            // 密码
            Md5PasswordEncoder md5 = new Md5PasswordEncoder();
            String defaulePassWord =
                    md5.encodePassword(StringUtil.getDefaultValue(WebUtils.getSysVar("JE_SYS_PASSWORD"), "123456"), null);
            user.set("PASSWORD", defaulePassWord);
            user.set("ISMANAGER", "0");
            buildUserDeptInfo(user);
        }
        return "导入成功";
    }


    private DynaBean buildDeptInfo(DynaBean deptMent, DynaBean parentDept) {
        PCDynaServiceTemplate pcDynaServiceTemplate = SpringContextHolder.getBean(PCDynaServiceTemplate.class);
        RoleManager roleManager = SpringContextHolder.getBean(RoleManager.class);
        pcDynaServiceTemplate.buildModelCreateInfo(deptMent);
        EndUser endUser = SecurityUserHolder.getCurrentUser();
        deptMent.setStr("CREATEORG", endUser.getDeptCode());
        deptMent.setStr("CREATEORGNAME", endUser.getDeptName());
        deptMent.setStr("MODIFYTIME", deptMent.getStr("SY_CREATETIME"));
        deptMent.setStr("CREATETIME", deptMent.getStr("SY_CREATETIME"));
        deptMent.setStr("CREATEUSER", endUser.getUserCode());
        deptMent.setStr("CREATEUSER", endUser.getUsername());
        deptMent.setStr("JTGSMC", endUser.getJtgsMc());
        deptMent.setStr("JTGSDM", endUser.getJtgsDm());
        deptMent.setStr("JTGSID", endUser.getJtgsId());
        deptMent.setStr("GSBMID", endUser.getGsbmId());
        deptMent.setStr("RANKCODE", "BM");
        deptMent.setStr("RANKNAME", "部门");
        deptMent.setStr("ORDERINDEX", deptMent.getStr("DISPLAY_ORDER"));
        deptMent.setStr("ZHID", endUser.getZhId());
        deptMent.setStr("ZHMC", endUser.getZhMc());
        deptMent.setStr("CREATEORG", endUser.getDeptCode());
        deptMent.setStr("CREATEORGNAME", endUser.getDeptName());
        deptMent.setStr("STATUS", "1");
        deptMent.setStr("LAYER", "1");
        deptMent.set("TREEORDERINDEX", parentDept.get("TREEORDERINDEX"));
        roleManager.generateTreeOrderIndex(deptMent);
        metaService.insert(deptMent);
        if (parentDept.getStr("NODETYPE").equals("LEAF")) {
            parentDept.setStr("NODETYPE", "GENERAL");
            metaService.update(parentDept);
        }
        return deptMent;
    }

    /**
     * 部门信息
     *
     * @param user
     */
    private void buildUserDeptInfo(DynaBean user) {
        user.setStr("USERID", JEUUID.uuid());
        String deptName = user.getStr("deptName");
        DynaBean dept = null;
        List<DynaBean> depts =
                metaService.select("JE_CORE_DEPARTMENT", ConditionsWrapper.builder().eq("DEPTNAME", deptName));
        if (depts.size() >= 1) {
            dept = depts.get(0);
        }
        if (dept == null) {
            dept = metaService.selectOne("JE_CORE_DEPARTMENT", ConditionsWrapper.builder().eq("DEPTNAME", "系统管理部"));
        }
        user.setStr("DEPTCODE", dept.getStr("DEPTCODE"));
        user.setStr("DEPTNAME", dept.getStr("DEPTNAME"));
        user.setStr("DEPTID", dept.getStr("DEPTID"));
        user.setStr("DEPTORDERINDEX", dept.getStr("TREEORDERINDEX"));
        buildUserCreateInfo(user);
        metaService.insert(user);
        buildUserRoleInfo(user);
    }

    /**
     * 角色信息
     *
     * @param user
     */
    private void buildUserRoleInfo(DynaBean user) {
        String roleNames = user.getStr("ROLENAMES");
        List<String> roleNamesList = Arrays.asList(roleNames.split(","));
        List<DynaBean> role = metaService.select("JE_CORE_ROLE",
                ConditionsWrapper.builder().in("ROLENAME", roleNamesList).eq("ROLETYPE", "ROLE").groupBy("ROLENAME"));
        if (role.size() == 0) {
            throw new RuntimeException(roleNames + "角色没有找到数据异常！");
        }
        StringBuffer roleIds = new StringBuffer();
        StringBuffer roleNamesStr = new StringBuffer();
        StringBuffer roleCodes = new StringBuffer();
        for (DynaBean roleDynaBean : role) {
            DynaBean roleUser = new DynaBean("JE_CORE_ROLE_USER", true);
            roleUser.setStr("USERID", user.getStr("USERID"));
            roleUser.setStr("ROLEID", roleDynaBean.getStr("ROLEID"));
            metaService.insert(roleUser);
            if (Strings.isNullOrEmpty(roleIds.toString())) {
                roleIds.append(roleDynaBean.getStr("ROLEID"));
                roleNamesStr.append(roleDynaBean.getStr("ROLENAME"));
                roleCodes.append(roleDynaBean.getStr("ROLECODE"));
            } else {
                roleIds.append("," + roleDynaBean.getStr("ROLEID"));
                roleNamesStr.append("," + roleDynaBean.getStr("ROLENAME"));
                roleCodes.append("," + roleDynaBean.getStr("ROLECODE"));
            }
        }
        // 角色编码
        user.setStr("ROLECODES", roleCodes.toString());
        // 角色主键
        user.setStr("ROLEIDS", roleIds.toString());
        // 角色名称
        user.setStr("ROLENAMES", roleNamesStr.toString());
        metaService.update(user);
    }

    /**
     * 基础信息
     *
     * @param user
     */
    private void buildUserCreateInfo(DynaBean user) {
        user.setStr("ISSYSUSER", "1");
        user.setStr("FLAG", "1");
        String ORDERINDEX = user.getStr("DISPLAY_ORDER");
        user.set("USERORDER", ORDERINDEX);
        user.setStr("INITPASSWORD", "1");
        user.setStr("SENTRYCODES", "");
        user.setStr("MONITORDEPTCODE", "");
        user.setStr("MENUTYPE", "");
        user.setStr("LOGINLOCKED", "0");
        user.setStr("VALID", "1");
        user.setStr("STATUS", "1");
        PCDynaServiceTemplate pcDynaServiceTemplate = SpringContextHolder.getBean(PCDynaServiceTemplate.class);
        pcDynaServiceTemplate.buildModelCreateInfo(user);
        EndUser endUser = SecurityUserHolder.getCurrentUser();
        user.setStr("CREATEORG", endUser.getDeptCode());
        user.setStr("CREATEORGNAME", endUser.getDeptName());
        user.setStr("CREATETIME", user.getStr("SY_CREATETIME"));
        user.setStr("CREATEUSER", endUser.getUserCode());
        user.setStr("CREATEUSERNAME", endUser.getUsername());
        user.setStr("MODIFYUSER", endUser.getUserCode());
        user.setStr("MODIFYUSERNAME", endUser.getUsername());
        user.setStr("JTGSMC", endUser.getJtgsMc());
        user.setStr("JTGSDM", endUser.getJtgsDm());
        user.setStr("JTGSID", endUser.getJtgsId());
        user.setStr("GSBMID", endUser.getGsbmId());
        user.setStr("ZHID", endUser.getZhId());
        user.setStr("ZHMC", endUser.getZhMc());
    }


}
