package com.je.paas.document.vo;

import java.util.Date;

public class DiskShareVO {

    /**
     * 分享id
     */
    private Long id;
    /**
     * 文件id
     */
    private Long nodeId;
    /**
     * 文件名称
     */
    private String nodeName;
    /**
     * 网盘类型
     */
    private String diskType;
    /**
     * 文件标签
     */
    private String nodeTag;
    /**
     * 文件类型
     */
    private String nodeType;
    /**
     * 文件尾缀
     */
    private String fileSuffix;
    /**
     * 文件大小
     */
    private Long fileSize;
    /**
     * 分享类型
     * 404：找不到文件，主文件已被删除
     * download：已下载
     * unloading：一转存
     */
    private String shareStatus;
    /**
     * 分享类型
     * send（发送）,receive（接收）
     */
    private String shareType;
    /**
     * 分享来源id
     */
    private Long shareTarget;
    /**
     * 分享人id
     */
    private String shareOutId;
    /**
     * 分享人姓名
     */
    private String shareOutName;
    /**
     * 被分享人id
     */
    private String shareInId;
    /**
     * 被分享人姓名
     */
    private String shareInName;
    /**
     * 创建人id
     */
    private String createUserId;
    /**
     * 创建人
     */
    private String createUserName;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date modifiedTime;
    /**
     * 修改人id
     */
    private String modifiedUserId;
    /**
     * 修改人姓名
     */
    private String modifiedUserName;
    /**
     * 转存地址
     */
    private String transferAddress;

    public String getDiskType() {
        return diskType;
    }

    public void setDiskType(String diskType) {
        this.diskType = diskType;
    }

    public String getFileTag() {
        return nodeTag;
    }

    public void setFileTag(String fileTag) {
        this.nodeTag = fileTag;
    }

    public String getFileSuffix() {
        return fileSuffix;
    }

    public void setFileSuffix(String fileSuffix) {
        this.fileSuffix = fileSuffix;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getShareType() {
        return shareType;
    }

    public void setShareType(String shareType) {
        this.shareType = shareType;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public String getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    public String getModifiedUserName() {
        return modifiedUserName;
    }

    public void setModifiedUserName(String modifiedUserName) {
        this.modifiedUserName = modifiedUserName;
    }

    public Long getNodeId() {
        return nodeId;
    }

    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getShareStatus() {
        return shareStatus;
    }

    public void setShareStatus(String shareStatus) {
        this.shareStatus = shareStatus;
    }

    public Long getShareTarget() {
        return shareTarget;
    }

    public void setShareTarget(Long shareTarget) {
        this.shareTarget = shareTarget;
    }


    public String getShareOutName() {
        return shareOutName;
    }

    public void setShareOutName(String shareOutName) {
        this.shareOutName = shareOutName;
    }


    public String getShareInName() {
        return shareInName;
    }

    public void setShareInName(String shareInName) {
        this.shareInName = shareInName;
    }

    public String getNodeTag() {
        return nodeTag;
    }

    public void setNodeTag(String nodeTag) {
        this.nodeTag = nodeTag;
    }

    public String getNodeType() {
        return nodeType;
    }

    public void setNodeType(String nodeType) {
        this.nodeType = nodeType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getShareOutId() {
        return shareOutId;
    }

    public void setShareOutId(String shareOutId) {
        this.shareOutId = shareOutId;
    }

    public String getShareInId() {
        return shareInId;
    }

    public void setShareInId(String shareInId) {
        this.shareInId = shareInId;
    }

    public String getTransferAddress() {
        return transferAddress;
    }

    public void setTransferAddress(String transferAddress) {
        this.transferAddress = transferAddress;
    }
}
