package com.je.core.util.bean;

import com.je.core.util.ArrayUtils;
import com.je.core.util.StringUtil;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 自定义动态类
 * <p>
 * 手工设定属性的内容，系统保留以下属性：
 * $TABLE_CODE$ :   表的名称
 * $ROWS$       :   结果记录集
 * $P_COUNT$      :   当前返回记录数
 * $A_COUNT$  :   总的查询记录数
 * $SQL$        :   查询的SQL语句
 * .....
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/12/11
 * @see BeanUtils (所有参数信息见)
 */
public class DynaBean implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = -2563215753689331432L;

    private static final Logger logger = LoggerFactory.getLogger(DynaBean.class);
    /**
     * 存放属性的值集
     */
    private HashMap<String, Object> values = new HashMap<>();

    /**
     * 初始化空的动态类
     */
    public DynaBean() {
        this.set(BeanUtils.KEY_WHERE, "");
    }

    /**
     * 初始化带表编码信息的动态类
     * 不被推荐的方法
     *
     * @param tableCode 表的编码（即数据库表名）
     */
    @Deprecated
    public DynaBean(String tableCode) {
        values.put(BeanUtils.KEY_TABLE_CODE, tableCode);
        //初始化主键字段
        String pkName = BeanUtils.getInstance().getPKeyFieldNames(tableCode);
        if (pkName != null) {
            this.set(BeanUtils.KEY_PK_CODE, pkName);
        }
        this.set(BeanUtils.KEY_WHERE, "");
    }

    /**
     * 初始化带表编码信息的动态类(是否装载组建)
     *
     * @param tableCode 表的编码（即数据库表名）
     * @param isHavePK  是否自动生成主键的名称
     */
    public DynaBean(String tableCode, Boolean isHavePK) {
        values.put(BeanUtils.KEY_TABLE_CODE, tableCode);
        if (isHavePK) {
            String pkName = BeanUtils.getInstance().getPKeyFieldNames(tableCode);
            if (pkName != null) {
                this.set(BeanUtils.KEY_PK_CODE, pkName);
            }
        }
        this.set(BeanUtils.KEY_WHERE, "");
    }

    /**
     * 研发部:云凤程
     * 初始化带表编码信息的动态类
     *
     * @param tableCode 表的编码（即数据库表名）
     * @param isHavePK  是否自动生成主键的名称
     * @param all       是否装载字段数据
     */
    public DynaBean(String tableCode, Boolean isHavePK, Boolean all) {
        values.put(BeanUtils.KEY_TABLE_CODE, tableCode);
        if (isHavePK) {
            String pkName = BeanUtils.getInstance().getPKeyFieldNames(tableCode);
            if (pkName != null) {
                this.set(BeanUtils.KEY_PK_CODE, pkName);
            }
        }
        if (all) {
            List<DynaBean> columns = (List<DynaBean>) BeanUtils.getInstance().getResourceTable(tableCode).get(BeanUtils.KEY_TABLE_COLUMNS);
            for (DynaBean c : columns) {
                this.set(c.getStr("TABLECOLUMN_CODE"), "");
            }
        }
        this.set(BeanUtils.KEY_WHERE, "");
    }

    //--------------------------------------------------特殊参数

    /**
     * 设置表名
     *
     * @param tableCode 表名
     * @return com.je.core.util.bean.DynaBean
     */
    public DynaBean table(String tableCode) {
        values.put(BeanUtils.KEY_TABLE_CODE, tableCode);
        //初始化主键字段
        String pkName = BeanUtils.getInstance().getPKeyFieldNames(tableCode);
        if (pkName != null) {
            this.set(BeanUtils.KEY_PK_CODE, pkName);
        }
        return this;
    }

    /**
     * 获取表名
     *
     * @return java.lang.String
     */
    public String getTableCode() {
        return getStr(BeanUtils.KEY_TABLE_CODE);
    }

    /**
     * 设置主键字段名
     *
     * @param pkCode 主键字段名
     * @return com.je.core.util.bean.DynaBean
     */
    public DynaBean pkCode(String pkCode) {
        values.put(BeanUtils.KEY_PK_CODE, pkCode);
        return this;
    }

    /**
     * 获取主键字段名
     *
     * @return java.lang.String
     */
    public String getPkCode() {
        return getStr(BeanUtils.KEY_PK_CODE);
    }

    /**
     * 获取主键值
     *
     * @return java.lang.String
     */
    public String getPkValue() {
        return getStr(getPkCode());
    }

    //--------------------------------------------------put

    /**
     * 存放值
     *
     * @param key   键
     * @param value 值
     * @return java.lang.Object
     */
    public Object put(String key, Object value) {
        //英文+数字+下划线
        String columnRegex = "^\\w+$";
        //校验是否匹配规则
        if (key != null && key.trim().matches(columnRegex)) {
            key = key.trim();
        } else {
//            logger.error("column '{}' is invalid.", key);
        }
        // 原逻辑
        if (StringUtil.isNotEmpty(key)) {
            if (BeanUtils.KEY_TABLE_CODE.equals(key)) {
                values.put(key, value);
                //如果传入的是表CODE继续进行主键操作
                //如果已经有PK_CODE则不去查询
                if (!(this.get(BeanUtils.KEY_PK_CODE) != null && !this.get(BeanUtils.KEY_PK_CODE).equals(""))) {
                    String pkName = BeanUtils.getInstance().getPKeyFieldNames(new DynaBean((String) value));
                    if (pkName != null) {
                        this.set(BeanUtils.KEY_PK_CODE, pkName);
                    }
                }
            } else if (BeanUtils.KEY_WHERE.equals(key)) {
                values.put(key, value);
            } else {
                values.put(key, value);
            }
        }
        return null;
    }


    /**
     * 设置属性值，如果有则直接覆盖，如果没有则添加一个
     *
     * @param key   属性名称
     * @param value 属性值
     */
    public DynaBean set(String key, Object value) {
        put(key, value);
        return this;
    }

    /**
     * 设置String类型的属性值，如果有则直接覆盖，如果没有则添加一个
     *
     * @param key   属性名称
     * @param value 属性值
     */
    public DynaBean setStr(String key, String value) {
        put(key, value);
        return this;
    }

    /**
     * 适用于枚举
     * 设置String类型的属性值，如果有则直接覆盖，如果没有则添加一个
     *
     * @param key   属性名称
     * @param value 属性值
     */
    public DynaBean setStr(Enum<?> key, String value) {
        return setStr(key.toString(), value);
    }

    /**
     * 设置int类型的属性值，如果有则直接覆盖，如果没有则添加一个
     *
     * @param key   属性名称
     * @param value 属性值
     */
    public DynaBean setInt(String key, int value) {
        put(key, value);
        return this;
    }

    /**
     * 设置long类型的属性值，如果有则直接覆盖，如果没有则添加一个
     *
     * @param key   属性名称
     * @param value 属性值
     */
    public DynaBean setLong(String key, long value) {
        put(key, value);
        return this;
    }

    /**
     * 设置float类型的属性值，如果有则直接覆盖，如果没有则添加一个
     *
     * @param key   属性名称
     * @param value 属性值
     */
    public DynaBean setFloat(String key, float value) {
        put(key, value);
        return this;
    }

    /**
     * 设置double类型的属性值，如果有则直接覆盖，如果没有则添加一个
     *
     * @param key   属性名称
     * @param value 属性值
     */
    public DynaBean setDouble(String key, double value) {
        put(key, value);
        return this;
    }

    /**
     * 根据表字段设置值
     *
     * @param jsonObj json数据
     */
    public void setJsonValues(JSONObject jsonObj) {
        setBeanValues(jsonObj, new String[]{});
    }

    /**
     * 根据表字段设置值
     *
     * @param valueMap 值
     */
    public void setBeanValues(Map valueMap) {
        setBeanValues(valueMap, new String[]{});
    }

    /**
     * 根据表字段设置值
     *
     * @param valueMap 值
     * @param excludes 排除字段
     */
    public void setBeanValues(Map valueMap, String[] excludes) {
        //1.得到表结构
        String tabelCode = (String) values.get(BeanUtils.KEY_TABLE_CODE);
        DynaBean table = BeanUtils.getInstance().getResourceTable(tabelCode);
        //2.得到列模式
        List<DynaBean> columns = (List<DynaBean>) table.get(BeanUtils.KEY_TABLE_COLUMNS);
        for (DynaBean column : columns) {
            String fieldCode = column.getStr("TABLECOLUMN_CODE");
            if (ArrayUtils.contains(excludes, fieldCode)) {
                continue;
            }
            if (valueMap.containsKey(fieldCode)) {
                values.put(fieldCode, StringUtil.getDefaultValue(valueMap.get(fieldCode), ""));
            }
        }
    }

    /**
     * 获取字段值
     *
     * @param column 字段名
     * @return 字段值
     */
    public Object get(String column) {
        return get(column, null);
    }

    //--------------------------------------------------get

    /**
     * 获取字段值
     *
     * @param column       字段名
     * @param defaultValue 字段默认值
     * @return 字段值
     */
    public Object get(String column, Object defaultValue) {
        return values.getOrDefault(column, defaultValue);
    }

    /**
     * 获取字段值 字符串
     *
     * @param column 字段名
     * @return 字段值
     */
    public String getStr(String column) {
        return getStr(column, null);
    }

    /**
     * 获取字段值 字符串
     *
     * @param column       字段名
     * @param defaultValue 字段默认值
     * @return 字段值
     */
    public String getStr(String column, String defaultValue) {
        Object value = get(column, defaultValue);
        if (value == null) {
            return defaultValue;
        }
        return String.valueOf(value);
    }

    /**
     * 获取字段值 Integer
     *
     * @param column 字段名
     * @return 字段值
     */
    public Integer getInt(String column) {
        return getInt(column, 0);
    }

    /**
     * 获取字段值 Integer
     *
     * @param column       字段名
     * @param defaultValue 字段默认值
     * @return 字段值
     */
    public Integer getInt(String column, int defaultValue) {
        String value = getStr(column);
        if (value == null || value.length() == 0) {
            return defaultValue;
        } else {
            return Integer.parseInt(value);
        }
    }

    /**
     * 获取字段值 Long
     *
     * @param column 字段名
     * @return 字段值
     */
    public Long getLong(String column) {
        return getLong(column, 0);
    }

    /**
     * 获取字段值 Long
     *
     * @param column       字段名
     * @param defaultValue 字段默认值
     * @return 字段值
     */
    public Long getLong(String column, long defaultValue) {
        String value = getStr(column);
        if (value == null || value.length() == 0) {
            return defaultValue;
        } else {
            return Long.parseLong(value);
        }
    }

    /**
     * 获取字段值 Double
     *
     * @param column 字段名
     * @return 字段值
     */
    public Double getDouble(String column) {
        return getDouble(column, 0);
    }

    /**
     * 获取字段值 Double
     *
     * @param column       字段名
     * @param defaultValue 字段默认值
     * @return 字段值
     */
    public Double getDouble(String column, double defaultValue) {
        String value = getStr(column);
        if (value == null || value.length() == 0) {
            return defaultValue;
        } else {
            return Double.parseDouble(value);
        }
    }

    /**
     * 获取字段值 Double
     *
     * @param column 字段名
     * @return 字段值
     */
    public Float getFloat(String column) {
        return getFloat(column, 0);
    }

    /**
     * 获取字段值 Float
     *
     * @param column       字段名
     * @param defaultValue 字段默认值
     * @return 字段值
     */
    public Float getFloat(String column, float defaultValue) {
        String value = getStr(column);
        if (value == null || value.length() == 0) {
            return defaultValue;
        } else {
            return Float.parseFloat(value);
        }
    }

    /**
     * 得到值集
     *
     * @return 值集
     */
    public HashMap getValues() {
        values.remove(BeanUtils.KEY_TABLE_CODE);
        values.remove(BeanUtils.KEY_PK_CODE);
        values.remove(BeanUtils.KEY_WHERE);
        values.remove(BeanUtils.DEF_ALL_FIELDS);
        values.remove(BeanUtils.KEY_QUERY_FIELDS);
        values.remove(BeanUtils.KEY_ORDER);
        return values;
    }

    /**
     * 设置值集
     *
     * @param valueMap 值集
     */
    public void setValues(Map valueMap) {
        this.values.clear();
        this.values.putAll(valueMap);
    }

    /**
     * 得到值集
     *
     * @return 值集
     */
    public HashMap fetchFilterValues() {
        HashMap hashmap = (HashMap) values.clone();
        hashmap.remove(BeanUtils.KEY_TABLE_CODE);
        hashmap.remove(BeanUtils.KEY_PK_CODE);
        hashmap.remove(BeanUtils.KEY_WHERE);
        hashmap.remove(BeanUtils.DEF_ALL_FIELDS);
        hashmap.remove(BeanUtils.KEY_QUERY_FIELDS);
        hashmap.remove(BeanUtils.KEY_ORDER);
        return hashmap;
    }

    /**
     * 获取当前全部数据包含TableCode等
     *
     * @return java.util.HashMap
     */
    public HashMap fetchAllValues() {
        return values;
    }

    //--------------------------------------------------other

    /**
     * 清除数据
     */
    public void clear() {
        this.values.clear();
    }

    /**
     * 覆盖父对象的clone方法，复制出一份内容完全一样的新对象。
     *
     * @return 内容完全一样的新对象
     */
    @Override
    public DynaBean clone() {
        DynaBean dynaBean = new DynaBean();
        dynaBean.setValues((HashMap) values.clone());
        return dynaBean;
    }

    /**
     * 删除指定属性
     *
     * @param key key
     */
    public void remove(String key) {
        if (containsKey(key)) {
            values.remove(key);
        }
    }

    /**
     * 是否包含键
     *
     * @param key key
     */
    public Boolean containsKey(String key) {
        return values.containsKey(key);
    }
}
