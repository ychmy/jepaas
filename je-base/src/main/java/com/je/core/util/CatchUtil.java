package com.je.core.util;

import com.je.core.exception.PlatformException;
import com.je.core.exception.PlatformExceptionEnum;

import java.util.function.Supplier;

/**
 * 异常捕获类
 *
 * @author wangmm
 * @date 2021/1/27
 */
public class CatchUtil {

    public static <T> T doCatch(Supplier<T> supplier) {
        return doCatch(null, supplier);
    }

    public static <T> T doCatch(String message, Supplier<T> supplier) {
        return doCatch(null, PlatformExceptionEnum.UNKOWN_ERROR, supplier);
    }

    /**
     * @param message       错误信息
     * @param exceptionEnum 错误类型
     * @param supplier      执行函数
     * @param <T>           返回值类型
     * @return supplier.get()执行结果
     */
    public static <T> T doCatch(String message, PlatformExceptionEnum exceptionEnum, Supplier<T> supplier) {
        try {
            return supplier.get();
        } catch (Exception e) {
            throw new PlatformException(message == null ? e.getMessage() : message, exceptionEnum, e);
        }
    }

}