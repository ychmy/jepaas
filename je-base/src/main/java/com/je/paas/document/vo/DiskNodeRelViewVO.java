package com.je.paas.document.vo;

import java.util.Date;

public class DiskNodeRelViewVO {

    private Long id;
    private Long nodeId;
    private String nodeType;
    private String nodeName;
    private String nodeTag;
    private Long fileId;
    private String fileKey;
    private String fileSuffix;
    private Long fileSize;
    private Long parent;
    private String parentPath;
    private String parentPathName;
    private String diskType;
    private String ownerId;
    private String ownerName;
    private Integer version;
    private String createUser;
    private String createUserName;
    private Date modifiedTime;
    private String modifiedUser;
    private String modifiedUserName;
    private Boolean deleted;
    private Long roleId;
    private String roleType;
//    private Long roleRoleId;
    private String roleRoleCode;
    private String roleRelId;
    private String roleRelName;
//    private Date roleCreateTime;
//    private String roleCreateUser;
//    private String roleCreateUserName;
//    private Date roleModifiedTime;
//    private String roleModifiedUser;
//    private String roleModifiedUserName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNodeType() {
        return nodeType;
    }

    public void setNodeType(String nodeType) {
        this.nodeType = nodeType;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getNodeTag() {
        return nodeTag;
    }

    public void setNodeTag(String nodeTag) {
        this.nodeTag = nodeTag;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileKey() {
        return fileKey;
    }

    public void setFileKey(String fileKey) {
        this.fileKey = fileKey;
    }

    public String getFileSuffix() {
        return fileSuffix;
    }

    public void setFileSuffix(String fileSuffix) {
        this.fileSuffix = fileSuffix;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public Long getParent() {
        return parent;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }

    public String getParentPath() {
        return parentPath;
    }

    public void setParentPath(String parentPath) {
        this.parentPath = parentPath;
    }

    public String getParentPathName() {
        return parentPathName;
    }

    public void setParentPathName(String parentPathName) {
        this.parentPathName = parentPathName;
    }

    public String getDiskType() {
        return diskType;
    }

    public void setDiskType(String diskType) {
        this.diskType = diskType;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public String getModifiedUser() {
        return modifiedUser;
    }

    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser;
    }

    public String getModifiedUserName() {
        return modifiedUserName;
    }

    public void setModifiedUserName(String modifiedUserName) {
        this.modifiedUserName = modifiedUserName;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getRoleRoleCode() {
        return roleRoleCode;
    }

    public void setRoleRoleCode(String roleRoleCode) {
        this.roleRoleCode = roleRoleCode;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public String getRoleRelId() {
        return roleRelId;
    }

    public void setRoleRelId(String roleRelId) {
        this.roleRelId = roleRelId;
    }

    public String getRoleRelName() {
        return roleRelName;
    }

    public void setRoleRelName(String roleRelName) {
        this.roleRelName = roleRelName;
    }

    public Long getNodeId() {
        return nodeId;
    }

    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }
    //    public Date getRoleCreateTime() {
//        return roleCreateTime;
//    }
//
//    public void setRoleCreateTime(Date roleCreateTime) {
//        this.roleCreateTime = roleCreateTime;
//    }
//
//    public String getRoleCreateUser() {
//        return roleCreateUser;
//    }
//
//    public void setRoleCreateUser(String roleCreateUser) {
//        this.roleCreateUser = roleCreateUser;
//    }
//
//    public String getRoleCreateUserName() {
//        return roleCreateUserName;
//    }
//
//    public void setRoleCreateUserName(String roleCreateUserName) {
//        this.roleCreateUserName = roleCreateUserName;
//    }
//
//    public Date getRoleModifiedTime() {
//        return roleModifiedTime;
//    }
//
//    public void setRoleModifiedTime(Date roleModifiedTime) {
//        this.roleModifiedTime = roleModifiedTime;
//    }
//
//    public String getRoleModifiedUser() {
//        return roleModifiedUser;
//    }
//
//    public void setRoleModifiedUser(String roleModifiedUser) {
//        this.roleModifiedUser = roleModifiedUser;
//    }
//
//    public String getRoleModifiedUserName() {
//        return roleModifiedUserName;
//    }
//
//    public void setRoleModifiedUserName(String roleModifiedUserName) {
//        this.roleModifiedUserName = roleModifiedUserName;
//    }
//
//    public Long getRoleRoleId() {
//        return roleRoleId;
//    }
//
//    public void setRoleRoleId(Long roleRoleId) {
//        this.roleRoleId = roleRoleId;
//    }
}
