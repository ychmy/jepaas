package com.je.thrid.vo;

import com.je.thrid.service.AbstractDynaBeanImportService;

/**
 * 三方添加vo
 */
public class ThridAddThreadVo {
    /**
     * 自定义service
     */
    private AbstractDynaBeanImportService abstractDynaBeanImportService;

    public ThridAddThreadVo(AbstractDynaBeanImportService abstractDynaBeanImportService) {
        this.abstractDynaBeanImportService = abstractDynaBeanImportService;
    }

    public AbstractDynaBeanImportService getAbstractDynaBeanImportService() {
        return abstractDynaBeanImportService;
    }

    public void setAbstractDynaBeanImportService(AbstractDynaBeanImportService abstractDynaBeanImportService) {
        this.abstractDynaBeanImportService = abstractDynaBeanImportService;
    }
}
