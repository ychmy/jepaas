package com.project.demo.service;

import com.je.core.base.MethodArgument;
import com.je.core.service.CommonService;
import com.je.core.service.MetaService;
import com.je.core.service.PCDynaBeanTemplate;
import com.je.core.util.JEUUID;
import com.je.core.util.StringUtil;
import com.je.core.util.bean.BeanUtils;
import com.je.core.util.bean.DynaBean;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.wf.processVo.WfEventSubmitInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("demoService")
public class DemoServiceImpl implements DemoService {
    @Autowired
    private CommonService commonService;

    @Autowired
    private PCDynaBeanTemplate dynaBeanTemplate;

    @Autowired
    private MetaService metaService;

    /**
     * 流程事件
     *
     * @param eventInfo
     */
    @Override
    public void doDemoService(WfEventSubmitInfo eventInfo) {
        System.out.println("流程事件启动!");
    }

    /**
     * 任务提交之后
     *
     * @param eventInfo
     */
    @Override
    public void doTaskEvent(WfEventSubmitInfo eventInfo) {
        System.out.println("任务提交之后");
    }


    /**
     * 重写doSave
     *
     * @param param
     * @return
     */
    @Override
    public DynaBean doSave(MethodArgument param) {

        //获取新增数据
        DynaBean dynaBean = param.getDynaBean();
        //设置系统字段默认值
        commonService.buildModelCreateInfo(dynaBean);

        // 构建编号
        String codeGenFieldInfo = param.getCodeGenFieldInfo();
        if (StringUtil.isNotEmpty(codeGenFieldInfo)) {
            commonService.buildCode(codeGenFieldInfo, dynaBean);
        }

        //检测是否有主功能,并且外键是否有值,如果有则添加,子功能添加树形,检查是否有ROOT节点操作
        //前端是否刷新左边的树
        Boolean treeLoad = false;
        DynaBean parentTree = dynaBeanTemplate.addParentTree(dynaBean, param.getFuncCode());
        if (parentTree != null) {
            String parentpkValue = parentTree.getStr("__PK_VALUE");
            dynaBean.setStr("SY_PARENT", parentpkValue);
            String uuid = dynaBean.getStr(dynaBean.getStr(BeanUtils.KEY_PK_CODE), "");
            if (StringUtil.isEmpty(uuid)) {
                uuid = JEUUID.uuid() + "";
            }
            dynaBean.set(dynaBean.getStr(BeanUtils.KEY_PK_CODE), uuid);
            dynaBean.setStr("SY_NODETYPE", "LEAF");
            dynaBean.setStr("SY_PARENTPATH", "/" + parentpkValue);
            dynaBean.setStr("SY_LAYER", "1");
            dynaBean.setStr("SY_PATH", "/" + parentpkValue);
            treeLoad = true;
        }
        //处理视图保存
        String funcType = param.getParameter("funcType");
        String funcCode = param.getParameter("funcCode");
        if ("view".equals(funcType) && StringUtil.isNotEmpty(funcCode)) {
            commonService.doViewData(funcCode, dynaBean);
        } else {
            //插入数据
            dynaBean = commonService.doSave(dynaBean);
        }
        //检测子功能是否增加ROOT节点 (子功能多树)
        commonService.doChildrenTree(dynaBean, param.getFuncCode());

        //处理单附件多附件上传
        commonService.doSaveFileMetadata(dynaBean, param.getBatchFilesFields(), param.getUploadableFields(), param.getFuncCode());

        //如果是操作视图，则数据重新查询
        String viewTableCode = param.getParameter("viewTableCode");
        if (StringUtil.isNotEmpty(viewTableCode)) {
            String pkName = BeanUtils.getInstance().getPKeyFieldNames(viewTableCode);
            String pkVal = dynaBean.getStr(pkName);
            if (StringUtil.isNotEmpty(pkVal)) {
                dynaBean = metaService.selectOne(viewTableCode, ConditionsWrapper.builder().eq(dynaBean.getPkCode(), pkVal));
            }
        }
        dynaBean.set("SY_TREELOAD", treeLoad);
        return dynaBean;
    }

}
