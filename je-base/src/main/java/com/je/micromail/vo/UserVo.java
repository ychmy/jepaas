package com.je.micromail.vo;


/**
 * @Description //User entity //暂无使用
 * @Auther: yuchunhui
 * @Date: 14 16
 * @Description:
 */

public class UserVo {
    private String userName;
    private String userId;
    //公司部门id
    private String deptId;
    //公司部门名称
    private String deptName;
    //集团公司id
    private String jtgsid;
    //集团公司名称
    private String jtgsmc;
    //账号
    private String userCode;
    //行政职务
    private String executiveName;
    //手机号
    private String phone;
    //性别
    private String gender;
    //头像
    private String photo;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getJtgsid() {
        return jtgsid;
    }

    public void setJtgsid(String jtgsid) {
        this.jtgsid = jtgsid;
    }

    public String getJtgsmc() {
        return jtgsmc;
    }

    public void setJtgsmc(String jtgsmc) {
        this.jtgsmc = jtgsmc;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getExecutiveName() {
        return executiveName;
    }

    public void setExecutiveName(String executiveName) {
        this.executiveName = executiveName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
