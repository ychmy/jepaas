package com.je.dd.service;

import com.alibaba.fastjson.JSONObject;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.core.mapper.query.Query;
import com.je.core.util.bean.DynaBean;
import com.je.dd.vo.DictionaryItemVo;
import com.je.ibatis.extension.conditions.ConditionsWrapper;

import java.util.List;

/**
 * 树形字典相关接口
 *
 * @author wangmm@ketr.com.cn
 * @date 2020/1/13
 */
public interface TreeService {
    /**
     * 读取异步树
     *
     * @param obj      字典查询条件
     * @param en       国际化
     * @param onlyItem 只包含子项
     * @return java.util.List<com.je.core.entity.extjs.JSONTreeNode>
     */
    List<JSONTreeNode> loadAsynTree(JSONObject obj, boolean en, boolean onlyItem);

    /**
     * 异步树时查找接口
     *
     * @param obj       字典查询条件
     * @param queryType 查找类型
     * @param value     查找值
     * @param en        国际化
     * @param onlyItem  只包含子项
     * @return java.util.List<com.je.core.entity.extjs.JSONTreeNode>
     */
    List<JSONTreeNode> findAsyncNodes(JSONObject obj, String queryType, String value, boolean en, boolean onlyItem);

    /**
     * 读取同步树
     *
     * @param obj      字典查询条件
     * @param en       国际化
     * @param onlyItem 只包含子项
     * @return java.util.List<com.je.core.entity.extjs.JSONTreeNode>
     */
    List<JSONTreeNode> loadSyncTree(JSONObject obj, boolean en, boolean onlyItem);

    /**
     * 获取异步树数据
     *
     * @param rootId 根节点ID
     * @param tableName 字典表名
     * @param template 字典模板
     * @param where 查询条件
     * @param isRoot 是否根节点
     * @param onlyWhereSql
     * @return java.util.List<com.je.core.entity.extjs.JSONTreeNode>
     */
    List<JSONTreeNode> loadAsyncTreeNodeList(String rootId, String tableName, JSONTreeNode template, ConditionsWrapper where, Boolean isRoot, Boolean onlyWhereSql);

    /**
     * 查询字典项
     *
     * @param dic      字典主表实体
     * @param query    查询条件
     * @param itemCode 指定查询字段
     * @param en       国际化
     * @param zwfFlag  自维护字典
     * @return java.util.List<com.je.dd.vo.DictionaryItemVo>
     */
    List<DictionaryItemVo> buildChildrenList(DynaBean dic, Query query, String itemCode, boolean en, boolean zwfFlag);

    /**
     * 级联下拉字典
     *
     * @param ddCode 字典编码
     * @param parentId 父级id
     * @param parentCode 多个父级code
     * @param rootId 跟节点ID
     * @param paramStr 参数数组
     * @param zwfFlag 自维护字典
     * @param en 国际化
     * @param query 查询条件
     * @return java.util.List<com.je.core.entity.extjs.JSONTreeNode>
     */
    List<JSONTreeNode> loadLinkTree(String ddCode, String parentId, String parentCode, String rootId, String paramStr, boolean zwfFlag, boolean en, Query query);

    List<JSONTreeNode> loadTreeNodeList(String rootId, String tableName, JSONTreeNode template, ConditionsWrapper where, List<String> includeIds, String[] beanFields);

    List<JSONTreeNode> loadTreeNodeList(String rootId, String tableName, JSONTreeNode template, ConditionsWrapper where, List<String> includeIds, String[] beanFields,String nodeInfo);

}