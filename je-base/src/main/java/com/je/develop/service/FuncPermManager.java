package com.je.develop.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.je.core.entity.extjs.JSONTreeNode;
import com.je.core.util.bean.DynaBean;
import com.je.develop.vo.*;
import com.je.rbac.model.EndUser;
import com.je.wf.processVo.ProcessInfo;

/**
 * TODO未处理
 */
public interface FuncPermManager {

	/**
	 * 获取功能权限
	 * @param funcCode 功能编码
	 * @param refresh 是否刷新，0：否，1：是
	 * @return
	 */
	FuncInfoSaas getFuncInfoSaas(String funcCode, String refresh);

	/**
	 * 获取功能流程信息
	 * @param funcInfo 功能
	 * @param zhId 租户ID
	 * @return
	 */
	List<ProcessInfo> getFuncProcess(FuncInfo funcInfo,String zhId);

	/**
	 * 获取功能权限
	 * @param funcInfo 功能信息
	 * @param processInfos 流程信息
	 * @param zhId 租户ID
	 * @return
	 */
	FuncInfoSaas getFuncInfoSaas(FuncInfo funcInfo,List<ProcessInfo> processInfos,String zhId);

	/**
	 * 获取功能权限
	 * @param funcCode 功能编码
	 * @param zhId 租户ID
	 * @param refresh 是否刷新，0：否，1：是
	 * @return
	 */
	FuncInfoSaas getFuncInfoSaas(String funcCode, String zhId, String refresh);

	/**
	 * 获取功能权限
	 * @param funcCode 功能编码
	 * @param refresh 是否刷新，0：否，1：是
	 * @return
	 */
	FuncUserPerm getFuncUserPerm(String funcCode, String refresh);

	/**
	 * 获取功能权限
	 * @param funcInfo 功能信息
	 * @param funcInfoSaas SAAS租户功能信息
	 * @return
	 */
	FuncUserPerm getFuncUserPerm(FuncInfo funcInfo,FuncInfoSaas funcInfoSaas);

	/**
	 * 获取功能权限
	 * @param funcCode 功能编码
	 * @param userId 用户ID
	 * @param zhId 租户ID
	 * @param roles 角色Beans
	 * @param refresh 是否刷新，0：否，1：是
	 * @return
	 */
	FuncUserPerm getFuncUserPerm(String funcCode, String userId, String zhId, List<DynaBean> roles, String refresh);

	/**
	 * 获取用户功能权限
	 * @param funcInfo 功能信息
	 * @param funcInfoSaas SAAS租户功能信息
	 * @param userId 用户ID
	 * @param zhId 租户ID
	 * @param roles 角色列表
	 * @return
	 */
	FuncUserPerm getFuncUserPerm(FuncInfo funcInfo,FuncInfoSaas funcInfoSaas, String userId,String zhId, List<DynaBean> roles);

	/**
	 * 根据功能编码获取VO
	 * @param funcCode 功能编码
	 * @return
	 */
	FuncPermVo getFuncPerm(String funcCode);

	/**
	 * 根据功能权限   功能人员权限获取权限数据
	 * @param funcPerm 功能权限
	 * @param funcUserPerm 功能人员权限
	 * @return
	 */
	FuncPermVo getFuncPerm(FuncPerm funcPerm, FuncUserPerm funcUserPerm);

	/**
	 * 构建功能权限SQL
	 * @param funcCode 功能编码
	 * @return
	 */
	String buildPermSql(String funcCode);

	/**
	 * 得到功能的可看角色
	 * @param funcCode 功能编码
	 * @param type 查询类型
	 * @param currentUser 创建用户
	 * @return
	 */
	List<JSONTreeNode> getFuncRoleTree(String funcCode, String type, EndUser currentUser);

	/**
	 * 获取改功能的权限
	 * @param funcInfo 功能信息
	 * @param type 类型
	 * @param currentUser 创建用户
	 * @return
	 */
	Map<String,List<HashMap>> getFuncRolePerm(DynaBean funcInfo, String type, EndUser currentUser);

	/**
	 * 根据角色找到功能的权限
	 * @param queryRoleIds 需要查询的角色ID，逗号分隔
	 * @return
	 */
	List<String> getFuncCodeByRole(String queryRoleIds);

	/**
	 * 根据功能获取 字段编码名称
	 * @param funcId 功能ID
	 * @return
	 */
	List<String> getFuncFieldDic(String funcId);

	/**
	 * 获取层级关系的功能
	 * @param cjId 层级ID
	 * @param type 类型  公司级 部门级
	 */
	DynaBean getYwcjPermSql(String cjId, String type);
}
