package com.je.micromail.vo;

import com.google.common.base.Strings;
import com.je.core.util.bean.DynaBean;
import net.sf.json.JSONArray;
import org.apache.commons.collections.map.HashedMap;

import java.util.List;
import java.util.Map;

/**
 * @Description //微邮entity
 * @Auther: yuchunhui
 * @Date: 14 16
 * @Description:
 */

public class MicromailVo {
    /**
     * @Description //主题
     **/
    private String zt;
    /**
     * @Description //发件人名称
     **/
    private String fjr;
    /**
     * @Description //发件人id
     **/
    private String fjrId;
    /**
     * @Description //最后修改时间
     **/
    private String updateTime;
    /**
     * @Description //收件人
     **/
    private Map<String, Map<String, Object>> sjr;
    /**
     * @Description //正文
     **/
    private String zw;
    /**
     * @Description //附件
     **/
    private JSONArray file;
    /**
     * @Description //点赞数量
     **/
    private long dzNum;
    /**
     * @Description //阅读数量
     **/
    private long yyNum;
    /**
     * @Description //阅读总数
     **/
    private long yyTotalNum;
    /**
     * @Description //水印
     **/
    private Boolean sy;
    /**
     * @Description //水印
     **/
    private Boolean bm;
    /**
     * @Description //开放讨论
    **/
    private Boolean kftl;
    /**
     * @Description //分别发送
    **/
    private Boolean fbfs;

    private String funcName;
    private String funcCode;
    private String pkValue;
    private String pkName;

    public static MicromailVo build(DynaBean dynaBean, List<DynaBean> receiveUsers) {
        MicromailVo mc = new MicromailVo();
        mc.setZt(dynaBean.getStr("MICROMAIL_ZT"));
        mc.setFuncName(dynaBean.getStr("MICROMAIL_FUNCNAME"));
        mc.setFuncCode(dynaBean.getStr("MICROMAIL_FUNCCODE"));
        mc.setPkValue(dynaBean.getStr("MICROMAIL_MODELID"));
        mc.setPkName(dynaBean.getStr("MICROMAIL_MODELNAME"));
        mc.setFjr(dynaBean.getStr("SY_CREATEUSERNAME"));
        mc.setFjrId(dynaBean.getStr("SY_CREATEUSERID"));
        mc.setUpdateTime(dynaBean.getStr("MICROMAIL_UPDATETIME"));
        String kftlStr = dynaBean.getStr("MICROMAIL_KFTL");
        if(!Strings.isNullOrEmpty(kftlStr) && kftlStr.equals("1")){
            mc.setKftl(true);
        }else{
            mc.setKftl(false);
        }
        String fbfsStr = dynaBean.getStr("MICROMAIL_FBFS");
        if(!Strings.isNullOrEmpty(fbfsStr) && fbfsStr.equals("1")){
            mc.setFbfs(true);
        }else{
            mc.setFbfs(false);
        }
        Map<String, Object> sjrResut =getReceiveUsers(receiveUsers);
        mc.setSjr((Map<String, Map<String, Object>>)sjrResut.get("list"));
        mc.setZw(dynaBean.getStr("MICROMAIL_ZW"));
        if(!Strings.isNullOrEmpty(dynaBean.getStr("MICROMAIL_FJ"))){
          String fjJson = dynaBean.getStr("MICROMAIL_FJ");
          JSONArray fileArray = JSONArray.fromObject(fjJson);
          mc.setFile(fileArray);
        }else{
          JSONArray fileArray =new JSONArray();
          mc.setFile(fileArray);
        }
        mc.setDzNum(dynaBean.getLong("DZNUM"));
        mc.setYyNum(dynaBean.getLong("YDNUM"));
        mc.setYyTotalNum(dynaBean.getLong("TOTALNUM"));
        if(!Strings.isNullOrEmpty(dynaBean.getStr("MICROMAIL_BM")) && dynaBean.get("MICROMAIL_BM").equals("1")){
            mc.setBm(true);
        }else{
            mc.setBm(false);
        }
        if (!Strings.isNullOrEmpty(dynaBean.getStr("MICROMAIL_SY")) && dynaBean.getStr("MICROMAIL_SY").equals("1")) {
            mc.setSy(true);
        } else {
            mc.setSy(false);
        }
        return mc;
    }

    private static Map<String, Object> getReceiveUsers(List<DynaBean> resUsers) {
      Map<String, Object> result = new HashedMap();
      Map<String, Map<String, Object>> sjr = new HashedMap();
      for(DynaBean dynaBean:resUsers){
        Map<String, Object> map = new HashedMap();
        map.put("order", dynaBean.getInt("SY_ORDERINDEX"));
        map.put("name", dynaBean.getStr("INBOX_JSR"));
        map.put("id", dynaBean.getStr("INBOX_JSRID"));
        map.put("code",dynaBean.getStr("INBOX_CODE"));
        map.put("photoUrl",dynaBean.getStr("INBOX_TX"));
        map.put("photo",dynaBean.getStr("INBOX_TXTP"));
        map.put("type",dynaBean.getStr("INBOX_TYPE"));
        String sfszrl=dynaBean.getStr("INBOX_SFSZSR");
        if(!Strings.isNullOrEmpty(sfszrl) && sfszrl.equals("1")){
          map.put("sfszsr",true);
        }else{
          map.put("sfszsr",false);
        }
        sjr.put(dynaBean.getStr("INBOX_JSRID"),map);
      }
      result.put("list",sjr);
      return result;
    }

    public String getZt() {
        return zt;
    }

    public void setZt(String zt) {
        this.zt = zt;
    }

    public String getFjr() {
        return fjr;
    }

    public void setFjr(String fjr) {
        this.fjr = fjr;
    }

    public String getFjrId() {
        return fjrId;
    }

    public void setFjrId(String fjrId) {
        this.fjrId = fjrId;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Map<String, Map<String, Object>> getSjr() {
        return sjr;
    }

    public void setSjr(Map<String, Map<String, Object>> sjr) {
        this.sjr = sjr;
    }

    public String getZw() {
        return zw;
    }

    public void setZw(String zw) {
        this.zw = zw;
    }

    public JSONArray getFile() {
        return file;
    }

    public void setFile(JSONArray file) {
        this.file = file;
    }

    public long getDzNum() {
        return dzNum;
    }

    public void setDzNum(long dzNum) {
        this.dzNum = dzNum;
    }

    public long getYyNum() {
        return yyNum;
    }

    public void setYyNum(long yyNum) {
        this.yyNum = yyNum;
    }

    public long getYyTotalNum() {
        return yyTotalNum;
    }

    public void setYyTotalNum(long yyTotalNum) {
        this.yyTotalNum = yyTotalNum;
    }

    public Boolean getSy() {
        return sy;
    }

    public void setSy(Boolean sy) {
        this.sy = sy;
    }

    public Boolean getBm() {
        return bm;
    }

    public void setBm(Boolean bm) {
        this.bm = bm;
    }

    public Boolean getKftl() {
        return kftl;
    }

    public void setKftl(Boolean kftl) {
        this.kftl = kftl;
    }

    public Boolean getFbfs() {
        return fbfs;
    }

    public void setFbfs(Boolean fbfs) {
        this.fbfs = fbfs;
    }

    public String getFuncName() {
        return funcName;
    }

    public void setFuncName(String funcName) {
        this.funcName = funcName;
    }

    public String getFuncCode() {
        return funcCode;
    }

    public void setFuncCode(String funcCode) {
        this.funcCode = funcCode;
    }

    public String getPkValue() {
        return pkValue;
    }

    public void setPkValue(String pkValue) {
        this.pkValue = pkValue;
    }

    public String getPkName() {
        return pkName;
    }

    public void setPkName(String pkName) {
        this.pkName = pkName;
    }
}

