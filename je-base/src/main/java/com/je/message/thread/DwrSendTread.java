package com.je.message.thread;

import com.je.core.util.ArrayUtils;
import com.je.core.util.StringUtil;
import com.je.core.util.bean.DynaBean;
import com.je.message.util.DwrUtil;
import com.je.message.vo.DwrMsgVo;

import java.util.ArrayList;
import java.util.List;

/**
 * DWR推送发送线程
 * @author zhangshuaipeng
 *
 */
public class DwrSendTread  extends Thread {
	private DwrMsgVo msgVo;//消息
	private String userId;//用户ID
	private List<String> userIds=null;//批量发送
	private DwrSendTread(){};
	public DwrSendTread(DwrMsgVo msgVo) {
		this();
		this.userId=msgVo.getUserId();
		this.msgVo = msgVo;
	}
	public DwrSendTread(DwrMsgVo msgVo,List<String> userIds) {
		this();
		this.userIds=userIds;
		this.msgVo = msgVo;
	}
	@Override
	public void run() {
		if(msgVo==null)return;
		if(userIds!=null && userIds.size()>0) {
			int count=50;
			List<String> targerIds=new ArrayList<>();
			for(String userId:userIds){
				if(targerIds.size()>count){
					DwrUtil.sendMsg(msgVo, StringUtil.buildSplitString(ArrayUtils.getArray(targerIds),","));
					targerIds=new ArrayList<>();
				}
				targerIds.add(userId);
			}
			if(targerIds.size()>0){
				DwrUtil.sendMsg(msgVo, StringUtil.buildSplitString(ArrayUtils.getArray(targerIds),","));
			}
		}else{
			DwrUtil.sendMsg(msgVo);
		}
	}
	public DwrMsgVo getMsgVo() {
		return msgVo;
	}

	public void setMsgVo(DwrMsgVo msgVo) {
		this.msgVo = msgVo;
	}


}
