package com.je.develop.vo;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * 角色数据权限
 *
 * @author zhangshuaipeng
 */
public class FuncSqlPermVo implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 8179460634172249845L;
    /**
     * 权限类型  ROLE/DEPT
     */
    private String permType;
    /**
     * 角色或部门ID
     */
    private String roleId;
    /**
     * 权限Sql
     */
    private String querySql;
    /**
     * 是否覆盖功能默认数据权限sql
     */
    private Boolean sqlOverwrite;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getPermType() {
        return permType;
    }

    public void setPermType(String permType) {
        this.permType = permType;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getQuerySql() {
        return querySql;
    }

    public void setQuerySql(String querySql) {
        this.querySql = querySql;
    }

    public Boolean getSqlOverwrite() {
        return sqlOverwrite;
    }

    public void setSqlOverwrite(Boolean sqlOverwrite) {
        this.sqlOverwrite = sqlOverwrite;
    }
}
