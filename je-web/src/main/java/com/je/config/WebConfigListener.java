package com.je.config;

import com.je.core.util.SystemProperty;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * web相关配置
 *
 * @author wangmm@ketr.com.cn
 * @date 2020/7/1
 */
public class WebConfigListener implements ServletContextListener {


    @Override
    public void contextInitialized(ServletContextEvent event) {
        SystemProperty.setRootPath(System.getProperty("jeplus.webapp"));
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        System.getProperties().remove(SystemProperty.ROOT_PATH);
    }
}