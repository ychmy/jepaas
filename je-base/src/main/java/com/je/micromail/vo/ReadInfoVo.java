package com.je.micromail.vo;

import com.je.core.util.bean.DynaBean;


/**
 * @Description //微邮阅读状态entity
 * @Auther: yuchunhui
 * @Date: 14 16
 * @Description:
 */

public class ReadInfoVo {
  /**
   * @Description //阅读人id
   **/
  private String userId;
  /**
   * @Description //阅读人
   **/
  private String userName;
  /**
   * @Description //阅读时间
   **/
  private String updateTime;
  /**
   * @Description //提醒次数
   **/
  private int txNumber;

  public static ReadInfoVo build(DynaBean dynaBean,String type) {
    //TODO
    ReadInfoVo readInfoVo = new ReadInfoVo();
    readInfoVo.setUserId(dynaBean.getStr("SY_CREATEUSERID"));
    readInfoVo.setUserName(dynaBean.getStr("SY_CREATEUSERNAME"));
    if(type.equals("1")){//阅读
      readInfoVo.setUpdateTime(dynaBean.getStr("INFO_YDSJ"));
    }else if (type.equals("2")){//点赞
      readInfoVo.setUpdateTime(dynaBean.getStr("INFO_DZSJ"));
    }
    readInfoVo.setTxNumber(3);
    return readInfoVo;
  }

  public static ReadInfoVo buildUser(DynaBean dynaBean) {
    //TODO
    ReadInfoVo readInfoVo = new ReadInfoVo();
    readInfoVo.setUserId(dynaBean.getStr("USERID"));
    readInfoVo.setUserName(dynaBean.getStr("USERNAME"));
    readInfoVo.setTxNumber(dynaBean.getInt("NUMBER"));
    return readInfoVo;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(String updateTime) {
    this.updateTime = updateTime;
  }

  public int getTxNumber() {
    return txNumber;
  }

  public void setTxNumber(int txNumber) {
    this.txNumber = txNumber;
  }
}
